import { getVideoData } from "../services/YouTubeService";


export const generateVideoSchema = async (videoURL) => {
  let schema = {};
  if (videoURL) {
    let videoData = await getVideoData(youtubeParser(videoURL))
      .then((response) => response.data.items[0])
      .catch(() => {
        return {};
      });

    schema = {
      "@context": "http://schema.org",
      "@type": "VideoObject",
      name: `${videoData ? videoData?.snippet?.localized?.title : 0}`,
      description: `${
        videoData ? videoData?.snippet?.localized?.description : 0
      }`,
      thumbnailUrl: `${
        videoData ? videoData?.snippet?.thumbnails?.default.url : 0
      }`,
      uploadDate: `${videoData ? videoData?.snippet?.publishedAt : 0}`,
      duration: `${videoData ? videoData?.contentDetails?.duration : 0}`,
      embedUrl: `https://www.youtube.com/embed/${youtubeParser(videoURL)}`,
      interactionCount: `${videoData ? videoData?.statistics?.viewCount : 0}`,
    };
  } else {
    schema = {};
  }

  return schema;
};

export function youtubeParser(url) {
  var regExp =
    /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
  var match = url.match(regExp);
  return match && match[7].length == 11 ? match[7] : false;
}

export const formatReadCount = (n) => {
  if (n < 1e3) return n;
  if (n >= 1e3 && n < 1e6) return +(n / 1e3).toFixed(1) + "K";
  if (n >= 1e6 && n < 1e9) return +(n / 1e6).toFixed(1) + "M";
  if (n >= 1e9 && n < 1e12) return +(n / 1e9).toFixed(1) + "B";
  if (n >= 1e12) return +(n / 1e12).toFixed(1) + "T";
};

export const getSearchSchema = () => {
  return {
    "@context": "http://schema.org",
    "@type": "WebSite",
    url: "https://www.collegevidya.com/",
    potentialAction: [
      {
        "@type": "SearchAction",
        target: "https://www.collegevidya.com/search?q={search_term_string}",
        "query-input": "required name=search_term_string",
      },
      {
        "@type": "SearchAction",
        target:
          "android-app://com.collegevidya.android/collegevidya/de_sq_seg_-search.collegevidya.com-_{search_term_string}",
        "query-input": "required name=search_term_string",
      },
    ],
  };
};

export const getFAQSchema = (faqs) => {
  let mainEntity = [];

  if (faqs.length) {
    mainEntity.push(
      faqs.map((faq) => {
        return {
          "@type": "Question",
          name: faq.title,
          acceptedAnswer: {
            "@type": "Answer",
            text: faq.content,
          },
        };
      })
    );
  }

  const faqSchema = {
    "@context": "https://schema.org",
    "@type": "FAQPage",
    mainEntity: mainEntity[0] ? mainEntity[0] : mainEntity,
  };

  return faqSchema;
};
