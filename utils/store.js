import { createState } from "@hookstate/core";

const store = createState({
  universalLogo:
    "https://d1aeya7jd2fyco.cloudfront.net/logo/universal-logo.png",
  tagLine: "#ChunoApnaSahi",
  cashbackSwitch: false,
  cashbackText: "Subsidy Cashback Available*",
  cashbackTextOnly: "Subsidy* Cashback",
  cashbackMaxAmount: "₹0",
  leadformButtonAboutText: "Compare on 30+ Factors!",
});

export default store;
