import React, { useEffect, useRef } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Footer from "../Components/Footer";
import Header from "../Components/Header";
import {
  getBlogDetails,
  getBlogSlugs,
  getCategorySlugs,
  readBlog,
} from "../services/BlogService";
import {
  getCities,
  getHomePageReels,
  getMenu,
  getStates,
  pushCustomActivity,
} from "../services/MiscService";
import Image from "next/image";
import Link from "next/link";
import { FacebookShareButton, FacebookIcon } from "next-share";
import { LinkedinShareButton, LinkedinIcon } from "next-share";
import { WhatsappShareButton, WhatsappIcon } from "next-share";
import { TwitterShareButton, TwitterIcon } from "next-share";
import { useRouter } from "next/router";
import {
  formatReadCount,
  generateVideoSchema,
  getFAQSchema,
  getSearchSchema,
} from "../utils/misc";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import BelowHeader from "../Components/BelowHeader";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import {
  addLead,
  createCVCounsellorUserAccount,
} from "../services/LeadService";
import { getCookie, hasCookie, setCookie } from "cookies-next";
import Spinner from "react-bootstrap/Spinner";
import { isMobile } from "react-device-detect";
import Accordion from "react-bootstrap/Accordion";
import LazyLoad from "react-lazy-load";
import Modal from "react-bootstrap/Modal";
import Head from "next/head";
import moment from "moment";
import LeadForm from "../Components/LeadForm";
import Timer from "../Components/Global/Timer/Timer";
import ProgressBar from "react-scroll-progress-bar";
import { InputGroup } from "react-bootstrap";
import CountryDropDown from "../Components/Global/CountryDropDown";
import BlogSlideImage from "../Components/Global/Slider/BlogSlideImage";
import UniversityFormSlider from "../Components/Global/Slider/UniversityFormSlider";
import UniversityFormSliderMobile from "../Components/Global/Slider/UniversityFormSliderMobile";
import GroupImage from "../Components/Global/GroupImage";
import RatingStar from "../Components/Global/RatingStar";
import Podcast from "../Components/Podcast";
import { Dropdown } from "primereact/dropdown";
import { createRoot } from "react-dom/client";
import RemoveMessage from "../Components/Global/RemoveMessage";
import ConnectWithTop from "../Components/Global/ConnectWithTop";
import SecureMessage from "../Components/Global/SecureMessage";
import VerifyOTPModal from "../Components/Global/VerifyOTPModal";
import Logo from "../Components/Global/Logo";
import CashbackCheckbox from "../Components/Global/CashbackCheckbox";
import { useState } from "@hookstate/core";
import store from "../utils/store";
import AuthorCard from "../Components/AuthorCard";
import CareerFinderCard from "../Components/Global/CareerFinderCard";
import UgcNotice from "../Components/Global/Notice/UgcNotice";

export async function getStaticPaths() {
  const allSlugs = await getBlogSlugs().then((response) => response.data.data);

  const paths = allSlugs.map((blogSlug) => {
    return {
      params: {
        blogSlug: blogSlug.slug,
      },
    };
  });

  return { paths, fallback: true };
}

export async function getStaticProps(context) {
  const blogSlug = context.params.blogSlug;
  let success = true;
  let blogData = null;
  let recentBlogs = null;
  let allCategories = null;
  let menuData = null;
  let states = null;
  let homepagereels = null;

  try {
    blogData = await getBlogDetails(blogSlug).then(
      (response) => response.data.data[0]
    );

    recentBlogs = await getBlogDetails(blogSlug).then(
      (response) => response.data.recent_posts
    );

    allCategories = await getCategorySlugs().then((response) => response.data);

    menuData = await getMenu().then((response) => response.data);
    states = await getStates().then((response) => response.data);
    homepagereels = await getHomePageReels().then((response) => response.data);
  } catch (e) {
    success = false;
  } finally {
    if (!success) {
      return {
        notFound: true,
      };
    }
  }

  if (!blogData) {
    return {
      notFound: true,
    };
  }

  const videoSchemas = [];
  if (blogData.blog_type === 2) {
    const videos = blogData.video_url;
    for (let i = 0; i < videos.length; i++) {
      const video = videos[i];
      const vSchema = await generateVideoSchema(video.video_url);
      videoSchemas.push(vSchema);
    }
  }

  return {
    props: {
      allCategories: allCategories,
      menuData: menuData,
      blogData: blogData,
      states: states,
      recentBlogs: recentBlogs,
      videoSchemas: videoSchemas,
      homepagereels: homepagereels,
    },
    revalidate: 60,
  };
}

const Blogdetail = ({
  menuData,
  allCategories,
  blogData,
  states,
  recentBlogs,
  videoSchemas,
  homepagereels,
  screenSize,
}) => {
  const router = useRouter();

  const [search, setSearch] = React.useState(false);
  const [status, setStatus] = React.useState(false);
  const [tocHeads, setTocHeads] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [blogShown, setBlogShown] = React.useState(false);
  const [show, setShow] = React.useState(false);
  const [showLeadForm, setShowLeadForm] = React.useState(false);
  const [modalContent, setModalContent] = React.useState(null);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [show1, setShow1] = React.useState(false);
  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);

  const gtmRef = useRef(null);
  const [cities, setCities] = React.useState([]);
  const [podcastModalContent, setPodcastModalContent] = React.useState(null);
  const [showPodcastModal, setShowPodcastModal] = React.useState(false);

  const [showOTPModal, setShowOTPModal] = React.useState(false);
  const [otpModalContent, setShowOTPModalContent] = React.useState(null);

  const { cashbackSwitch, tagLine, leadformButtonAboutText, cashbackMaxAmount } = useState(store);
  const [scrollClass, setScrollClass] = React.useState(["d-none"]);

  const validationSchema = Yup.object().shape({
    gender: Yup.string().required("Please select a gender"),
    full_name: Yup.string().required("Name is required"),
    dob: Yup.date()
      .typeError("Invalid date")
      .required("Please enter your date of birth")
      .min(moment().subtract(100, "years"), "Max age 100 Yr")
      .max(moment().subtract(12, "years"), "Min age 12 Yr"),
    email: Yup.string()
      .email("Invalid email")
      .matches(/^[a-zA-Z0-9_.]+@[^\s@]+\.[a-zA-Z]{2,}$/, "Invalid email")
      .required("Please enter your email address"),
    selected_country: Yup.number().required(),
    phone_code: Yup.string(),
    country_name: Yup.string(),
    mobile_number: Yup.number()
      .typeError("Invalid number")
      .required("Number is required")
      .when("selected_country", {
        is: (selected_country) => {
          return selected_country === 101;
        },
        then: Yup.number()
          .typeError("Invalid number")
          .required("Number is required")
          .test("len", "Invalid Number", (val) => val.toString().length === 10)
          .test("startsWith", "Invalid Number", (val) =>
            /^[6-9]/.test(val.toString())
          ),
      })
      .when("selected_country", {
        is: (selected_country) => {
          return selected_country !== 101;
        },
        then: Yup.number()
          .typeError("Invalid number")
          .required("Number is required")
          .test(
            "len",
            "Invalid Number",
            (val) => val.toString().length >= 3 && val.toString().length <= 15
          ),
      }),
    course: Yup.number()
      .typeError("Course is required")
      .required("Course is required"),
    state: Yup.number()
      .required("Please select a state")
      .typeError("State is required"),
    city: Yup.number()
      .required("Please select a city")
      .typeError("City is required"),
    couponApplied: Yup.string().required("Select an option"),
  });

  const {
    register,
    getValues,
    setValue,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {},
    mode: "onChange",
    resolver: yupResolver(validationSchema),
  });

  const saveLeadForm = (leadformdata) => {
    setCookie("leadform", JSON.stringify(leadformdata));
  };

  useEffect(() => {
    const handleScroll = () => {
      const scrollPosition = window.scrollY || window.pageYOffset;
      const windowHeight = window.innerHeight;
      const fullHeight = document.documentElement.scrollHeight;

      if (scrollPosition + windowHeight >= fullHeight) {
        setScrollClass("d-none");
      } else if (scrollPosition > 900) {
        setScrollClass("d-block");
      } else {
        setScrollClass("d-none");
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [scrollClass]);

  useEffect(() => {
    if (getValues("state")) {
      getCities(getValues("state"))
        .then((response) => {
          setCities(response.data.data);
          if (getValues("state") === 42) {
            setValue("city", 8487, { shouldValidate: true });
          } else {
            setValue("city", null, { shouldValidate: true });
          }
        })
        .catch(() => {
          setCities([]);
          setValue("city", null, { shouldValidate: true });
        });
    }
  }, [getValues("state")]);

  const onSubmit = (data) => {
    setLoading(true);

    const selectedCourse = menuData
      .map((domain) => domain.courses)
      .flat()
      .filter((course) => course.id === data.course)[0];

    let formData = new FormData();
    formData.append("user_id", -1);
    formData.append("gender", data.gender);
    formData.append("name", data.full_name);
    formData.append("email", data.email);
    if (getValues("selected_country") === 101) {
      formData.append("mobile", data.mobile_number);
    } else {
      formData.append(
        "mobile",
        "+" + getValues("phone_code") + "-" + data.mobile_number
      );
    }
    formData.append("country_name", getValues("country_name"));
    formData.append("dob", data.dob);
    if (selectedCourse.parent_courses[0]) {
      const redirectionalCourse = selectedCourse.parent_courses[0];
      formData.append("course", redirectionalCourse.id);
    } else {
      formData.append("course", data.course);
    }
    formData.append("specializations", "");
    formData.append("state", data.state);
    formData.append("city", data.city);
    formData.append("website_url", router.asPath);
    formData.append("actual_course", selectedCourse.name);

    if (getValues("couponApplied") === "true" || getValues("couponApplied") === "true1") {
      formData.append("coupon_apply", data.couponApplied);
    }

    if (hasCookie("referrer_id")) {
      formData.append("referral", getCookie("referrer_id"));
    } else if (hasCookie("campaign_name")) {
      formData.append("campaign_name", getCookie("campaign_name"));
      formData.append("ad_group_name", getCookie("ad_group_name"));
      formData.append("ads_name", getCookie("ads_name"));
      formData.append("source_campaign", getCookie("source_campaign"));
      if (hasCookie("partner_type"))
        formData.append("partner_type", getCookie("partner_type"));
    }

    addLead(formData)
      .then((response) => {
        setLoading(false);

        saveLeadForm({
          gender: data.gender,
          name: data.full_name,
          email: data.email,
          selected_country: data.selected_country,
          mobile_number: data.mobile_number,
          dob: data.dob,
          state: data.state,
          city: data.city,
        });

        if (!hasCookie("lead-filled")) {
          gtmRef.current.click();
        }

        setCookie("lead-filled", "true");

        if (getValues("couponApplied") === "true" || getValues("couponApplied") === "true1") {
          pushCustomActivity({
            uid: response.data.user_id,
            mx_value_one: getValues("couponApplied") === "true" ? `Receive CV Subsidy Cash upto ${cashbackMaxAmount.get()} directly in your bank account` : `Get upskilling courses worth ${cashbackMaxAmount.get()}`,
            activity_name: "type_to_apply",
          });
        }

        let thirdPageURL = null;

        if (selectedCourse.parent_courses[0]) {
          const redirectionalCourse = selectedCourse.parent_courses[0];
          thirdPageURL = `https://collegevidya.com/colleges-universities/${redirectionalCourse.slug}/?uid=${response.data.user_id}&expert=true`;
        } else {
          thirdPageURL = `https://collegevidya.com/colleges-universities/${selectedCourse.slug}?uid=${response.data.user_id}&expert=true`;
        }

        if (
          hasCookie("referrer_id") &&
          response.data.refer_lead_already_exist === "true"
        ) {
          iziToast.error({
            title: "Lead Already Exists",
            message:
              "Your referrer will not be paid on your admission but you will still get the subsidy.",
            timeout: 3000,
            position: "topRight",
          });
        }

        setShowOTPModalContent(
          <VerifyOTPModal
            uid={response.data.user_id}
            userName={data.full_name}
            userEmail={data.email}
            userMobile={
              getValues("selected_country") === 101
                ? data.mobile_number
                : "+" + getValues("phone_code") + "-" + data.mobile_number
            }
            redirectionURL={thirdPageURL}
            closeModal={() => setShowOTPModal(false)}
          />
        );
        setShowOTPModal(true);
      })
      .catch(() => {
        setLoading(false);
      });
  };

  const startScroll = (id) => {
    const heading = document.getElementById(id);
    heading.scrollIntoView({ behavior: "smooth", block: "start" });
  };

  useEffect(() => {
    if (blogData) {
      let tables = document.getElementsByTagName("table");

      [].slice.call(tables).forEach((table) => {
        let wrapper = document.createElement("div");
        wrapper.className = "custom-table-wrap";
        table.parentNode.insertBefore(wrapper, table);
        wrapper.appendChild(table);
      });
    }
  }, [blogData, blogShown]);

  useEffect(() => {
    if (blogData) {
      readBlog(blogData.id);

      let headingsFinal = [];
      const headingsCollection = document
        .getElementById("blog_wrapper_new")
        ?.getElementsByTagName("h2");

      const headingsArray = headingsCollection ? [...headingsCollection] : [];

      headingsArray.forEach(function (heading) {
        heading.id = heading.innerText;
        headingsFinal.push({
          title: heading.innerText,
          id: heading.id,
        });
      });

      setTocHeads(headingsFinal);
    }
  }, [blogData, blogShown]);

  useEffect(() => {
    if (blogData) {
      const allEmbedImages = document.getElementsByClassName("embed-iframe");
      const allImagesArray = [...allEmbedImages];

      allImagesArray.forEach(function (currentImg) {
        const childDiv = document.createElement("div");
        childDiv.className = "youtube_blog_icon";
        const youtubeImg = document.createElement("img");
        youtubeImg.src = "/blog/Images/icons/youtube.gif";
        childDiv.appendChild(youtubeImg);
        currentImg.appendChild(childDiv);
        currentImg.addEventListener("click", () =>
          embedIFrame(currentImg, currentImg.children[0].getAttribute("video"))
        );
      });
    }
  }, [blogData, blogShown]);

  useEffect(() => {
    if (blogData) {
      const allFormLinks = document.getElementsByClassName("test-link");
      const allLinksArray = [...allFormLinks];

      allLinksArray.forEach(function (currentLink) {
        currentLink.addEventListener("click", () => {
          const externalLink = currentLink.getAttribute("href");

          if (!hasCookie("lead-filled")) {
            setupModal(externalLink);
          } else {
            window.open(externalLink, "_blank");
          }
        });
      });
    }
  }, [blogData, blogShown]);

  useEffect(() => {
    if (blogData) {
      const allOpenFormTags = document.querySelectorAll(".form-tag");

      allOpenFormTags.forEach(function (currentFormTag) {
        const root = createRoot(currentFormTag);
        root.render(
          <div className="col-12 col-md-6 my-2">
            <LeadForm
              menuData={menuData}
              states={states}
              externalLink={"#"}
              handleClose={() => { }}
              fromInline={true}
              screenSize={screenSize}
            />
          </div>
        );
      });
    }
  }, [blogData]);

  useEffect(() => {
    if (blogData) {
      const imagesCollection = document
        .getElementById("blog_wrapper_new")
        ?.getElementsByTagName("img");

      const imagesArray = imagesCollection ? [...imagesCollection] : [];
      imagesArray.forEach(function (image) {
        let imageName = image.src.replace(/^.*[\\\/]/, "");
        if (imageName.includes(".")) {
          imageName = imageName.split(".")[0];
        }
        image.setAttribute(
          "alt",
          imageName.replaceAll ? imageName.replaceAll("-", " ") : ""
        );
      });
    }
  }, [blogData, blogShown]);

  useEffect(() => {
    if (!hasCookie("lead-filled")) {
      if (hasCookie("blog-timer-3")) {
        const timer = setTimeout(() => {
          setShow({ show: true });
        }, 180000);

        return () => clearTimeout(timer);
      } else {
        const timer = setTimeout(() => {
          setShow({ show: true });
        }, 15000);
        setCookie("blog-timer-3", true);
        return () => clearTimeout(timer);
      }
    }
  }, []);

  const embedIFrame = (currentImg, video) => {
    const iframe = document.createElement("iframe");
    iframe.setAttribute("frameborder", "0");
    iframe.setAttribute("allowfullscreen", "1");
    iframe.setAttribute(
      "allow",
      "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
    );
    iframe.setAttribute(
      "src",
      `https://www.youtube.com/embed/${video}?rel=0&showinfo=1&autoplay=1`
    );
    currentImg.innerHTML = "";
    currentImg.appendChild(iframe);
  };

  const setupModal = (externalLink) => {
    setModalContent(
      <LeadForm
        menuData={menuData}
        states={states}
        externalLink={externalLink}
        handleClose={() => setShowLeadForm(false)}
      />
    );
    setShowLeadForm(true);
  };

  const checkLogin = (redirectLink) => {
    if (!hasCookie("lead-filled")) {
      setupModal(redirectLink);
    }
    else {
      router.push(redirectLink);
    }
  }

  const setupPodcastModal = (podcastURL) => {
    setPodcastModalContent(<Podcast podcastURL={podcastURL} />);
    setShowPodcastModal(true);
  };

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  const blog_member = [
    {
      img: "/blog/Images/video/mentor-8.png",
      name: "Pooja",
      exp: "MBA 7 yrs exp",
      rating: "4",
    },
    {
      img: "/blog/Images/video/mentor-2.png",
      name: "Sarthak",
      exp: "M.Com 4 yrs exp",
      rating: "5",
    },
    {
      img: "/blog/Images/video/mentor-3.png",
      name: "Kapil Gupta",
      exp: "MCA 5 yrs exp",
      rating: "4",
    },
  ];

  return (
    <>
      <Head>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="robots"
          content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1"
        />
        <title>{blogData.title}</title>
        <meta name="description" content={blogData.description} />
        <link
          rel="canonical"
          href={`https://collegevidya.com/blog/${blogData.slug}/`}
        />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content={blogData.title} />
        <meta property="og:description" content={blogData.description} />
        <meta
          property="og:url"
          content={`https://collegevidya.com/blog/${blogData.slug}/`}
        />
        <meta property="og:site_name" content="College Vidya Blog" />
        <meta
          property="article:published_time"
          content="2021-10-14T09:56:20+00:00"
        />
        <meta
          property="article:modified_time"
          content="2022-07-29T07:51:08+00:00"
        />

        <meta property="og:image:width" content="1024" />
        <meta property="og:image:height" content="434" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:label1" content="Written by" />
        <meta name="twitter:data1" content="compare" />
        <meta name="twitter:label2" content="Est. reading time" />
        <meta name="twitter:data2" content="6 minutes" />
        <meta
          property="og:image"
          content={blogData.banner ? blogData.banner : blogData.image}
        />
        <meta
          property="twitter:image"
          content={blogData.banner ? blogData.banner : blogData.image}
        />

        {blogData.blog_type === 1 ? (
          <div dangerouslySetInnerHTML={{ __html: blogData.metadata }} />
        ) : null}

        {blogData.blog_type === 2 ? (
          <script
            type="application/ld+json"
            dangerouslySetInnerHTML={{
              __html: JSON.stringify({
                "@context": "http://schema.org",
                "@type": "Article",
                articleSection: blogData.categories[0].name,
                mainEntityOfPage: {
                  "@type": "WebPage",
                  "@id": `https://collegevidya.com/blog/${blogData.slug}/`,
                },
                headline: blogData.title,
                image: {
                  "@type": "ImageObject",
                  url: blogData.banner ? blogData.banner : blogData.image,
                  copyrightYear: 2025,
                  height: 1024,
                  width: 433,
                },
                datePublished: blogData.created_at,
                dateModified: blogData.updated_at,
                author: {
                  "@type": "Person",
                  name: blogData.author_blog.name,
                  description: blogData.author_blog.about,
                  jobTitle: blogData.author_blog.current_role_in_the_industry,
                  url: `https://collegevidya.com/blog/author/${blogData.author_blog.slug}/`,
                },
                publisher: {
                  "@type": "Organization",
                  name: "College Vidya",
                  url: "https://collegevidya.com/",
                  sameAs: [
                    "https://www.instagram.com/collegevidya",
                    "https://facebook.com/collegevidya",
                    "https://twitter.com/collegevidya",
                    "https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                    "https://www.linkedin.com/company/college-vidya",
                  ],
                  logo: {
                    "@type": "ImageObject",
                    url: "https://collegevidya.com/images/logo.svg",
                    width: 150,
                    height: 80,
                  },
                },
                description: blogData.description,
              }),
            }}
          />
        ) : null}

        {blogData.blog_type === 2 ? (
          <>
            <script
              type="application/ld+json"
              dangerouslySetInnerHTML={{
                __html: JSON.stringify(getSearchSchema()),
              }}
            />
            <script
              type="application/ld+json"
              dangerouslySetInnerHTML={{
                __html: JSON.stringify({
                  "@context": "http://schema.org",
                  "@type": "webpage",
                  url: `https://collegevidya.com/blog/${blogData.slug}`,
                  name: blogData.title_h_one
                    ? blogData.title_h_one
                    : blogData.title,
                  description: blogData.description,
                  speakable: {
                    "@type": "SpeakableSpecification",
                    cssSelector: [".seotag"],
                  },
                }),
              }}
            />

            <script
              type="application/ld+json"
              dangerouslySetInnerHTML={{
                __html: JSON.stringify({
                  "@context": "http://schema.org",
                  "@type": "Organization",
                  name: "College Vidya",
                  url: "https://collegevidya.com/",
                  sameAs: [
                    "https://facebook.com/collegevidya",
                    "https://twitter.com/collegevidya",
                    " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                    "https://www.linkedin.com/company/college-vidya",
                    "https://www.instagram.com/collegevidya",
                  ],
                  logo: "https://collegevidya.com/images/logo.svg",
                  legalName: "College Vidya",
                  address: {
                    "@type": "PostalAddress",
                    addressCountry: "India",
                    addressLocality: "",
                    addressRegion: "",
                    postalCode: "",
                    streetAddress: ",",
                  },
                  contactPoint: {
                    "@type": "ContactPoint",
                    telephone: "18004205757",
                    contactType: "Customer Service",
                    contactOption: "TollFree",
                    areaServed: ["IN"],
                  },
                }),
              }}
            />
          </>
        ) : null}

        {/* Breadcrumb Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "https://schema.org",
              "@type": "BreadcrumbList",
              itemListElement: [
                {
                  "@type": "ListItem",
                  position: 1,
                  item: {
                    "@id": "https://collegevidya.com/blog",
                    name: "Blog",
                  },
                },
                {
                  "@type": "ListItem",
                  position: 2,
                  item: {
                    "@id": `https://collegevidya.com/blog/category/${blogData.categories[0].slug}/`,
                    name: blogData.categories[0].name,
                  },
                },
                {
                  "@type": "ListItem",
                  position: 3,
                  item: {
                    "@id": `https://collegevidya.com/blog/${blogData.slug}/`,
                    name: blogData.title,
                  },
                },
              ],
            }),
          }}
        />

        {/* VideoSchema */}
        {videoSchemas.map((videoSchema, index) => {
          return (
            <script
              key={index}
              type="application/ld+json"
              dangerouslySetInnerHTML={{
                __html: JSON.stringify(videoSchema),
              }}
            />
          );
        })}

        {/* PodcastSchema */}
        {blogData.podcast.map((podcastSchema, index) => {
          return (
            <script
              key={index}
              type="application/ld+json"
              dangerouslySetInnerHTML={{
                __html: JSON.stringify({
                  "@context": "https://schema.org/",
                  "@type": "PodcastEpisode",
                  url: podcastSchema.url,
                  name: podcastSchema.title,
                  datePublished: "2015-02-18",
                  timeRequired: "PT37M",
                  description: podcastSchema.description,
                  associatedMedia: {
                    "@type": "MediaObject",
                    contentUrl: podcastSchema.url,
                  },
                  partOfSeries: {
                    "@type": "PodcastSeries",
                    name: "CV Talk",
                    url: podcastSchema.url,
                  },
                }),
              }}
            />
          );
        })}

        {/* FAQ Schema */}
        {blogData.blog_type === 2 && blogData.faqs?.length > 0 ? (
          <script
            type="application/ld+json"
            dangerouslySetInnerHTML={{
              __html: JSON.stringify(getFAQSchema(blogData.faqs)),
            }}
          />
        ) : null}

        {/* Audio Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "https://schema.org/",
              "@type": "PodcastEpisode",
              url: blogData.url,
              name: blogData.title,
              datePublished: "2015-02-18",
              timeRequired: "PT37M",
              description: blogData.description,
              associatedMedia: {
                "@type": "MediaObject",
                contentUrl: `${process.env.NEXT_PUBLIC_PLESK_AUDIO_URL}${blogData.slug}.mp3`,
              },
              partOfSeries: {
                "@type": "PodcastSeries",
                name: "CV Talk",
                url: `${process.env.NEXT_PUBLIC_PLESK_AUDIO_URL}${blogData.slug}.mp3`,
              },
            }),
          }}
        />
      </Head>

      <ProgressBar bgcolor="#0056d2" height="3px" />

      <Header menuData={menuData} search={search} />
      <BelowHeader menuData={menuData} />
      <Container className="mt-2 mt-sm-3">
        <Row>
          <Col className="col-12 col-xl-9">
            {blogData.banner || blogData.image ? (
              <Image
                src={blogData.banner ? blogData.banner : blogData.image}
                width={800}
                height={400}
                layout="responsive"
                alt={
                  blogData.banner
                    ? blogData.banner
                      .substring(blogData.banner.lastIndexOf("/") + 1)
                      .replace(/\.[^\/.]+$/, "")
                      .replace(/-/g, " ")
                    : blogData.image
                      .substring(blogData.image.lastIndexOf("/") + 1)
                      .replace(/\.[^\/.]+$/, "")
                      .replace(/-/g, " ")
                }
                priority={true}
                quality={100}
                className="rounded"
              />
            ) : null}

            <div className={`mt-4 mb-3`} style={{ fontSize: "14px" }}>
              <Link href="/" style={{ color: "#f75d34" }}>
                Home
              </Link>{" "}
              <span>&#10095;</span>{" "}
              <Link
                href={`/category/${blogData.categories[0].slug}`}
                style={{ color: "#f75d34" }}
              >
                {blogData.categories[0].name}
              </Link>{" "}
              <span>&#10095; </span>
              {blogData.title_h_one ? blogData.title_h_one : blogData.title}
            </div>
            <h1 className="h3 fw-bold mb-2 blog-main-head seotag">
              {blogData.title_h_one ? blogData.title_h_one : blogData.title}
            </h1>
            <p
              style={{ fontSize: "14px" }}
              className="d-flex align-items-center justify-content-between flex-wrap"
            >
              <span className="d-flex align-items-center gap-2 flex-wrap">
                <span className="d-flex align-items-center gap-1 hover-underline">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    fill="currentColor"
                    className="bi bi-person-circle"
                    viewBox="0 0 16 16"
                  >
                    <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0" />
                    <path
                      fill-rule="evenodd"
                      d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8m8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1"
                    />
                  </svg>{" "}
                  <Link
                    href={`/author/${blogData.author_blog.slug}`}
                    target="_blank"
                  >
                    {" "}
                    {blogData.author_blog.name}
                  </Link>
                </span>
                <span className="d-flex align-items-center">
                  <svg
                    stroke="currentColor"
                    fill="currentColor"
                    strokeWidth="0"
                    viewBox="0 0 16 16"
                    className="me-1"
                    height="1em"
                    width="1em"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"></path>
                  </svg>{" "}
                  {moment(blogData.updated_at).format("ll")}
                </span>
              </span>
              <span className="d-flex align-items-center">
                <svg
                  stroke="currentColor"
                  fill="currentColor"
                  strokeWidth="0"
                  viewBox="0 0 16 16"
                  className="me-1"
                  height="1em"
                  width="1em"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"></path>
                  <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"></path>
                </svg>{" "}
                {formatReadCount(blogData.blog_read)} Reads
              </span>
            </p>
            <div className="d-flex flex-wrap align-items-center justify-content-between">
              <div className="d-flex gap-2 mb-3">
                <FacebookShareButton
                  url={`https://collegevidya.com/blog/${blogData.slug}`}
                  title={blogData.title}
                >
                  <FacebookIcon size={25} round />
                </FacebookShareButton>
                <TwitterShareButton
                  url={`https://collegevidya.com/blog/${blogData.slug}`}
                  title={blogData.title}
                >
                  <TwitterIcon size={25} round />
                </TwitterShareButton>
                <WhatsappShareButton
                  url={`https://collegevidya.com/blog/${blogData.slug}`}
                  title={blogData.title}
                  separator=":: "
                >
                  <WhatsappIcon size={25} round />
                </WhatsappShareButton>
                <LinkedinShareButton
                  url={`https://collegevidya.com/blog/${blogData.slug}`}
                >
                  <LinkedinIcon size={25} round />
                </LinkedinShareButton>
              </div>
              {blogData.podcasts_url ? (
                <div
                  onClick={() => setupPodcastModal(blogData.podcasts_url)}
                  className="d-flex align-items-center gap-1 mb-3 bg-primary px-3 py-2 rounded shadow"
                  role="button"
                >
                  <span className="text-white">Listen Podcast</span>{" "}
                  <img
                    src={"/blog/Images/icons/microphone.png"}
                    style={{ width: "30px" }}
                  />
                </div>
              ) : null}
            </div>

            {/* <p className="mt-3">
              <Image
                src={"/blog/Images/icons/audio-iocn.webp"}
                width={30}
                height={30}
                alt="audio"
              />{" "}
              Listen us
            </p>
            <audio
              src={blogData.podcasts}
              controls
              className="w-100 mb-3"
            ></audio> */}
            <UgcNotice screenSize={screenSize} />

            <div className="d-flex flex-wrap align-items-center justify-content-between">
              {tocHeads.length > 0 ? (
                <div
                  style={{
                    backgroundColor: "aliceblue",
                    width: "max-content",
                    maxWidth: "100%",
                    border: "1px solid #aaa",
                  }}
                  className="px-3 py-2 rounded mt-2 mb-3"
                >
                  <h6 className="mb-0 d-flex justify-content-between align-items-center fw-bold">
                    Table of Contents{" "}
                    <span
                      className="ms-3 cursor-pointer"
                      onClick={() => setStatus(!status)}
                    >
                      <Image
                        src={"/blog/Images/icons/text-indent-left.svg"}
                        width={27}
                        height={27}
                        alt="table of content"
                      />
                    </span>
                  </h6>

                  {status ? (
                    <ul className="list-unstyled ps-1 pt-3">
                      {tocHeads.map((head) =>
                        head.title ? (
                          <>
                            <li key={head.id} className="mb-3 d-flex">
                              <span className="me-2"> - </span>{" "}
                              <span
                                onClick={() => startScroll(head.id)}
                                style={{ color: "#444" }}
                                className="fs-16 text-dark cursor-pointer"
                              >
                                {head.title}
                              </span>
                            </li>
                          </>
                        ) : null
                      )}
                    </ul>
                  ) : null}
                </div>
              ) : null}
            </div>
            <div id="blog_wrapper_new">
              <div
                id="blog_wrapper"
                dangerouslySetInnerHTML={{
                  __html: blogData.content,
                }}
              />
              {blogData.faqs.length > 0 ? (
                <h2 className="mt-5 h4 mb-3 bold-font">
                  FAQs (Frequently Asked Questions)
                </h2>
              ) : null}
            </div>

            <Accordion>
              {blogData.faqs.map((faq) => (
                <Accordion.Item key={faq.id} eventKey={faq.id}>
                  <Accordion.Header>⭐ {faq.title}</Accordion.Header>
                  <Accordion.Body>
                    <div dangerouslySetInnerHTML={{ __html: faq.content }} />
                  </Accordion.Body>
                </Accordion.Item>
              ))}
            </Accordion>

            <AuthorCard authorData={blogData.author_blog} />

            <h2 className="h4 mt-5 bold-font">Every query is essential.</h2>
            <p className="fs-16 text-dark mb-3">
              Our team of experts, or experienced individuals, will answer it
              within 24 hours.
            </p>
            <p className="m-0">
              <Link
                target="_blank"
                className="d-inline-block fw-bold fs-14 text-white rounded-pill px-3 py-2 bgprimary shadow-sm border border-primary text-nowrap"
                href={"https://collegevidya.com/question-answer/"}
              >
                {" "}
                Ask any Question - CV Forum
              </Link>
            </p>

            <h2 className="h4 mt-5 bold-font">Recommended for you</h2>
            <LazyLoad offset={50}>
              <Row className="mt-3 row-cols-1 row-cols-md-2 row-cols-lg-3 mb-4">
                {recentBlogs.slice(0, 3).map((blog) => (
                  <Col key={blog.id}>
                    <Link href={`/${blog.slug}`} className="text-dark">
                      <div className="pb-3 mb-3">
                        <Image
                          src={blog.banner ? blog.banner : blog.image}
                          className="rounded"
                          layout="responsive"
                          alt=""
                          width={300}
                          height={150}
                          priority={false}
                        />
                        <p className="mt-3 px-3 textwrap textline_two">
                          {blog.title_h_one ? blog.title_h_one : blog.title}
                        </p>
                      </div>
                    </Link>
                  </Col>
                ))}
              </Row>
            </LazyLoad>
          </Col>
          <Col className="col-12 col-xl-3">
            <div className="border rounded p-3 mb-3 bg-white shadow-sm">
              <p
                className="mb-2 text-primary text-center"
                style={{ fontSize: "12px" }}
              >
                Tired of dealing with call centers!
              </p>
              <p className="fw-bold text-center" style={{ fontSize: "14px" }}>
                Get a professional advisor for Career!
              </p>

              <p
                className="fw-bold text-center m-0"
                style={{ fontSize: "20px" }}
              >
                LIFETIME FREE
              </p>
              <p className="fw-bold text-center" style={{ fontSize: "14px" }}>
                <del>Rs.1499</del>
                <span className="ms-2 text-success">
                  (Exclusive offer for today)
                </span>
              </p>

              {blog_member.map((list, index) => (
                <div
                  key={index}
                  className="d-flex justify-content-between gap-3 py-2 px-2 rounded mb-3"
                  role="button"
                  style={{ backgroundColor: "aliceblue" }}
                  onClick={handleShow1}
                >
                  <div className="d-flex align-items-center  gap-2">
                    <div>
                      <img
                        src={list.img}
                        style={{ width: "50px", height: "50px" }}
                      />
                    </div>
                    <div>
                      <p className="m-0 text-primary">{list.name}</p>
                      <p
                        className="mb-1 text-secondary"
                        style={{ fontSize: "12px" }}
                      >
                        {list.exp}
                      </p>
                      <RatingStar value={list.rating} />
                    </div>
                  </div>
                  <div className="d-flex align-items-center me-1">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      className="bi bi-arrow-right"
                      viewBox="0 0 16 16"
                    >
                      <path
                        fillRule="evenodd"
                        d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"
                      />
                    </svg>
                  </div>
                </div>
              ))}
              <p className="text-center">or</p>
              <div
                className="bgprimary text-white px-3 py-3 rounded text-center d-flex align-items-center gap-2"
                role="button"
                style={{ fontSize: "14px" }}
                onClick={handleShow1}
              >
                <GroupImage />
                GET A CALL BACK
              </div>
            </div>
            <CareerFinderCard />
          </Col>
        </Row>
      </Container>

      <Footer
        allCategories={allCategories}
        search={search}
        setSearch={setSearch}
        menuData={menuData}
        homepagereels={homepagereels}
      />

      <input ref={gtmRef} id="BLOG-FORM" type={"hidden"} />

      <Modal
        size="md"
        show={showPodcastModal}
        onHide={() => setShowPodcastModal(false)}
        centered
        className="overflow-hidden"
      >
        <Modal.Header closeButton>Listen Podcast</Modal.Header>
        <Modal.Body className="p-0">{podcastModalContent}</Modal.Body>
      </Modal>

      <Modal
        size="md"
        show={showLeadForm}
        onHide={() => setShowLeadForm(false)}
        centered
        className="overflow-hidden"
        style={{ zIndex: "9999", backgroundColor: "rgba(0,0,0,0.5)" }}
      >
        <Modal.Body className="p-0">{modalContent}</Modal.Body>
      </Modal>

      <Modal
        show={show}
        onHide={handleClose}
        fullscreen
        scrollable
        style={{ zIndex: "9999" }}
        enforceFocus={false}
      >
        <Modal.Body
          className="text-center position-relative pt-5"
          style={{ paddingBottom: "85px" }}
        >
          <Image
            className="position-absolute end-0 top-0 cursor-pointer me-5 mt-5"
            onClick={handleClose}
            style={{ zIndex: "1111" }}
            src={"/blog/Images/icons/close-dark.svg"}
            width="40"
            height="40"
            alt="close"
          />

          <div className="pt-5 mt-5">
            {/* <Image
              src={"/blog/Images/ond/ond_logo.png"}
              width={100}
              height={100}
              alt="rohit"
            /> */}
            <Image
              src={"/blog/Images/rohit_and_aman.webp"}
              width={100}
              height={100}
              alt="rohit"
            />
            <div className="d-block">
              <div>
                <div className="mb-3 d-inline-block mt-3">
                  {/* <p
                      className={`bg-primary text-white px-2 py-1 rounded mb-0 ${
                        screenSize && screenSize.width < 768 ? "fs-12" : "fs-14"
                      }`}
                    >
                      Online Education ke <b>Big Billion Days</b>
                    </p> */}
                  <p
                    style={{ backgroundColor: "#f75d34" }}
                    className={`text-white px-2 py-1 rounded mb-0 ${screenSize && screenSize.width < 768 ? "fs-12" : "fs-14"
                      }`}
                  >
                    <Image
                      quality={100}
                      priority
                      // className="bg-white rounded"
                      src={"/blog/Images/giftimg.png"}
                      width={18}
                      height={18}
                      alt="refer"
                    /> {blogData.categories[0].ptimer_des &&
                      blogData.categories[0].ptimer_des !== "" ? blogData.categories[0].ptimer_des : `Guaranteed CV Subsidy Cashback upto ${cashbackMaxAmount.get()}`}
                  </p>
                  <Timer timerText={blogData.categories[0].ptimer_des &&
                    blogData.categories[0].ptimer_des !== "" ? blogData.categories[0].ptimer_des : `Guaranteed CV Subsidy Cashback upto ${cashbackMaxAmount.get()}`} />
                </div>
              </div>
            </div>
            <div
              role="button"
              onClick={() => checkLogin(
                blogData.categories[0].psmu_link
                  ? blogData.categories[0].psmu_link
                  : "https://collegevidya.com/suggest-me-an-university/?utm_source=blog&utm_medium=popup&utm_campaign=neil_popup&utm_content=lead"
              )}
              style={{ backgroundColor: "#f75d34" }}
              className="d-inline-block mb-2 text-white rounded delay-my-popup text-white fs-14 px-2 py-2"
            >
              {/* <Image
                src={"/blog/Images/ond/off.png"}
                width={200}
                height={55}
                alt="subsidy"
              /> */}
              {blogData.categories[0].psmu_heading
                ? blogData.categories[0].psmu_heading
                : "Enroll Now"}{" "}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-arrow-right"
                viewBox="0 0 16 16"
              >
                <path
                  fill-rule="evenodd"
                  d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8"
                />
              </svg>
            </div>
            <h3 className="fw-bold mb-2 mt-2">
              {blogData.categories[0].p_title
                ? blogData.categories[0].p_title
                : "DID YOU KNOW?"}
            </h3>

            {blogData.categories[0].pdyn_des ? (
              <p
                className="mb-4"
                dangerouslySetInnerHTML={{
                  __html: blogData.categories[0].pdyn_des,
                }}
              />
            ) : (
              <p className="mb-4">
                Now you can get the right guidance to select the right
                university for your desired course. <br /> <strong>How?</strong>{" "}
                With our{" "}
                <span style={{ color: "#0056d2", fontSize: "18px" }}>
                  AI-Based technology{" "}
                </span>{" "}
                that gives you the right university according to your
                preferences.
              </p>
            )}

            <Row className="mb-4">
              <Col className="col-12 col-md-5 mx-auto">
                <UniversityFormSliderMobile />
              </Col>
            </Row>

            {/* <p
              className={`mb-1 ${
                screenSize && screenSize.width < 768 ? "fs-12" : "fs-14"
              }`}
            >
              Take admission to any Online Degree Program
            </p>
            <p
              className={`mb-1 ${screenSize && screenSize.width < 768 ? "fs-12" : "fs-14"
                }`}
            >
              Compare 100+ Online Universities & find the best!
            </p> */}

            <div>
              {blogData.categories[0].psmu_des ? (
                <p
                  className="d-inline-block px-3 py-3 rounded"
                  style={{ backgroundColor: "#eee" }}
                  dangerouslySetInnerHTML={{
                    __html: blogData.categories[0].psmu_des,
                  }}
                />
              ) : (
                <p
                  className="d-inline-block px-3 py-3 rounded"
                  style={{ backgroundColor: "#eee" }}
                >
                  Oh and don&apos;t forget you can{" "}
                  <span className="fw-bold">compare 50+</span> top online
                  university in seconds. <br /> Today is your day to get the
                  right university in seconds
                </p>
              )}
            </div>
          </div>
        </Modal.Body>
      </Modal>

      <Modal
        className="px-0"
        show={show1}
        onHide={handleClose1}
        centered
        size="lg"
        style={{ zIndex: "9999" }}
      >
        {/* <Modal.Header closeButton className="border-0"></Modal.Header> */}
        <Modal.Body className="p-0">
          <Row>
            <Col
              className="d-none d-lg-flex col-5 py-4 rounded-start d-flex flex-column justify-content-between"
              style={{ backgroundColor: "#fafafa" }}
            >
              <div>
                <p className="ps-2 mb-3" style={{ fontSize: "15px" }}>
                  <h4 className="mb- mt-4">Talk with us!</h4>
                  <img
                    className="me-2"
                    src={"/blog/Images/icons/hand-shake.png"}
                    width={15}
                    height={15}
                    alt="hand-shake"
                  />{" "}
                  Connected with{" "}
                  <span className="fw-bold text-success"> 165 people</span>
                </p>
                <p className="ps-2 mb-3" style={{ fontSize: "15px" }}>
                  <img
                    className="me-2"
                    src={"/blog/Images/icons/language.png"}
                    width={15}
                    height={15}
                    alt="hand-shake"
                  />{" "}
                  Speaks English, Hindi and{" "}
                  <span className="fw-bold">more</span>
                </p>
                <p className="ps-2 mb-3" style={{ fontSize: "15px" }}>
                  <img
                    className="me-2"
                    src={"/blog/Images/icons/eye.png"}
                    width={15}
                    height={15}
                    alt="hand-shake"
                  />{" "}
                  Viewed by <span className="fw-bold">5873+ people</span>
                </p>
                <UniversityFormSlider />
              </div>

              <div className="mt-4 text-center d-flex justify-content-center align-items-center text-center">
                {/* <SwipeImage/> */}
                <BlogSlideImage />
              </div>
            </Col>

            <Col className="col-12 col-lg-7">
              <div className="text-end">
                <svg
                  onClick={handleClose1}
                  role="button"
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className="bi bi-x-lg me-3 mt-3"
                  viewBox="0 0 16 16"
                >
                  <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z" />
                </svg>
              </div>
              <Form
                className="px-3 py-4 rounded contact_form"
                onSubmit={handleSubmit(onSubmit)}
              >
                <p
                  className="fw-bold mb-3 text-dark"
                  style={{ fontSize: "20px" }}
                >
                  Good choice! Let’s set up your first call!
                </p>

                <div className="mb-3 d-block d-lg-none">
                  <UniversityFormSliderMobile />
                </div>
                {/* <Form.Group className="d-flex gap-3 align-items-center mb-4 position-relative">
                  <Form.Check
                    type="radio"
                    label="Male"
                    id="Male"
                    value={"male"}
                    checked={getValues("gender") === "male"}
                    onChange={(e) =>
                      setValue("gender", e.target.value, {
                        shouldValidate: true,
                      })
                    }
                  />

                  <Form.Check
                    type="radio"
                    label="Female"
                    id="Female"
                    value={"female"}
                    checked={getValues("gender") === "female"}
                    onChange={(e) =>
                      setValue("gender", e.target.value, {
                        shouldValidate: true,
                      })
                    }
                  />
                  <span className="p-error text-xs d-inline-block">
                    {errors.gender?.message}
                  </span>
                </Form.Group> */}

                <Form.Group
                  className="mb-4 position-relative"
                  controlId="formBasicEmail"
                >
                  <Form.Control
                    {...register("full_name")}
                    type="text"
                    placeholder="Enter Name"
                    size="lg"
                  />
                  <span className="p-error text-xs">
                    {errors.full_name?.message}
                  </span>
                </Form.Group>

                <Row>
                  <Col className="col-12 col-md-6 pe-md-1">
                    <Form.Group className="mb-4 position-relative">
                      <Dropdown
                        appendTo={"self"}
                        panelStyle={{ maxWidth: "100%" }}
                        options={[
                          {
                            id: "male",
                            name: "Male",
                          },
                          {
                            id: "female",
                            name: "Female",
                          },
                          {
                            id: "other",
                            name: "Other",
                          },
                        ]}
                        value={getValues("gender")}
                        onChange={(e) =>
                          setValue("gender", e.value, { shouldValidate: true })
                        }
                        className={`w-100 ${errors.gender ? "p-invalid block" : ""
                          }`}
                        optionValue="id"
                        optionLabel="name"
                        placeholder="Select a Gender"
                      />
                      <span className="p-error text-xs">
                        {errors.gender?.message}
                      </span>
                    </Form.Group>
                  </Col>
                  <Col className="col-12 col-md-6 ps-md-1">
                    {" "}
                    <Form.Group
                      className="mb-4 position-relative"
                      controlId="formBasicPassword"
                    >
                      <Form.Control
                        type="text"
                        placeholder="Date of Birth"
                        onClick={(e) => {
                          e.currentTarget.type = "date";
                        }}
                        onFocus={(e) => {
                          e.currentTarget.type = "date";
                        }}
                        onMouseEnter={(e) => {
                          e.currentTarget.type = "date";
                        }}
                        max={new Date().toISOString().split("T")[0]}
                        {...register("dob")}
                      />
                      <span className="p-error text-xs">
                        {errors.dob?.message}
                      </span>
                    </Form.Group>
                  </Col>
                </Row>

                <InputGroup className={`flag_dropdown mb-4 position-relative`}>
                  <CountryDropDown setValue={setValue} />
                  <Form.Control
                    type="text"
                    placeholder="Mobile Number"
                    size="lg"
                    maxLength={10}
                    {...register("mobile_number")}
                  />

                  {!errors.mobile_number && (
                    <span
                      className="position-absolute end-0 me-2 px-2 rounded-bottom text-success fw-bold"
                      style={{
                        bottom: "-16px",
                        backgroundColor: "#d4fee8",
                        fontSize: "11px",
                      }}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="12"
                        height="12"
                        fill="currentColor"
                        className="bi bi-shield-fill-check"
                        viewBox="0 0 16 16"
                      >
                        <path
                          fillRule="evenodd"
                          d="M8 0c-.69 0-1.843.265-2.928.56-1.11.3-2.229.655-2.887.87a1.54 1.54 0 0 0-1.044 1.262c-.596 4.477.787 7.795 2.465 9.99a11.777 11.777 0 0 0 2.517 2.453c.386.273.744.482 1.048.625.28.132.581.24.829.24s.548-.108.829-.24a7.159 7.159 0 0 0 1.048-.625 11.775 11.775 0 0 0 2.517-2.453c1.678-2.195 3.061-5.513 2.465-9.99a1.541 1.541 0 0 0-1.044-1.263 62.467 62.467 0 0 0-2.887-.87C9.843.266 8.69 0 8 0zm2.146 5.146a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647z"
                        />
                      </svg>{" "}
                      We don&apos;t spam{" "}
                    </span>
                  )}

                  <span className="p-error text-xs">
                    {errors.mobile_number?.message}
                  </span>
                </InputGroup>

                <Row>
                  <Col className="col-12 col-md-6 pe-md-1">
                    <Form.Group
                      className="mb-4 position-relative"
                      controlId="formBasicPassword"
                    >
                      <Form.Control
                        type="email"
                        placeholder="Email"
                        size="lg"
                        {...register("email")}
                      />
                      {!errors.email && (
                        <span
                          className="position-absolute end-0 me-1 px-1 rounded-bottom text-success fw-bold"
                          style={{
                            bottom: "-16px",
                            backgroundColor: "#d4fee8",
                            fontSize: "11px",
                          }}
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="12"
                            height="12"
                            fill="currentColor"
                            className="bi bi-shield-fill-check"
                            viewBox="0 0 16 16"
                          >
                            <path
                              fillRule="evenodd"
                              d="M8 0c-.69 0-1.843.265-2.928.56-1.11.3-2.229.655-2.887.87a1.54 1.54 0 0 0-1.044 1.262c-.596 4.477.787 7.795 2.465 9.99a11.777 11.777 0 0 0 2.517 2.453c.386.273.744.482 1.048.625.28.132.581.24.829.24s.548-.108.829-.24a7.159 7.159 0 0 0 1.048-.625 11.775 11.775 0 0 0 2.517-2.453c1.678-2.195 3.061-5.513 2.465-9.99a1.541 1.541 0 0 0-1.044-1.263 62.467 62.467 0 0 0-2.887-.87C9.843.266 8.69 0 8 0zm2.146 5.146a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647z"
                            />
                          </svg>{" "}
                          We don&apos;t spam{" "}
                        </span>
                      )}

                      <span
                        className="p-error text-xs position-absolute"
                        style={{ bottom: "-18px" }}
                      >
                        {errors.email?.message}
                      </span>
                    </Form.Group>
                  </Col>
                  <Col className="col-12 col-md-6 ps-md-1">
                    <Form.Group className="mb-4 position-relative">
                      <Dropdown
                        appendTo={"self"}
                        panelStyle={{ maxWidth: "100%" }}
                        options={menuData
                          .map((domain) => domain.courses)
                          .flat()}
                        value={getValues("course")}
                        onChange={(e) =>
                          setValue("course", e.value, { shouldValidate: true })
                        }
                        className={`w-100 ${errors.course ? "p-invalid block" : ""
                          }`}
                        optionValue="id"
                        optionLabel="display_name"
                        filter
                        placeholder="Select a Course"
                      />
                      <span className="p-error text-xs">
                        {errors.course?.message}
                      </span>
                    </Form.Group>
                  </Col>
                </Row>

                {getValues("selected_country") === 101 ? (
                  <Row>
                    <Col className="col-12 col-md-6 pe-md-1">
                      {" "}
                      <Form.Group className="mb-4 position-relative">
                        <Dropdown
                          appendTo={"self"}
                          panelStyle={{ maxWidth: "100%" }}
                          options={states}
                          disabled={getValues("selected_country") !== 101}
                          value={getValues("state")}
                          onChange={(e) =>
                            setValue("state", e.value, { shouldValidate: true })
                          }
                          className={`w-100 ${errors.state ? "p-invalid block" : ""
                            }`}
                          optionValue="id"
                          optionLabel="state"
                          filter
                          placeholder="Select a State"
                        />
                        <span className="p-error text-xs">
                          {errors.state?.message}
                        </span>
                      </Form.Group>
                    </Col>

                    <Col className="col-12 col-md-6 ps-md-1">
                      <Form.Group className="mb-4 position-relative">
                        <Dropdown
                          appendTo={"self"}
                          panelStyle={{ maxWidth: "100%" }}
                          options={cities}
                          disabled={getValues("selected_country") !== 101}
                          value={getValues("city")}
                          onChange={(e) =>
                            setValue("city", e.value, { shouldValidate: true })
                          }
                          className={`w-100 ${errors.city ? "p-invalid block" : ""
                            }`}
                          optionValue="id"
                          optionLabel="city"
                          filter
                          placeholder="Select a City"
                        />
                        <span className="p-error text-xs">
                          {errors.city?.message}
                        </span>
                      </Form.Group>
                    </Col>
                  </Row>
                ) : null}

                {cashbackSwitch.get() && (
                  <CashbackCheckbox
                    screenSize={screenSize}
                    getValues={getValues}
                    setValue={setValue}
                    errors={errors}
                  />
                )}

                <div className="mb-0 text-center">
                  {" "}
                  <span
                    className="m-0 fw-bold text-center px-3 rounded-top py-1"
                    style={{
                      fontSize: "12px",
                      color: "#f75d34",
                      backgroundColor: "#faebd7",
                      borderTop: "2px solid #f75d34",
                    }}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      className="bi bi-repeat"
                      viewBox="0 0 16 16"
                    >
                      <path d="M11 5.466V4H5a4 4 0 0 0-3.584 5.777.5.5 0 1 1-.896.446A5 5 0 0 1 5 3h6V1.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384l-2.36 1.966a.25.25 0 0 1-.41-.192m3.81.086a.5.5 0 0 1 .67.225A5 5 0 0 1 11 13H5v1.466a.25.25 0 0 1-.41.192l-2.36-1.966a.25.25 0 0 1 0-.384l2.36-1.966a.25.25 0 0 1 .41.192V12h6a4 4 0 0 0 3.585-5.777.5.5 0 0 1 .225-.67Z" />
                    </svg>{" "}
                    {leadformButtonAboutText.get()}{" "}
                  </span>
                </div>
                <Button
                  disabled={loading}
                  variant="primary"
                  type="submit"
                  className="bgprimary w-100 py-2"
                >
                  {loading ? (
                    <>
                      <Spinner size="sm" animation="border" /> Submit
                    </>
                  ) : (
                    "Submit"
                  )}
                </Button>
                {/* <p
                  className="m-0 text-center mt-3 py-2"
                  style={{
                    fontSize: "13px",
                    lineHeight: "15px",
                    backgroundColor: "#d4fee8",
                  }}
                >
                  <Image
                    src={"/blog/Images/icons/lock-fill.svg"}
                    width={14}
                    height={14}
                    alt="key"
                  />{" "}
                  Your information is secure with us
                </p> */}
                <SecureMessage />
                <ConnectWithTop />
                <RemoveMessage />
              </Form>
            </Col>
          </Row>
        </Modal.Body>
      </Modal>

      <Modal
        style={{ zIndex: 99999, backgroundColor: "rgba(0,0,0,0.9)" }}
        show={showOTPModal}
        onHide={() => setShowOTPModal(false)}
        backdrop="static"
        centered
        className="overflow-hidden"
      >
        <Modal.Header className="border-0 justify-content-start position-relative ps-0">
          <Modal.Title
            id="contained-modal-title-vcenter"
            className="d-flex align-items-center pt-2"
          >
            <div className="d-flex align-items-center ps-3">
              <Logo />
              <p className="m-0" style={{ fontSize: "12px" }}>
                {tagLine.get()}
              </p>
            </div>
          </Modal.Title>
          <span
            role="button"
            className="position-absolute cursor-pointer"
            onClick={() => setShowOTPModal(false)}
            style={{ top: "8px", right: "16px" }}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="26"
              height="26"
              fill="currentColor"
              className="bi bi-x"
              viewBox="0 0 16 16"
            >
              <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708" />
            </svg>
          </span>
        </Modal.Header>
        <Modal.Body className="px-3 py-4">{otpModalContent}</Modal.Body>
      </Modal>

      {screenSize && screenSize.width > 991 ? (
        <div
          className={`position-fixed bottom-0 w-100 py-3 shadow ${scrollClass}`}
          style={{ zIndex: "1111", backgroundColor: "aliceblue" }}
        >
          <Container>
            <Row>
              <Col className="text-center">
                <Link
                  href="https://collegevidya.com/suggest-me-an-university/"
                  target="_blank"
                >
                  {" "}
                  <p className="text-center text-primary fw-bold mb-0 d-inline-block">
                    <img
                      src="/blog/Images/icons/apprentice.png"
                      alt="university"
                      style={{ width: "40px" }}
                    />{" "}
                    Get Best University in 2 mins - Compare on 30+ Factors{" "}
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      className="bi bi-arrow-right"
                      viewBox="0 0 16 16"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8"
                      />
                    </svg>
                  </p>
                </Link>
              </Col>
            </Row>
          </Container>
        </div>
      ) : null}
    </>
  );
};

export default Blogdetail;
