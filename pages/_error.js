import { useEffect } from "react";

function Error({ statusCode }) {
  useEffect(() => {
    window.location.href = "https://collegevidya.com/blog/404/?error=ErrorPage";
  }, []);

  return (
    <p>
      {statusCode
        ? `An error ${statusCode} occurred on server`
        : "An error occurred on client"}
    </p>
  );
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

export default Error;
