import { getAuthorSlugs, getBlogSlugs, getCategorySlugs } from "../services/BlogService";
import moment from "moment";

function generateSiteMap(blogSlugs, categorySlugs, authorSlugs) {
  return `<?xml version="1.0" encoding="UTF-8"?>
            <urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">
                <url>
                    <loc>https://collegevidya.com/blog/</loc>
                    <lastmod>${moment().format('YYYY-MM-DD')}T09:53:18.185Z</lastmod>
                    <changefreq>daily</changefreq>
                    <priority>0.9</priority>
                </url>
                <url>
                    <loc>https://collegevidya.com/blog/expert-interviews/</loc>
                    <lastmod>${moment().format('YYYY-MM-DD')}T09:53:18.185Z</lastmod>
                    <changefreq>daily</changefreq>
                    <priority>0.9</priority>
                </url>
                ${blogSlugs
      .map((slug) => {
        return `<url>
                  <loc>https://collegevidya.com/blog/${slug.slug}/</loc>
                  <lastmod>${moment().format('YYYY-MM-DD')}T09:53:18.185Z</lastmod>
                  <changefreq>daily</changefreq>
                  <priority>0.9</priority>
                </url>`;
      })
      .join("")}
                ${categorySlugs
      .map((slug) => {
        return `<url>
                    <loc>https://collegevidya.com/blog/category/${slug.slug}/</loc>
                    <lastmod>${moment().format('YYYY-MM-DD')}T09:53:18.185Z</lastmod>
                    <changefreq>daily</changefreq>
                    <priority>0.9</priority>
                </url>`;
      })
      .join("")}

                ${authorSlugs.map((slug) => {
        return `<url>
                  <loc>https://collegevidya.com/blog/author/${slug.slug}/</loc>
                  <lastmod>${moment().format('YYYY-MM-DD')}T09:53:18.185Z</lastmod>
                  <changefreq>daily</changefreq>
                  <priority>0.9</priority>
                </url>`
      })
      .join("")}
            </urlset>`;
}

function Sitemap() { }

export async function getServerSideProps({ res }) {
  const blogSlugs = await getBlogSlugs().then((response) => response.data.data);
  const categorySlugs = await getCategorySlugs().then(
    (response) => response.data
  );

  const authorSlugs = await getAuthorSlugs().then((response) => response.data);

  const sitemap = generateSiteMap(blogSlugs, categorySlugs, authorSlugs);

  res.setHeader("Content-Type", "text/xml");
  // we send the XML to the browser
  res.write(sitemap);
  res.end();

  return {
    props: {},
  };
}

export default Sitemap;
