import Head from "next/head";
import Image from "next/image";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Link from "next/link";
import Header from "../Components/Header";
import Footer from "../Components/Footer";
import {
  getBlogs,
  getCategoryBlogs,
  getCategorySlugs,
} from "../services/BlogService";
import { formatReadCount, getSearchSchema } from "../utils/misc";
import { getHomePageReels, getMenu } from "../services/MiscService";
import { useState } from "react";
import { flushSync } from "react-dom";
import Spinner from "react-bootstrap/Spinner";
import { isMobile } from "react-device-detect";
import LazyLoad from "react-lazy-load";
import moment from "moment";

export async function getStaticProps() {
  const topSixBlogs = await getBlogs(0, 6).then(
    (response) => response.data.data
  );

  const cvTalkBlogs = await getCategoryBlogs("cv-talks", 1, 4).then(
    (response) => response.data.data
  );

  const allCategories = await getCategorySlugs().then(
    (response) => response.data
  );

  const menuData = await getMenu().then((response) => response.data);

  const homepagereels = await getHomePageReels().then(
    (response) => response.data
  );

  return {
    props: {
      topSixBlogs: topSixBlogs,
      cvTalkBlogs: cvTalkBlogs,
      allCategories: allCategories,
      menuData: menuData,
      homepagereels: homepagereels,
    },
    revalidate: 60,
  };
}

export default function Home({
  cvTalkBlogs,
  topSixBlogs,
  allCategories,
  menuData,
  homepagereels
}) {
  const [search, setSearch] = useState(false);
  const [loadingBlogs, setLoadingBlogs] = useState(false);
  const [blogs, setBlogs] = useState([]);
  const [totalBlogs, setTotalBlogs] = useState(0);
  const [lowerLimit, setLowerLimit] = useState(6);
  const [upperLimit, setUpperLimit] = useState(12);

  function loadMore() {
    if (totalBlogs >= blogs.length) {
      setLoadingBlogs(true);
      flushSync(() => {
        setLowerLimit((minBlogs) => minBlogs + 6);
        setUpperLimit((maxBlogs) => maxBlogs + 6);
      });

      setLoadingBlogs(true);
      getBlogs(lowerLimit, upperLimit)
        .then((response) => {
          setBlogs((prev) => prev.concat(response.data.data));
          setTotalBlogs(response.data.total_blogs);
          setLoadingBlogs(false);
        })
        .catch(() => {
          setLoadingBlogs(false);
        });
    }
  }

  return (
    <div>
      <Head>
        <title>
          College Vidya Blog (Online Colleges, Courses, Reviews, Fees)
        </title>
        <link rel="canonical" href="https://collegevidya.com/blog/" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="College Vidya Blog (Online Colleges, Courses, Reviews, Fees)"
        />
        <meta
          name="description"
          content="Learn updated details about online colleges, courses, admission process, reviews, fees, career prospectus and more."
        />
        <meta property="og:url" content="https://collegevidya.com/blog/" />
        <meta
          property="og:site_name"
          content="College Vidya Blog (Online Colleges, Courses, Reviews, Fees)"
        />
        <meta name="twitter:card" content="summary_large_image" />

        <meta property="og:image" content={cvTalkBlogs[0]?.blogs.image} />
        <meta property="twitter:image" content={cvTalkBlogs[0]?.blogs.image} />

        {/* Search Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        {/* Schema One */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidya.com/blog/",
              name: "College Vidya Blog (Online Colleges, Courses, Reviews, Fees)",
              description:
                "Learn updated details about online colleges, courses, admission process, reviews, fees, career prospectus and more.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: [".seotag"],
              },
            }),
          }}
        />

        {/* Schema Two */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        {/* Breadcrumb Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "https://schema.org",
              "@type": "BreadcrumbList",
              itemListElement: [
                {
                  "@type": "ListItem",
                  position: 1,
                  item: {
                    "@id": "https://collegevidya.com/blog/",
                    name: "Blog",
                  },
                },
              ],
            }),
          }}
        />
      </Head>

      <Header menuData={menuData} search={search} />

      <Container className="mt-3 mt-sm-5 blog_listing">
        <h1 className="text-center h2 fw-bold">College Vidya Blog</h1>
        <p className="text-center">
          {" "}
          Updates on the <b>Latest Career Opportunities,</b> Online Education,
          Online Universities, & more
        </p>

        <Row className="mt-1 mt-sm-5">
          {cvTalkBlogs.slice(0, 1).map((blog) => (
            <Col key={blog.blogs.id} className="col-12 col-lg-6">
              <div>
                <Link className="text-dark" href={`/${blog.blogs.slug}`}>
                  <Image
                    className="rounded shadow-1"
                    src={
                      blog.blogs.banner ? blog.blogs.banner : blog.blogs.image
                    }
                    width={600}
                    height={300}
                    priority
                    alt={
                      blog.blogs.banner
                        ? blog.blogs.banner
                            .substring(blog.blogs.banner.lastIndexOf("/") + 1)
                            .replace(/\.[^\/.]+$/, "")
                            .replace(/-/g, " ")
                        : blog.blogs.image
                            .substring(blog.blogs.image.lastIndexOf("/") + 1)
                            .replace(/\.[^\/.]+$/, "")
                            .replace(/-/g, " ")
                    }
                    layout="responsive"
                    quality={isMobile ? 40 : 75}
                  />
                  <p className="h4 my-2 fw-bold blogcard_title bold-font">
                    {blog.blogs.title_h_one
                      ? blog.blogs.title_h_one
                      : blog.blogs.title}
                  </p>
                  <p style={{ fontSize: "12px" }}>
                    {" "}
                    Updated on : {moment(blog.blogs.updated_at).format("ll")}
                  </p>
                </Link>
              </div>
            </Col>
          ))}

          <Col className="col-12 col-lg-6 d-flex flex-wrap align-content-between pb-0 pb-sm-5 banner-part mt-3 mt-md-0">
            {cvTalkBlogs.slice(1, 4).map((blog) => (
              <Link
                href={`/${blog.blogs.slug}`}
                className="text-dark"
                key={blog.blogs.id}
              >
                <Row key={blog.blogs.id} className="mb-3">
                  <Col className="col-3">
                    <Image
                      className="rounded shadow-1"
                      src={
                        blog.blogs.banner ? blog.blogs.banner : blog.blogs.image
                      }
                      width={145}
                      height={80}
                      priority={isMobile ? false : true}
                      alt={
                        blog.blogs.banner
                          ? blog.blogs.banner
                              .substring(blog.blogs.banner.lastIndexOf("/") + 1)
                              .replace(/\.[^\/.]+$/, "")
                              .replace(/-/g, " ")
                          : blog.blogs.image
                              .substring(blog.blogs.image.lastIndexOf("/") + 1)
                              .replace(/\.[^\/.]+$/, "")
                              .replace(/-/g, " ")
                      }
                    />
                  </Col>
                  <Col>
                    <p className="h5 textwrap textline_two blogcard_title bold-font">
                      {blog.blogs.title_h_one
                        ? blog.blogs.title_h_one
                        : blog.blogs.title}
                    </p>
                    <p style={{ fontSize: "12px" }}>
                      Updated on : {moment(blog.blogs.updated_at).format("ll")}
                    </p>
                  </Col>
                </Row>
              </Link>
            ))}
          </Col>
        </Row>

        <LazyLoad offset={10}>
          <Row className="mt-0 mt-sm-0 row-cols-1 row-cols-sm-2 row-cols-lg-3">
            {topSixBlogs.map((blog) => (
              <Col key={blog.id} className="col mb-4">
                <div className="border rounded shadow-1">
                  <Link href={blog.slug} className="text-dark">
                    <Image
                      src={blog.banner ? blog.banner : blog.image}
                      width={300}
                      height={250}
                      priority={isMobile ? false : true}
                      layout="responsive"
                      alt={
                        blog.banner
                          ? blog.banner
                              .substring(blog.banner.lastIndexOf("/") + 1)
                              .replace(/\.[^\/.]+$/, "")
                              .replace(/-/g, " ")
                          : blog.image
                              .substring(blog.image.lastIndexOf("/") + 1)
                              .replace(/\.[^\/.]+$/, "")
                              .replace(/-/g, " ")
                      }
                    />
                    <div className="p-3">
                      <p className="textwrap textline_two fw-bold h4 blogcard_title bold-font">
                        {blog.title_h_one ? blog.title_h_one : blog.title}
                      </p>
                      <p
                        className="px-3 rounded textwrap textline_one d-inline-block my-2"
                        style={{
                          backgroundColor: "aliceblue",
                          fontSize: "13px",
                        }}
                      >
                        {blog.categories[0].name}
                      </p>
                      <p
                        className="d-flex justify-content-between mb-2 align-items-center"
                        style={{ fontSize: "13px" }}
                      >
                        <span className="d-flex align-items-center">
                          {moment(blog.updated_at).format("ll")}
                        </span>
                        <span className="d-flex align-items-center">
                          {formatReadCount(blog.blog_read)} Reads
                        </span>
                      </p>
                      <p
                        className="textwrap textline_two mb-0"
                        style={{ fontSize: "14px" }}
                      >
                        {blog.home_description}
                      </p>
                    </div>
                  </Link>

                  <p
                    style={{ fontSize: "12px", borderTop: "1px solid #ddd" }}
                    className="m-0 px-3 py-3 d-flex justify-content-between flex-wrap align-items-center"
                  >
                    <Link href={`/author/${blog.author_blog.slug}/`}>
                      <img
                        className="rounded-circle"
                        src={blog.author_blog.profile_image}
                        style={{ width: "25px", height: "25px",objectFit:"cover" }}
                      />{" "}
                      <span className="text-primary">
                        {blog.author_blog.name}
                      </span>{" "}
                    </Link>
                    <span className="text-success d-flex gap-1">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        class="bi bi-shield-fill-check"
                        viewBox="0 0 16 16"
                      >
                        <path
                          fill-rule="evenodd"
                          d="M8 0c-.69 0-1.843.265-2.928.56-1.11.3-2.229.655-2.887.87a1.54 1.54 0 0 0-1.044 1.262c-.596 4.477.787 7.795 2.465 9.99a11.8 11.8 0 0 0 2.517 2.453c.386.273.744.482 1.048.625.28.132.581.24.829.24s.548-.108.829-.24a7 7 0 0 0 1.048-.625 11.8 11.8 0 0 0 2.517-2.453c1.678-2.195 3.061-5.513 2.465-9.99a1.54 1.54 0 0 0-1.044-1.263 63 63 0 0 0-2.887-.87C9.843.266 8.69 0 8 0m2.146 5.146a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793z"
                        />
                      </svg>{" "}
                      Verified <span className="d-none d-md-block">Expert</span>
                    </span>
                  </p>
                </div>
              </Col>
            ))}
            {blogs.map((blog) => (
              <Col key={blog.id} className="col mb-4">
                <div className="border rounded shadow-1">
                  <Link href={blog.slug} className="text-dark">
                    {blog.banner || blog.image ? (
                      <Image
                        src={blog.banner ? blog.banner : blog.image}
                       
                        width={300}
                        height={200}
                        priority={isMobile ? false : true}
                        layout="responsive"
                        alt={
                          blog.banner
                            ? blog.banner
                                .substring(blog.banner.lastIndexOf("/") + 1)
                                .replace(/\.[^\/.]+$/, "")
                                .replace(/-/g, " ")
                            : blog.image
                                .substring(blog.image.lastIndexOf("/") + 1)
                                .replace(/\.[^\/.]+$/, "")
                                .replace(/-/g, " ")
                        }
                      />
                    ) : (
                      <Image
                        src={"/blog/Images/default-thumbnail.png"}
                        width={300}
                        height={200}
                        priority={isMobile ? false : true}
                        layout="responsive"
                        alt="default image"
                      />
                    )}

                    <div className="p-3">
                      <p className="textwrap textline_two fw-bold h3 blogcard_title h5 bold-font">
                        {blog.title_h_one ? blog.title_h_one : blog.title}
                      </p>
                      <p
                        className="px-3 rounded textwrap textline_one d-inline-block my-2"
                        style={{
                          backgroundColor: "aliceblue",
                          fontSize: "13px",
                        }}
                      >
                        {blog.categories[0].name}
                      </p>
                      <p
                        className="d-flex justify-content-between mb-2 align-items-center"
                        style={{ fontSize: "13px" }}
                      >
                        <span className="d-flex align-items-center">
                          {moment(blog.updated_at).format("ll")}
                        </span>
                        <span className="d-flex align-items-center">
                          {formatReadCount(blog.blog_read)} Reads
                        </span>
                      </p>
                      <p
                        className="textwrap textline_two mb-0"
                        style={{ fontSize: "14px" }}
                      >
                        {blog.description}
                      </p>
                    </div>
                  </Link>
                  <p
                    style={{ fontSize: "12px", borderTop: "1px solid #ddd" }}
                    className="m-0 px-3 py-3 d-flex justify-content-between flex-wrap align-items-center"
                  >
                    <Link href={`/author/${blog.author_blog.slug}/`}>
                      <img
                        className="rounded-circle"
                        src={blog.author_blog.profile_image}
                        style={{ width: "25px", height: "25px",objectFit:"cover" }}
                      />{" "}
                      <span className="text-primary">
                        {blog.author_blog.name}
                      </span>{" "}
                    </Link>
                    <span className="text-success d-flex gap-1">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        class="bi bi-shield-fill-check"
                        viewBox="0 0 16 16"
                      >
                        <path
                          fill-rule="evenodd"
                          d="M8 0c-.69 0-1.843.265-2.928.56-1.11.3-2.229.655-2.887.87a1.54 1.54 0 0 0-1.044 1.262c-.596 4.477.787 7.795 2.465 9.99a11.8 11.8 0 0 0 2.517 2.453c.386.273.744.482 1.048.625.28.132.581.24.829.24s.548-.108.829-.24a7 7 0 0 0 1.048-.625 11.8 11.8 0 0 0 2.517-2.453c1.678-2.195 3.061-5.513 2.465-9.99a1.54 1.54 0 0 0-1.044-1.263 63 63 0 0 0-2.887-.87C9.843.266 8.69 0 8 0m2.146 5.146a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793z"
                        />
                      </svg>{" "}
                      Verified <span className="d-none d-md-block">Expert</span>
                    </span>
                  </p>
                </div>
              </Col>
            ))}
          </Row>
        </LazyLoad>

        {totalBlogs >= blogs.length ? (
          loadingBlogs ? (
            <div className="text-center disabled">
              <p
                className="rounded d-inline-block px-3 py-2 text-white"
                style={{ backgroundColor: "#0056d2", cursor: "not-allowed" }}
              >
                <Spinner size="sm" animation="border" /> Load More
              </p>
            </div>
          ) : (
            <div
              onClick={() => loadMore()}
              className="text-center cursor-pointer"
            >
              <p
                className="rounded d-inline-block px-3 py-2 text-white"
                style={{ backgroundColor: "#0056d2" }}
              >
                Load More
              </p>
            </div>
          )
        ) : null}
      </Container>

      <Footer
        allCategories={allCategories}
        search={search}
        setSearch={setSearch}
        menuData={menuData}
        homepagereels={homepagereels}
      />
    </div>
  );
}
