import React, { useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "next/image";
import Modal from "react-bootstrap/Modal";
import { getHomePageReels, getMenu } from "../services/MiscService";
import Header from "../Components/Header";
import Footer from "../Components/Footer";
import { getCategorySlugs } from "../services/BlogService";
import BelowHeader from "../Components/BelowHeader";
import Head from "next/head";
import { getSearchSchema } from "../utils/misc";

export async function getStaticProps(context) {
  const allCategories = await getCategorySlugs().then(
    (response) => response.data
  );

  const menuData = await getMenu().then((response) => response.data);
  const homepagereels = await getHomePageReels().then(
    (response) => response.data
  );

  return {
    props: {
      allCategories: allCategories,
      menuData: menuData,
      homepagereels: homepagereels
    },
    revalidate: 60,
  };
}

const Expert = ({ allCategories, menuData, categorySlug, categoryBlogs, homepagereels }) => {
  const [search, setSearch] = useState(false);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = (title, video) => {
    setSelectedTitle(title);
    setSelectedVideo(video);
    setShow(true);
  };

  const [selectedTitle, setSelectedTitle] = useState(null);
  const [selectedVideo, setSelectedVideo] = useState(null);
  const alllist = [
    {
      image: `https://img.youtube.com/vi/n1KzZYIJsl4/hqdefault.jpg`,
      title:
        "The Real Truth of Online B.Tech & M.Tech! Must Watch before Enrolling in Online B.Tech & M.Tech",
      video: "https://www.youtube.com/embed/n1KzZYIJsl4",
      description:
        "We are here again with an informative Interview video from CEO of College Vidya Mr. Mayank Gupta, on “Experts Ki Raye”.",
    },
    {
      image: `https://img.youtube.com/vi/1usW1A0OiK4/hqdefault.jpg`,
      title:
        "Tips to Crack Interview straight from the HR: Career Advice | 'Experts Ki Raye' | Work Profile",
      video: "https://www.youtube.com/embed/1usW1A0OiK4",
      description:
        "In Today’s video of the College Vidya series “Experts ki Raye,” we come up with the most insightful interview as it is with the HR Manager, who has experience of over 20+ years and worked in different companies.",
    },
    {
      image: `https://img.youtube.com/vi/FoybcvL6VIg/hqdefault.jpg`,
      title:
        "Why choose Manipal University Online Education? Q&A With Director | 'Experts Ki Raye' | Doubts Clear",
      video: "https://www.youtube.com/embed/FoybcvL6VIg",
      description:
        " “Interview with the director of Manipal University Jaipur Online Education” What else you can ever ask for if you get all your queries resolved by the experts.",
    },
    {
      image: `https://img.youtube.com/vi/uf9ZCHQc6ZU/hqdefault.jpg`,
      title:
        "JAIN Online Q & A Session: “Experts Ki Raye”| Online Education Problems | Placement | Career| Future",
      video: "https://www.youtube.com/embed/uf9ZCHQc6ZU",
      description:
        "We are glad to share that we again have come up with the new session of “Experts Ki Raye”. Today we have our guest from JAIN online, JAIN Online JAIN (Deemed-to-be University).",
    },
    {
      image: `https://img.youtube.com/vi/_RjkUxWUu7E/hqdefault.jpg`,
      title:
        "Sumanth Palepu:IIM Alumni & Business Head of UPES CCE Exclusive Interview| Experts ki Raye| Q & A",
      video: "https://www.youtube.com/embed/_RjkUxWUu7E",
      description:
        "As you all know every time we call new guest to our show to enlighten you with the reality and future of online education and at the same time give you the insights from real-time industry experts. ",
    },
    {
      image: `https://img.youtube.com/vi/K492AGPY4eY/hqdefault.jpg`,
      title:
        "Why Choose LPU Online? Insights by Senior Director Ginni Nijhawan| Q&A Session| 'Experts ki Raye'",
      video: "https://www.youtube.com/embed/K492AGPY4eY",
      description:
        "We have another episode of “Experts Ki Raye”. You all will be amazed to see that this time our guest is no other than the Senior Director of LPU online Mrs. Ginni Nijhawan. She has a total experience of 18+ years and that too in Lovely Professional University only. ",
    },
    {
      image: `https://img.youtube.com/vi/rU3QnI6qFsI/hqdefault.jpg`,
      title:
        "B.Voc Course Complete Details: Is it a Valid Degree? Career| Future| package| Advantages",
      video: "https://www.youtube.com/embed/rU3QnI6qFsI",
      description:
        "This video has covered all the doubts students have related to B.Voc program as what exactly is B.voc program, its different fields, its approvals, and its advantages.",
    },
    {
      image: `https://img.youtube.com/vi/KJjqUDafKgc/hqdefault.jpg`,
      title:
        "Interview: Dr. D.Y. Patil Vidyapeeth Online| 'Experts ki Raye'| Dr. Smita Jadhav| Executive Director",
      video: "https://www.youtube.com/embed/KJjqUDafKgc",
      description:
        "This video has covered topics such as Multiple Entry & Exit System, New Education Policy, UGC New Guidelines, Digital Revolution, etc. For complete Details you must watch the complete video.",
    },
    {
      image: `https://img.youtube.com/vi/LXsBO_qqogo/hqdefault.jpg`,
      title:
        "Interview of CEO of College Vidya: Mr. Mayank Gupta| Online Education| Vision Behind College Vidya",
      video: "https://www.youtube.com/embed/LXsBO_qqogo",
      description:
        "Today's session is going to be very fruitful and informative as we have invited the founder of “College Vidya” Mr. Mayank Gupta.",
    },
    {
      image: `https://img.youtube.com/vi/RNPJx_4dtKQ/hqdefault.jpg`,
      title:
        "Know The Validity Of Online Degrees | CEO Of College Vidya (Must Watch)- Mr. Mayank Gupta",
      video: "https://www.youtube.com/embed/RNPJx_4dtKQ",
      description:
        "Students remain suspicious about the validity of online degrees that’s why Mr. Mayank Gupta, CEO of College Vidya, himself answered this question with honesty and put some light on University approvals. ",
    },
    {
      image: `https://img.youtube.com/vi/pVCIO6m76eE/hqdefault.jpg`,
      title:
        "Will I be able to Apply for Foreign Jobs after doing Online & Distance Education?| College Vidya",
      video: "https://www.youtube.com/embed/pVCIO6m76eE",
      description:
        " If you all are afraid of whether you get foreign jobs or higher education in Foreign after completing education from online & distance education? Then it is the one-stop solution for all your doubts from the highly experienced expert. ",
    },
    {
      image: `https://img.youtube.com/vi/OnAsWO1WPYg/hqdefault.jpg`,
      title:
        "Will online/distance word would be mentioned on my Degree? : Look for the Approvals| Don't worry",
      video: "https://www.youtube.com/embed/OnAsWO1WPYg",
      description:
        "This video will clear all your doubts and fear of getting the word of online & distance in your degree will affect your career or not.",
    },
    {
      image: `https://img.youtube.com/vi/8VBvLdDFC3U/hqdefault.jpg`,
      title:
        "Compare & Get the best online & distance university on just one platform: College Vidya",
      video: "https://www.youtube.com/embed/8VBvLdDFC3U",
      description:
        "If you're confused of searching the best online & distance university just because you don't have a valid platform. then now you have 'College Vidya' One stop solution for all your university selection problem.",
    },
    {
      image: `https://img.youtube.com/vi/WlNVfO5-Zjk/hqdefault.jpg`,
      title:
        "Last Warning from UGC To All The Ed-Tech Companies: Must Watch to Save Your Career!",
      video: "https://www.youtube.com/embed/WlNVfO5-Zjk",
      description:
        "This video is a kind of awareness for all the students who are not aware of the actual and correct way of gaining education because of which they get trapped in such schemes of Ed-Tech Companies. ",
    },
    {
      image: `https://img.youtube.com/vi/uaMglevWidw/hqdefault.jpg`,
      title:
        "The value of Digital Learning & Digital University | CNBC & College Vidya",
      video: "https://www.youtube.com/embed/uaMglevWidw",
      description:
        "CEO of College Vidya Mayank Gupta made an attempt to enlighten new age students about Digital University which is recently announced in the Budget 2022 at the platform of CNBC Awaaz. ",
    },
    {
      image: `https://img.youtube.com/vi/thAntb9AqDI/hqdefault.jpg`,
      title: "Expansion Of Digital Learning | CNBC & College Vidya",
      video: "https://www.youtube.com/embed/thAntb9AqDI",
      description:
        "In a recent discussion over Digital University which was recently mentioned in Budget 2022, CEO Of College Vidya Mayank Gupta shared his views at CNBC Awaaz.  ",
    },
  ];

  return (
    <>
      <Head>
        <title>
          Experts Interviews - Insights About Online Learning | CV Blog
        </title>
        <meta
          name="description"
          content="Find exclusive interviews about online education where our industry experts share their insights about online learning and their career growth In India."
        />
        <link
          rel="canonical"
          href={`https://collegevidya.com/blog/expert-interviews/`}
        />
        <meta property="og:locale" content="en_US" />
        <meta
          property="og:url"
          content={`https://collegevidya.com/blog/expert-interviews/`}
        />
        <meta property="og:site_name" content="College Vidya Blog" />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: `https://collegevidya.com/blog/expert-interviews/`,
              name: `Experts Interviews - Insights About Online Learning | CV Blog`,
              description:
                "Find exclusive interviews about online education where our industry experts share their insights about online learning and their career growth In India.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: [".seotag"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />
      </Head>
      <Header menuData={menuData} search={search} />
      <BelowHeader menuData={menuData} />
      {/* <div style={{ backgroundColor: "aliceblue" }} className="py-4 mb-5">
        <Container>
          <h1 className="h3 m-0 text-center">Experts Reviews</h1>
        </Container>
      </div> */}
      <Container className="d-flex justify-content-center mb-5 mt-5">
        <div
          className="d-inline-flex gap-2 px-3 py-3 rounded"
          style={{ backgroundColor: "#eee" }}
        >
          <div>
            <Image
              src="/blog/Images/icons/uparrow.webp"
              width={50}
              height={50}
              priority
              alt="up arrow"
            />
          </div>
          <div>
            <h1 className="text-center mb-0 h6 seotag">
              Do you want to know the lesser-known facts about Online Education?
              <br />
              Listen to Real-Life Experiences & Reviews by Academic Experts
            </h1>
          </div>
        </div>
      </Container>

      <Container>
        <Col className="col-11 mx-auto">
          <Row className="mt-5 mt-sm-0 row-cols-1 row-cols-sm-2 row-cols-lg-2">
            {alllist.map((list, index) => (
              <Col
                key={index}
                className={`col px-0 px-md-5 ${index % 2
                  ? "border-none mt-3 mt-md-5 pt-0 pt-md-5"
                  : "border-end"
                  }`}
              >
                <div
                  className="expert_card border rounded shadow-1 mb-5 cursor-pointer"
                  onClick={() => handleShow(list.title, list.video)}
                >
                  <div className="position-relative expert_div">
                    <img
                      className="rounded"
                      src={list.image}
                      width={`100%`}
                      height={250}
                      alt="banner"
                      style={{ objectFit: "cover" }}
                    />
                  </div>
                  <div className="p-3">
                    <p className="textwrap textline_two fw-bold h3 blogcard_title">
                      {list.title}
                    </p>
                    <p
                      className="px-3 rounded textwrap textline_one d-inline-block my-2"
                      style={{
                        backgroundColor: "aliceblue",
                        fontSize: "13px",
                      }}
                    >
                      {list.category}
                    </p>

                    <p className="textwrap textline_two">{list.description}</p>
                  </div>
                </div>
              </Col>
            ))}
          </Row>
        </Col>

        <Modal
          show={show}
          onHide={handleClose}
          size="lg"
          centered
          className="p-0"
        >
          <Modal.Body className="p-0 position-relative bg-transparent">
            <Image
              className="position-absolute end-0 cursor-pointer"
              onClick={handleClose}
              style={{ top: "-25px" }}
              src={"/blog/Images/icons/close.svg"}
              width={25}
              height={25}
              alt="close"
              priority
            />
            <iframe
              width="100%"
              height="450"
              src={selectedVideo}
              title="YouTube video player"
              frameborder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
          </Modal.Body>
        </Modal>
      </Container>
      <Footer
        allCategories={allCategories}
        search={search}
        setSearch={setSearch}
        menuData={menuData}
        homepagereels={homepagereels}
      />
    </>
  );
};

export default Expert;
