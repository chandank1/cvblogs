import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Link from "next/link";
import Image from "next/image";
import Footer from "../../Components/Footer";
import Header from "../../Components/Header";
import { getHomePageReels, getMenu } from "../../services/MiscService";
import { getCategoryBlogs, getCategorySlugs } from "../../services/BlogService";
import { formatReadCount, getSearchSchema } from "../../utils/misc";
import { useRouter } from "next/router";
import { flushSync } from "react-dom";
import Spinner from "react-bootstrap/Spinner";
import Head from "next/head";
import moment from "moment";
import { useState } from "@hookstate/core";
import store from "../../utils/store";

export async function getStaticPaths() {
  const allSlugs = await getCategorySlugs().then((response) => response.data);
  const paths = allSlugs.map((categorySlug) => {
    return {
      params: {
        categorySlug: categorySlug.slug,
      },
    };
  });

  return { paths, fallback: true };
}

export async function getStaticProps(context) {
  const categorySlug = context.params.categorySlug;
  const allCategories = await getCategorySlugs().then(
    (response) => response.data
  );

  const menuData = await getMenu().then((response) => response.data);

  const categoryBlogs = await getCategoryBlogs(categorySlug, 1, 6).then(
    (response) => response.data.data
  );

  const homepagereels = await getHomePageReels().then(
    (response) => response.data
  );

  return {
    props: {
      allCategories: allCategories,
      menuData: menuData,
      categorySlug: categorySlug,
      categoryBlogs: categoryBlogs,
      homepagereels: homepagereels,
    },
    revalidate: 60,
  };
}

const Category = ({
  allCategories,
  menuData,
  categorySlug,
  categoryBlogs,
  homepagereels,
}) => {
  const router = useRouter();
  const [search, setSearch] = React.useState(false);
  const [loadingBlogs, setLoadingBlogs] = React.useState(false);
  const [blogs, setBlogs] = React.useState([]);
  const [totalBlogs, setTotalBlogs] = React.useState(0);
  const [lowerLimit, setLowerLimit] = React.useState(7);
  const [upperLimit, setUpperLimit] = React.useState(12);
  const { universalLogo } = useState(store);

  function loadMore() {
    if (totalBlogs >= blogs.length) {
      setLoadingBlogs(true);
      flushSync(() => {
        setLowerLimit((minBlogs) => minBlogs + 6);
        setUpperLimit((maxBlogs) => maxBlogs + 6);
      });

      setLoadingBlogs(true);
      getCategoryBlogs(categorySlug, lowerLimit, upperLimit)
        .then((response) => {
          setBlogs((prev) => prev.concat(response.data.data));
          setTotalBlogs(response.data.blog_count);
          setLoadingBlogs(false);
        })
        .catch(() => {
          setLoadingBlogs(false);
        });
    }
  }

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <Head>
        <title>
          {
            allCategories.filter(
              (category) => category.slug === categorySlug
            )[0].title
          }
        </title>
        <meta
          name="description"
          content={
            allCategories.filter(
              (category) => category.slug === categorySlug
            )[0].meta_description
          }
        />
        <link
          rel="canonical"
          href={`https://collegevidya.com/blog/category/${
            allCategories.filter(
              (category) => category.slug === categorySlug
            )[0].slug
          }/`}
        />

        <meta
          property="og:url"
          content={`https://collegevidya.com/blog/category/${
            allCategories.filter(
              (category) => category.slug === categorySlug
            )[0].slug
          }/`}
        />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidya"
        />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content={
            allCategories.filter(
              (category) => category.slug === categorySlug
            )[0].title
          }
        />
        <meta
          property="twitter:description"
          content={
            allCategories.filter(
              (category) => category.slug === categorySlug
            )[0].meta_description
          }
        />
        <meta property="twitter:url" content="https://collegevidya.com/" />

        <meta property="og:image" content={universalLogo.get()} />
        <meta property="twitter:image" content={universalLogo.get()} />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: `https://collegevidya.com/blog/category/${
                allCategories.filter(
                  (category) => category.slug === categorySlug
                )[0].slug
              }/`,
              name: allCategories.filter(
                (category) => category.slug === categorySlug
              )[0].title,
              description: allCategories.filter(
                (category) => category.slug === categorySlug
              )[0].description,
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: [".seotag"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        {/* Breadcrumb Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "https://schema.org",
              "@type": "BreadcrumbList",
              itemListElement: [
                {
                  "@type": "ListItem",
                  position: 1,
                  item: {
                    "@id": "https://collegevidya.com/blog",
                    name: "Blog",
                  },
                },
                {
                  "@type": "ListItem",
                  position: 2,
                  item: {
                    "@id": `https://collegevidya.com/blog/category/${categorySlug}/`,
                    name: allCategories.filter(
                      (category) => category.slug === categorySlug
                    )[0].name,
                  },
                },
              ],
            }),
          }}
        />
      </Head>
      <Header menuData={menuData} search={search} />

      <div style={{ backgroundColor: "aliceblue" }} className="py-4 mb-4">
        <Container>
          <h1 className="h3 m-0 text-center">
            {
              allCategories.filter(
                (category) => category.slug === categorySlug
              )[0].title
            }
          </h1>
        </Container>
      </div>
      {allCategories.filter((category) => category.slug === categorySlug)[0]
        .description ? (
        <Container>
          <p className="text-center mb-4 bgprimary text-white px-2 py-3 rounded">
            {
              allCategories.filter(
                (category) => category.slug === categorySlug
              )[0].description
            }
          </p>
        </Container>
      ) : null}

      <Container>
        <Row className="mt-0 mt-sm-0 row-cols-1 row-cols-sm-2 row-cols-lg-3">
          {categoryBlogs.map((blog) => (
            <Col key={blog.blogs.id} className="col mb-4">
              <div className="border rounded shadow-1">
                <Link href={`/${blog.blogs.slug}`} className="text-dark">
                  <Image
                    src={
                      blog.blogs.banner ? blog.blogs.banner : blog.blogs.image
                    }
                    width={300}
                    height={200}
                    priority
                    layout="responsive"
                    alt=""
                  />
                  <div className="p-3">
                    <p className="textwrap textline_two fw-bold h3 blogcard_title h5 bold-font">
                      {blog.blogs.title}
                    </p>
                    <p
                      className="px-3 rounded textwrap textline_one d-inline-block my-2"
                      style={{
                        backgroundColor: "aliceblue",
                        fontSize: "13px",
                      }}
                    >
                      {
                        allCategories.filter(
                          (category) => category.slug === categorySlug
                        )[0].name
                      }
                    </p>
                    <p
                      className="d-flex justify-content-between mb-2 align-items-center"
                      style={{ fontSize: "13px" }}
                    >
                      <span className="d-flex align-items-center">
                        {moment(blog.blogs.updated_at).format("ll")}
                      </span>
                      <span className="d-flex align-items-center">
                        {formatReadCount(blog.blogs.blog_read)} Reads
                      </span>
                    </p>
                    <p className="textwrap textline_two">
                      {blog.blogs.description}
                    </p>
                  </div>
                </Link>
              </div>
            </Col>
          ))}
          {blogs.map((blog) => (
            <Col key={blog.blogs.id} className="col mb-4">
              <div className="border rounded shadow-1">
                <Link href={`/${blog.blogs.slug}`} className="text-dark">
                  <Image
                    src={
                      blog.blogs.banner ? blog.blogs.banner : blog.blogs.image
                    }
                    width={300}
                    height={200}
                    priority
                    layout="responsive"
                    alt=""
                  />
                  <div className="p-3">
                    <p className="textwrap textline_two fw-bold h3 blogcard_title h5 bold-font">
                      {blog.blogs.title}
                    </p>
                    <p
                      className="px-3 rounded textwrap textline_one d-inline-block my-2"
                      style={{
                        backgroundColor: "aliceblue",
                        fontSize: "13px",
                      }}
                    >
                      {
                        allCategories.filter(
                          (category) => category.slug === categorySlug
                        )[0].name
                      }
                    </p>
                    <p
                      className="d-flex justify-content-between mb-2 align-items-center"
                      style={{ fontSize: "13px" }}
                    >
                      <span className="d-flex align-items-center">
                        {moment(blog.blogs.updated_at).format("ll")}
                      </span>
                      <span className="d-flex align-items-center">
                        {formatReadCount(blog.blogs.blog_read)} Reads
                      </span>
                    </p>
                    <p className="textwrap textline_two">
                      {blog.blogs.description}
                    </p>
                  </div>
                </Link>
              </div>
            </Col>
          ))}
        </Row>

        {totalBlogs >= blogs.length ? (
          loadingBlogs ? (
            <div className="text-center disabled">
              <p
                className="rounded d-inline-block px-3 py-2 text-white"
                style={{ backgroundColor: "#0056d2", cursor: "not-allowed" }}
              >
                <Spinner size="sm" animation="border" /> Load More
              </p>
            </div>
          ) : (
            <div
              onClick={() => loadMore()}
              className="text-center cursor-pointer"
            >
              <p
                className="rounded d-inline-block px-3 py-2 text-white"
                style={{ backgroundColor: "#0056d2" }}
              >
                Load More
              </p>
            </div>
          )
        ) : null}
      </Container>
      <Footer
        allCategories={allCategories}
        search={search}
        setSearch={setSearch}
        menuData={menuData}
        homepagereels={homepagereels}
      />
    </>
  );
};

export default Category;
