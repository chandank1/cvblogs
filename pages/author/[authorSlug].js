import Link from "next/link";
import { getAuthorBlogs, getAuthorDetails, getAuthorSlugs, getCategorySlugs } from "../../services/BlogService";
import { getHomePageReels, getMenu } from "../../services/MiscService";
import Head from "next/head";
import Header from "../../Components/Header";
import { useState } from "react";
import { useRouter } from "next/router";
import { Col, Container, Row, Spinner } from "react-bootstrap";
import { FacebookIcon, LinkedinIcon, TwitterIcon, WhatsappIcon } from "next-share";
import moment from "moment";
import { formatReadCount } from "../../utils/misc";
import { flushSync } from "react-dom";
import Footer from "../../Components/Footer";

export async function getStaticPaths() {
  const allSlugs = await getAuthorSlugs().then((response) => response.data);

  const paths = allSlugs.map((authorSlug) => {
    return {
      params: {
        authorSlug: authorSlug.slug,
      },
    };
  });

  return { paths, fallback: true };
}

export async function getStaticProps(context) {
  const authorSlug = context.params.authorSlug;


  let authorData = null;
  let menuData = null;
  let topSixPublishedBlogs = [];
  let topSixPopularBlogs = [];
  let homepagereels = [];
  let allCategories = [];

  try {
    authorData = await getAuthorDetails(authorSlug).then(
      (response) => response.data
    );

    menuData = await getMenu().then((response) => response.data);

    topSixPublishedBlogs = await getAuthorBlogs(authorSlug, 0, 6).then(
      (response) => response.data
    );

    topSixPopularBlogs = await getAuthorBlogs(authorSlug, 0, 6, true).then(
      (response) => response.data
    );

    homepagereels = await getHomePageReels().then((response) => response.data);

    allCategories = await getCategorySlugs().then((response) => response.data);
  }
  catch (e) {
    return {
      notFound: true,
    };
  }

  if (!authorData || !menuData) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      menuData: menuData,
      authorData: authorData,
      topSixPublishedBlogs: topSixPublishedBlogs.data,
      publishedBlogCount: topSixPublishedBlogs.total_blogs,
      topSixPopularBlogs: topSixPopularBlogs.data,
      popularBlogCount: topSixPopularBlogs.total_blogs,
      homepagereels: homepagereels,
      allCategories: allCategories
    },
    revalidate: 60,
  };
}

const AuthorDetail = ({ menuData, authorData, topSixPublishedBlogs, publishedBlogCount, topSixPopularBlogs, popularBlogCount, homepagereels, allCategories, screenSize }) => {

  const router = useRouter();
  const [search, setSearch] = useState(false);
  const [view, setView] = useState("published");

  const [publishedBlogs, setPublishedBlogs] = useState([]);
  const [popularBlogs, setPopularBlogs] = useState([]);

  const [totalPublishedBlogs, setTotalPublishedBlogs] = useState(publishedBlogCount);
  const [lowerPublishedLimit, setLowerPublishedLimit] = useState(6);
  const [upperPublishedLimit, setUpperPublishedLimit] = useState(12);

  const [totalPopularBlogs, setTotalPopularBlogs] = useState(popularBlogCount);
  const [lowerPopularLimit, setLowerPopularLimit] = useState(6);
  const [upperPopularLimit, setUpperPopularLimit] = useState(12);

  const [loadingBlogs, setLoadingBlogs] = useState(false);

  const SubHeading = (props) => {
    return <p className="fw-bold h6 m-0">{props.title} :</p>;
  };

  const BlogCard = (props) => {
    return (
      <div className="border rounded shadow-1 overflow-hidden">
        <Link href={`/${props.slug}`} className="text-dark">
          <img src={props.img} className="w-100" layout="responsive" alt={""} />
          <div className="p-3">
            <p className="textwrap textline_two fw-bold h4 blogcard_title bold-font">
              {props.title}
            </p>
            <p
              className="px-3 rounded textwrap textline_one d-inline-block my-2"
              style={{
                backgroundColor: "aliceblue",
                fontSize: "13px",
              }}
            >
              {props.tag}
            </p>
            <p
              className="d-flex justify-content-between mb-2 align-items-center"
              style={{ fontSize: "13px" }}
            >
              <span className="d-flex align-items-center">{props.date}</span>
              <span className="d-flex align-items-center">
                {props.read} Reads
              </span>
            </p>
            <p
              className="textwrap textline_two mb-0"
              style={{ fontSize: "14px" }}
            >
              {props.desc}
            </p>
          </div>
        </Link>

        <p
          style={{ fontSize: "12px", borderTop: "1px solid #ddd" }}
          className="m-0 ps-3 py-3"
        >
          by <span className="text-primary">{authorData.name}</span>{" "}
        </p>
      </div>
    );
  };

  const loadMorePublished = () => {
    if (totalPublishedBlogs >= publishedBlogs.length) {
      setLoadingBlogs(true);
      flushSync(() => {
        setLowerPublishedLimit((minBlogs) => minBlogs + 6);
        setUpperPublishedLimit((maxBlogs) => maxBlogs + 6);
      });

      setLoadingBlogs(true);
      getAuthorBlogs(authorData.slug, lowerPublishedLimit, upperPublishedLimit)
        .then((response) => {
          setPublishedBlogs((prev) => prev.concat(response.data.data));
          setTotalPublishedBlogs(response.data.total_blogs);
          setLoadingBlogs(false);
        })
        .catch(() => {
          setLoadingBlogs(false);
        });
    }
  }

  const loadMorePopular = () => {
    if (totalPopularBlogs >= popularBlogs.length) {
      setLoadingBlogs(true);
      flushSync(() => {
        setLowerPopularLimit((minBlogs) => minBlogs + 6);
        setUpperPopularLimit((maxBlogs) => maxBlogs + 6);
      });

      setLoadingBlogs(true);
      getAuthorBlogs(authorData.slug, lowerPopularLimit, upperPopularLimit, true)
        .then((response) => {
          setPopularBlogs((prev) => prev.concat(response.data.data));
          setTotalPopularBlogs(response.data.total_blogs);
          setLoadingBlogs(false);
        })
        .catch(() => {
          setLoadingBlogs(false);
        });
    }
  }

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <Head>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="robots"
          content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1"
        />
        <title>
          {authorData.meta_title ||
            `${authorData.name} - Author at College Vidya Blog`}
        </title>
        <meta
          name="description"
          content={
            authorData.meta_description ||
            `${authorData.about.substring(0, 150)}...`
          }
        />
        <link
          rel="canonical"
          href={
            authorData.canonical_url ||
            `https://collegevidya.com/blog/author/${authorData.slug}/`
          }
        />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="article" />
        <meta
          property="og:title"
          content={
            authorData.meta_title ||
            `${authorData.name} - Author at College Vidya Blog`
          }
        />
        <meta
          property="og:description"
          content={
            authorData.meta_description ||
            `${authorData.about.substring(0, 150)}...`
          }
        />
        <meta
          property="og:url"
          content={`https://collegevidya.com/blog/author/${authorData.slug}/`}
        />
        <meta property="og:site_name" content="College Vidya Blog" />
        <meta property="og:image:width" content="100" />
        <meta property="og:image:height" content="100" />
        <meta property="og:image" content={authorData.profile_image} />
        <meta
          property="twitter:title"
          content={
            authorData.meta_title ||
            `${authorData.name} - Author at College Vidya Blog`
          }
        />
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:image" content={authorData.profile_image} />
        <meta name="robots" content="index, follow" />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "WebPage",
              name:
                authorData.meta_title ||
                `${authorData.name} - Author at College Vidya Blog`,
              description:
                authorData.meta_description ||
                `${authorData.about.substring(0, 150)}...`,
              publisher: {
                "@type": "webpage",
                name:
                  authorData.meta_title ||
                  `${authorData.name} - Author at College Vidya Blog`,
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify([
              {
                givenName: authorData.name,
                hasOccupation:
                  authorData.meta_description ||
                  `${authorData.about.substring(0, 150)}...`,
                "@type": "http://schema.org/author",
                sameAs: [],
              },
            ]),
          }}
        />
      </Head>
      <Header menuData={menuData} search={search} />
      <div className="position-relative">
        <div className={`author_bg_hero position-absolute w-100`}></div>{" "}
        <Container
          className={`pt-3 pt-md-5 ${screenSize && screenSize.width < 1200 ? "mw-100" : ""
            }`}
        >
          <Row>
            <Col sm={12} md={4} className="mb-4">
              <div className="border rounded p-4 shadow-sm bg-white sticky-top">
                <div className="text-center">
                  <img
                    className="rounded-circle mb-3"
                    src={authorData.profile_image}
                    style={{
                      width: "100px",
                      height: "100px",
                      objectFit: "cover",
                    }}
                  />
                  <p className="fw-bold h5 mb-3">{authorData.name}</p>
                  <p className="text-center" style={{ fontSize: "12px" }}>
                    {" "}
                    <span className="text-success d-flex justify-content-center align-items-center gap-1">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        class="bi bi-shield-fill-check"
                        viewBox="0 0 16 16"
                      >
                        <path
                          fill-rule="evenodd"
                          d="M8 0c-.69 0-1.843.265-2.928.56-1.11.3-2.229.655-2.887.87a1.54 1.54 0 0 0-1.044 1.262c-.596 4.477.787 7.795 2.465 9.99a11.8 11.8 0 0 0 2.517 2.453c.386.273.744.482 1.048.625.28.132.581.24.829.24s.548-.108.829-.24a7 7 0 0 0 1.048-.625 11.8 11.8 0 0 0 2.517-2.453c1.678-2.195 3.061-5.513 2.465-9.99a1.54 1.54 0 0 0-1.044-1.263 63 63 0 0 0-2.887-.87C9.843.266 8.69 0 8 0m2.146 5.146a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793z"
                        />
                      </svg>{" "}
                      Verified{" "}
                      <span className="d-none d-md-block">Expert</span>
                    </span>
                  </p>
                  <p style={{ fontSize: "12px" }}>
                    <span className="bg-light px-2 py-1 rounded border">
                      <b>{formatReadCount(publishedBlogCount)}+</b> of
                      articles published
                    </span>
                  </p>
                  <p className="fs-14 mb-1">{authorData.title}</p>
                </div>
                <div>
                  <hr />
                  {authorData.domain && (
                    <>
                      <SubHeading title="Domain" />
                      <p className="fs-14 mt-2">{authorData.domain}</p>
                    </>
                  )}

                  {authorData.educational_qualification && (
                    <>
                      <SubHeading title="Educational Qualification" />
                      <p className="fs-14 mt-2">
                        {authorData.educational_qualification}
                      </p>
                    </>
                  )}

                  {authorData.current_role_in_the_industry && (
                    <>
                      <SubHeading title="Current role in the industry" />
                      <p className="fs-14 mt-2">
                        {authorData.current_role_in_the_industry}
                      </p>
                    </>
                  )}

                  {authorData.author_expertise && (
                    <>
                      <SubHeading title="Expertise" />
                      <p className="d-flex gap-2 flex-wrap mt-2">
                        {authorData.author_expertise.map((list, index) => (
                          <span
                            key={index}
                            className="rounded-pill px-2 py-1 border text-primary"
                            style={{
                              backgroundColor: "aliceblue",
                              fontSize: "12px",
                            }}
                          >
                            {list.title}
                          </span>
                        ))}
                      </p>
                    </>
                  )}

                  {authorData.author_tools_technologies && (
                    <>
                      <SubHeading title="Tools & Technologies" />
                      <p className="d-flex gap-2 flex-wrap mt-2">
                        {authorData.author_tools_technologies.map(
                          (list, index) => (
                            <span
                              key={index}
                              className="rounded-pill px-2 py-1 border text-primary"
                              style={{
                                backgroundColor: "aliceblue",
                                fontSize: "12px",
                              }}
                            >
                              {list.title}
                            </span>
                          )
                        )}
                      </p>
                    </>
                  )}
                </div>
              </div>
            </Col>
            <Col sm={12} md={8} className="mb-4">
              <div>
                <h1 className="h3 fw-bold d-flex justify-content-between align-items-center mb-4">
                  <span>About</span>
                  <span>
                    <Link
                      href="/"
                      className="bg-white fs-14 shadow-sm border border px-3 py-2 rounded text-primary"
                    >
                      All Blogs{" "}
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        className="bi bi-chevron-right"
                        viewBox="0 0 16 16"
                      >
                        <path
                          fillRule="evenodd"
                          d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708"
                        />
                      </svg>
                    </Link>
                  </span>
                </h1>
                <p className="fs-14">{authorData.about}</p>
                <div className="d-flex align-items-center">
                  {" "}
                  <SubHeading title="Follow me on" />
                  <div className="d-flex gap-2 ps-1">
                    {authorData.author_follow_me.map((sm, index) => {
                      if (sm.logo === "facebook") {
                        return (
                          <Link key={index} href={sm.link} target="_blank">
                            <FacebookIcon size={25} round />
                          </Link>
                        );
                      }
                      if (sm.logo === "twitter") {
                        return (
                          <Link key={index} href={sm.link} target="_blank">
                            <TwitterIcon size={25} round />
                          </Link>
                        );
                      }
                      if (sm.logo === "whatsapp") {
                        return (
                          <Link key={index} href={sm.link} target="_blank">
                            <WhatsappIcon size={25} round />
                          </Link>
                        );
                      }
                      if (sm.logo === "linkedin") {
                        return (
                          <Link key={index} href={sm.link} target="_blank">
                            <LinkedinIcon size={25} round />
                          </Link>
                        );
                      }
                    })}
                  </div>
                </div>
                <div className="d-inline-flex gap-3 mt-4 bg-white px-2 py-2 rounded">
                  <div
                    className={`px-3 py-2 rounded fs-14 ${view === "published"
                      ? "bg-primary text-white"
                      : "bg-white text-dark"
                      }`}
                    onClick={() => setView("published")}
                    role="button"
                  >
                    Published
                  </div>
                  <div
                    className={`px-3 py-2 rounded fs-14 ${view === "popular"
                      ? "bg-primary text-white"
                      : "bg-white text-dark"
                      }`}
                    onClick={() => setView("popular")}
                    role="button"
                  >
                    Most Popular
                  </div>
                </div>
                {view === "published" && (
                  <>
                    <Row className="row-cols-1 row-cols-sm-2 mt-4">
                      {topSixPublishedBlogs.map((data, index) => (
                        <Col key={index} className="mb-4">
                          <BlogCard
                            img={data.banner || data.image}
                            title={
                              data.title_h_one ? data.title_h_one : data.title
                            }
                            tag={data.categories[0]?.name}
                            date={moment(data.updated_at).format("ll")}
                            read={formatReadCount(data.blog_read)}
                            desc={data.description}
                            slug={data.slug}
                          />
                        </Col>
                      ))}
                      {publishedBlogs.map((data, index) => (
                        <Col key={index} className="mb-4">
                          <BlogCard
                            img={data.banner || data.image}
                            title={
                              data.title_h_one ? data.title_h_one : data.title
                            }
                            tag={data.categories[0]?.name}
                            date={moment(data.updated_at).format("ll")}
                            read={formatReadCount(data.blog_read)}
                            desc={data.description}
                            slug={data.slug}
                          />
                        </Col>
                      ))}
                    </Row>

                    {totalPublishedBlogs >= publishedBlogs.length ? (
                      loadingBlogs ? (
                        <div className="text-center disabled">
                          <p
                            className="rounded d-inline-block px-3 py-2 text-white"
                            style={{
                              backgroundColor: "#0056d2",
                              cursor: "not-allowed",
                            }}
                          >
                            <Spinner size="sm" animation="border" /> Load More
                          </p>
                        </div>
                      ) : (
                        <div
                          onClick={() => loadMorePublished()}
                          className="text-center cursor-pointer"
                        >
                          <p
                            className="rounded d-inline-block px-3 py-2 text-white"
                            style={{ backgroundColor: "#0056d2" }}
                          >
                            Load More
                          </p>
                        </div>
                      )
                    ) : null}
                  </>
                )}

                {view === "popular" && (
                  <>
                    <Row className="row-cols-1 row-cols-sm-2 mt-4">
                      {topSixPopularBlogs.map((data, index) => (
                        <Col key={index} className="mb-4">
                          <BlogCard
                            img={data.banner || data.image}
                            title={
                              data.title_h_one ? data.title_h_one : data.title
                            }
                            tag={data.categories[0]?.name}
                            date={moment(data.updated_at).format("ll")}
                            read={formatReadCount(data.blog_read)}
                            desc={data.description}
                            slug={data.slug}
                          />
                        </Col>
                      ))}
                      {popularBlogs.map((data, index) => (
                        <Col key={index} className="mb-4">
                          <BlogCard
                            img={data.banner || data.image}
                            title={
                              data.title_h_one ? data.title_h_one : data.title
                            }
                            tag={data.categories[0]?.name}
                            date={moment(data.updated_at).format("ll")}
                            read={formatReadCount(data.blog_read)}
                            desc={data.description}
                            slug={data.slug}
                          />
                        </Col>
                      ))}
                    </Row>

                    {totalPopularBlogs >= popularBlogs.length ? (
                      loadingBlogs ? (
                        <div className="text-center disabled">
                          <p
                            className="rounded d-inline-block px-3 py-2 text-white"
                            style={{
                              backgroundColor: "#0056d2",
                              cursor: "not-allowed",
                            }}
                          >
                            <Spinner size="sm" animation="border" /> Load More
                          </p>
                        </div>
                      ) : (
                        <div
                          onClick={() => loadMorePopular()}
                          className="text-center cursor-pointer"
                        >
                          <p
                            className="rounded d-inline-block px-3 py-2 text-white"
                            style={{ backgroundColor: "#0056d2" }}
                          >
                            Load More
                          </p>
                        </div>
                      )
                    ) : null}
                  </>
                )}
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      <Footer
        allCategories={allCategories}
        search={search}
        setSearch={setSearch}
        menuData={menuData}
        homepagereels={homepagereels}
      />
    </>
  );
}

export default AuthorDetail;