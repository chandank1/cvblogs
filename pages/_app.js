import "bootstrap/dist/css/bootstrap.min.css";
import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primereact/resources/primereact.min.css";
import { useRouter } from "next/router";
import Script from "next/script";
import React, { useEffect } from "react";
import { Modal, SSRProvider } from "react-bootstrap";
import "../styles/globals.scss";
import ErrorBoundary from "../Components/Global/ErrorBoundary";
import useWindowSize from "../utils/use-window-size";
import { useState } from "@hookstate/core";
import store from "../utils/store";
import axios from "axios";

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const [videoID, setVideoID] = React.useState(null);
  const [showModal, setShowModal] = React.useState(false);
  const size = useWindowSize();
  const { cashbackSwitch, cashbackMaxAmount, tagLine, universalLogo } = useState(store);

  useEffect(() => {
    if (router.isReady) {
      if (router.query.yt_id) {
        setVideoID(router.query.yt_id);
        setShowModal(true);
      }
    }
  }, [router.isReady]);

  // useEffect(() => {
  //   const originalTitle = document.title;

  //   const interval = setInterval(() => {
  //     if (document.title === originalTitle) {
  //       document.title = `${tagLine.get()} 😎`;
  //     } else {
  //       document.title = originalTitle;
  //     }
  //   }, 2000);

  //   const handleRouteChange = () => {
  //     clearInterval(interval);
  //     document.title = originalTitle;
  //   };

  //   router.events.on("routeChangeStart", handleRouteChange);

  //   return () => {
  //     clearInterval(interval);
  //     router.events.off("routeChangeStart", handleRouteChange);
  //     document.title = originalTitle;
  //   };
  // }, [router]);


  useEffect(() => {
    axios
      .get("https://admin.collegevidya.com/cashback_status/")
      .then((response) => {
        const cashbackStatus = response.data.status;
        cashbackSwitch.set(cashbackStatus);
        cashbackMaxAmount.set(
          `₹${parseInt(response.data.max_amount).toLocaleString("en-IN")}`
        );
      });

    axios
      .get("https://admin.collegevidya.com/website_logo/")
      .then((response) => {
        const primary_logo = response.data.primary_logo;
        const tag_line = response.data.tag_line;
        universalLogo.set(primary_logo);
        tagLine.set(tag_line);
      });
  }, []);

  return (
    <SSRProvider>
      <Script
        id="izitoast-script"
        src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"
      />

      <Script
        id="gtm-tag"
        type="text/partytown"
        dangerouslySetInnerHTML={{
          __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                    })(window,document,'script','dataLayer','GTM-NDZNV24');
                    `,
        }}
      />

      <Script
        id="gtag-script"
        type="text/partytown"
        src="https://www.googletagmanager.com/gtag/js?id=AW-10855552401"
      />

      <ErrorBoundary>
        <Component screenSize={size} {...pageProps} />
      </ErrorBoundary>
      <Modal
        show={showModal}
        size="lg"
        onHide={() => setShowModal(false)}
        centered
      >
        <Modal.Body className="p-0 m-0 position-relative">
          <span
            className="position-absolute top-0 end-0 cursor-pointer"
            fontSize={42}
            onClick={() => setShowModal(false)}
            style={{ marginTop: "-30px", color: "white" }}
          >
            X
          </span>
          <iframe
            className="embed-responsive-item"
            width="100%"
            height="450"
            src={`https://youtube.com/embed/${videoID}`}
            title="YouTube video player"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          />
        </Modal.Body>
      </Modal>
    </SSRProvider>
  );
}

export default MyApp;
