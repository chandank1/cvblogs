import React, { useState } from "react";
import { getHomePageReels, getMenu } from "../services/MiscService";
import Header from "../Components/Header";
import Footer from "../Components/Footer";
import { getCategorySlugs } from "../services/BlogService";
import BelowHeader from "../Components/BelowHeader";
import Head from "next/head";
import { getSearchSchema } from "../utils/misc";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "next/image";
import Link from "next/link";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import styles from "./error.module.css";

export async function getStaticProps(context) {
  const allCategories = await getCategorySlugs().then(
    (response) => response.data
  );

  const menuData = await getMenu().then((response) => response.data);
  const homepagereels = await getHomePageReels().then(
    (response) => response.data
  );

  return {
    props: {
      allCategories: allCategories,
      menuData: menuData,
      homepagereels: homepagereels,
    },
    revalidate: 60,
  };
}

const Error = ({ allCategories, menuData, homepagereels }) => {
  const [search, setSearch] = useState(false);

  return (
    <>
      <Head>
        <title>404 - Oops, Page Not Found</title>
        <meta
          name="description"
          content="Oops, We couldn't find that page. Please try again or contact the website administrator to get some help."
        />
      </Head>
      <Header menuData={menuData} search={search} />
      <BelowHeader menuData={menuData} />
      <Container className="text-center mt-0 mt-md-5">
        <Row>
          <Col className="col-12">
            <Image
              src={"/blog/Images/error.jpg"}
              width={250}
              height={250}
              alt="404 page"
              quality={100}
            />
            {/* <p style={{fontSize:"40px"}}>Page Not Found</p> */}
            <div>
              <Link
                href="https://collegevidya.com/blog/"
                className="px-4 py-2 d-inline-block"
                style={{
                  backgroundColor: "aliceblue",
                  borderRadius: "25px",
                  color: "#0056d2",
                  border: "1px solid #0056d2",
                }}
              >
                Homepage
              </Link>
            </div>
          </Col>
          <Col className="col-12 col-lg-8 mx-auto pt-1 pt-sm-2">
            <Tabs
              defaultActiveKey="PG Courses"
              className={`${styles.error_courses} mb-3 mt-4`}
            >
              {menuData.map((domain) => (
                <Tab eventKey={domain.name} title={domain.name} key={domain.id}>
                  <div className="row row-cols-3 row-cols-md-3 row-cols-lg-3 mt-1">
                    {domain.courses.map((course) => (
                      <Link
                        href={`https://collegevidya.com/courses/${course.slug}`}
                        className="d-flex mb-3 px-1"
                        key={course.id}
                      >
                        
                        <p
                          className="m-0 h-100 d-flex align-items-center flex-column justify-content-center rounded px-2 py-3"
                          style={{
                            backgroundColor: "aliceblue",
                            border: "1px solid #0056d2",
                            color: "#0056d2",
                            fontSize: "14px",
                            width: "-webkit-fill-available",
                          }}
                        >
                          <span className="d-block"><Image src={course.icon} width={25} height={25} alt={course.name} /></span>
                          <span className={`d-block mt-2 ${styles.course_name}`}>{course.name}</span>
                        </p>
                      </Link>
                    ))}
                  </div>
                </Tab>
              ))}
            </Tabs>
          </Col>
        </Row>
      </Container>

      <Footer
        allCategories={allCategories}
        search={search}
        setSearch={setSearch}
        menuData={menuData}
        homepagereels={homepagereels}
      />
    </>
  );
};

export default Error;
