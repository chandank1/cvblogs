import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Col, Container, Row, Spinner } from "react-bootstrap";
import Header from "../Components/Header";
import { getCategorySlugs, getSearchedBlogs } from "../services/BlogService";
import { getHomePageReels, getMenu } from "../services/MiscService";
import { isMobile } from "react-device-detect";
import moment from "moment";
import { formatReadCount, getSearchSchema } from "../utils/misc";
import { flushSync } from "react-dom";
import Footer from "../Components/Footer";
import Head from "next/head";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);
  const allCategories = await getCategorySlugs().then(
    (response) => response.data
  );
  const homepagereels = await getHomePageReels().then(
    (response) => response.data
  );

  return {
    props: {
      menuData: menuData,
      allCategories: allCategories,
      homepagereels: homepagereels
    },
    revalidate: 60,
  };
}

const Search = ({ menuData, allCategories, homepagereels }) => {
  const router = useRouter();
  const [search, setSearch] = useState(false);

  const [searchValue, setSearchValue] = useState(null);
  const [initialLoading, setInitialLoading] = useState(false);
  const [loadingBlogs, setLoadingBlogs] = useState(false);
  const [blogs, setBlogs] = useState([]);
  const [totalBlogs, setTotalBlogs] = useState(0);
  const [lowerLimit, setLowerLimit] = useState(0);
  const [upperLimit, setUpperLimit] = useState(6);

  useEffect(() => {
    if (router.isReady) {
      setSearchValue(router.query.q);
    }
  }, [router]);

  useEffect(() => {
    if (searchValue) {
      setInitialLoading(true);
      getSearchedBlogs(searchValue, lowerLimit, upperLimit)
        .then((response) => {
          setBlogs(response.data.data);
          setTotalBlogs(response.data.total_blogs);
          setInitialLoading(false);
        })
        .catch(() => {
          setInitialLoading(false);
        });
    }
  }, [searchValue]);

  function loadMore() {
    if (totalBlogs >= blogs.length) {
      setLoadingBlogs(true);
      const newLowerLimit = lowerLimit + 6;
      const newUpperLimit = upperLimit + 6;

      flushSync(() => {
        setLowerLimit((minBlogs) => minBlogs + 6);
        setUpperLimit((maxBlogs) => maxBlogs + 6);
      });

      setLoadingBlogs(true);
      getSearchedBlogs(searchValue, newLowerLimit, newUpperLimit)
        .then((response) => {
          setBlogs((prev) => prev.concat(response.data.data));
          setTotalBlogs(response.data.total_blogs);
          setLoadingBlogs(false);
        })
        .catch(() => {
          setLoadingBlogs(false);
        });
    }
  }

  return (
    <div>
      <Head>
        <title>
          You searched for {searchValue} - College Vidya Blog (Online Colleges,
          Courses, Reviews, Fees)
        </title>
        <link rel="canonical" href={`https://collegevidya.com/blog/search/`} />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content={`You searched for ${searchValue} - College Vidya Blog (Online Colleges,
            Courses, Reviews, Fees)`}
        />
        <meta
          property="og:url"
          content="https://collegevidya.com/blog/search/"
        />
        <meta
          property="og:site_name"
          content="College Vidya Blog (Online Colleges, Courses, Reviews, Fees)"
        />
        <meta name="twitter:card" content="summary_large_image" />

        {/* Search Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        {/* Schema One */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidya.com/blog/search/",
              name: "College Vidya Blog (Online Colleges, Courses, Reviews, Fees)",
              description:
                "Learn updated details about online colleges, courses, admission process, reviews, fees, career prospectus and more.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: [".seotag"],
              },
            }),
          }}
        />

        {/* Schema Two */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        {/* Breadcrumb Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "https://schema.org",
              "@type": "BreadcrumbList",
              itemListElement: [
                {
                  "@type": "ListItem",
                  position: 1,
                  item: {
                    "@id": "https://collegevidya.com/blog",
                    name: "Blog",
                  },
                },
                {
                  "@type": "ListItem",
                  position: 2,
                  item: {
                    "@id": "https://collegevidya.com/blog/search",
                    name: "Blog Search",
                  },
                },
              ],
            }),
          }}
        />
      </Head>
      <Header menuData={menuData} search={search} />

      <Container className="mt-3 mt-sm-5">
        <div className="d-flex align-items-center justify-content-center gap-2">
          <Link href={"https://collegevidya.com"}>College Vidya</Link>
          <span className="text-secondary" style={{ fontSize: "20px" }}>
            &gt;
          </span>
          <Link href={"https://collegevidya.com/blog"}>Blog</Link>
        </div>
        <div className="mt-3 text-center">
          <h1>
            <b>You Searched &quot;{searchValue}&quot;</b>
          </h1>
        </div>
        {!initialLoading && blogs.length === 0 ? (
          <div className="text-center mt-5">
            No content found! Please go{" "}
            <Link
              href={"https://collegevidya.com/blog"}
              style={{ color: "#0d6efd" }}
            >
              back to the Blog
            </Link>
            <br />
            or try searching another keyword.
          </div>
        ) : null}
        <Row className="mt-1 mt-sm-5 row-cols-1 row-cols-sm-2 row-cols-lg-3">
          {blogs.map((blog) => (
            <Col key={blog.id} className="col mb-4">
              <div className="border rounded shadow-1">
                <Link href={blog.slug} className="text-dark">
                  <Image
                    src={blog.banner ? blog.banner : blog.image}
                    width={300}
                    height={200}
                    priority={isMobile ? false : true}
                    layout="responsive"
                    alt=""
                  />
                  <div className="p-3">
                    <p className="textwrap textline_two fw-bold h4 blogcard_title bold-font">
                      {blog.title_h_one ? blog.title_h_one : blog.title}
                    </p>
                    <p
                      className="px-3 rounded textwrap textline_one d-inline-block my-2"
                      style={{
                        backgroundColor: "aliceblue",
                        fontSize: "13px",
                      }}
                    >
                      {blog.categories[0].name}
                    </p>
                    <p
                      className="d-flex justify-content-between mb-2 align-items-center"
                      style={{ fontSize: "13px" }}
                    >
                      <span className="d-flex align-items-center">
                        {moment(blog.updated_at).format("ll")}
                      </span>
                      <span className="d-flex align-items-center">
                        {formatReadCount(blog.blog_read)} Reads
                      </span>
                    </p>
                    <p className="textwrap textline_two">{blog.description}</p>
                  </div>
                </Link>
              </div>
            </Col>
          ))}
        </Row>

        {totalBlogs > blogs.length ? (
          loadingBlogs ? (
            <div className="text-center disabled">
              <p
                className="rounded d-inline-block px-3 py-2 text-white"
                style={{ backgroundColor: "#0056d2", cursor: "not-allowed" }}
              >
                <Spinner size="sm" animation="border" /> Load More
              </p>
            </div>
          ) : (
            <div
              onClick={() => loadMore()}
              className="text-center cursor-pointer"
            >
              <p
                className="rounded d-inline-block px-3 py-2 text-white"
                style={{ backgroundColor: "#0056d2" }}
              >
                Load More
              </p>
            </div>
          )
        ) : null}
      </Container>
      <Footer
        allCategories={allCategories}
        search={search}
        setSearch={setSearch}
        menuData={menuData}
        homepagereels={homepagereels}
      />
    </div>
  );
};

export default Search;
