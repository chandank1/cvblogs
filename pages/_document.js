/* eslint-disable @next/next/no-sync-scripts */
import Document, { Html, Head, Main, NextScript } from "next/document";
import { Partytown } from "@builder.io/partytown/react";

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html
        xmlns="https://www.w3.org/1999/xhtml"
        dir="ltr"
        lang="en-US"
        translate="no"
      >
        <Head>
          <link rel="preconnect" href="https://www.googletagmanager.com" />
          <link rel="preconnect" href="https://www.google-analytics.com" />

          <Partytown
            debug={false}
            forward={["dataLayer.push", "GoogleAnalyticsObject", "ga", "gtag"]}
            resolveUrl={(url) => {
              const proxyMap = {
                "www.google-analytics.com": "dlcrsnrblg19w.cloudfront.net",
                "connect.facebook.net": "d4o9kd7mq0jyd.cloudfront.net",
                "secure.quantserve.com": "d9kp1axw6x3tn.cloudfront.net",
                "api.trstplse.com": "d28tsn69kmk58w.cloudfront.net",
                "bat.bing.com": "d364vn0ljaz1zn.cloudfront.net",
                "sc-events-sdk.sharechat.com": "d3k2i49jd52cw.cloudfront.net",
                "rules.quantcount.com": "dq0ss2ypkqdat.cloudfront.net",
              };
              url.hostname = proxyMap[url.hostname] || url.hostname;
              return url;
            }}
          />
        </Head>
        <body>
          <Main />
          <NextScript />
          <noscript
            dangerouslySetInnerHTML={{
              __html: `<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NDZNV24" height="0" width="0" style="display: none; visibility: hidden;" />`,
            }}
          />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
