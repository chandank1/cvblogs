import axios from "axios";

export const getCategorySlugs = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "blog_categories/");
};

export const getBlogs = (min, max) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + `blognew/${min}/${max}`
  );
};

export const getCategoryBlogs = (categorySlug, min, max) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL +
    `blogcategorylistnew/${categorySlug}/${min}/${max}/`
  );
};

export const getBlogSlugs = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "blog_slugs/");
};

export const getBlogDetails = (slug) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + `blog_details/${slug}`
  );
};

export const readBlog = (blogID) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + `blog_read/${blogID}/`
  );
};

export const getSearchedBlogs = (query, min, max) => {
  let formData = new FormData();
  formData.append("min", min);
  formData.append("max", max);
  formData.append("keywords", query);
  return axios.post(
    process.env.NEXT_PUBLIC_BACKEND_URL + `blog_search/`,
    formData
  );
};

export const getAuthorSlugs = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "author_slugs/");
};

export const getAuthorDetails = (slug) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + `author_details/${slug}/`
  );
};

export const getAuthorBlogs = (authorSlug, min, max, mostPopular) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + `author/${authorSlug}/${min}/${max}/${mostPopular ? "most-popular/" : ""}`
  );
};