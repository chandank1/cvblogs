const axios = require("axios");

export const getMenu = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "menu");
};

export const getAllCountries = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + `countries/`);
};

export const getStates = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "state/");
};

export const getUserCountry = () => {
  return axios.get("https://api.country.is/", { timeout: 1000 }).then((res) => res.data);
};

export const getCities = (stateID) => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + `city/${stateID}`);
};

export const saveError = (data) => {
  return axios.post(process.env.NEXT_PUBLIC_BACKEND_URL + `get_error/`, data);
};

export const getHomePageReels = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "influencers/");
};

export const verifyOTP = (mobile, otp) => {
  let formData = new FormData();
  formData.append("mobile", mobile);
  formData.append("otp", otp);

  return axios.post(process.env.NEXT_PUBLIC_BACKEND_URL + `otp_verification/`, formData);
}

export const pushCustomActivity = (formData) => {
  return axios.post(
    process.env.NEXT_PUBLIC_BACKEND_URL + "create_activity/",
    formData
  );
};

export const getOTP = (name = "", number, email) => {
  let formData = new FormData();
  formData.append("name", name);
  formData.append("mobile", number);
  if (email === "") {
    formData.append("email", -1);
  } else {
    formData.append("email", email);
  }
  return axios.post(process.env.NEXT_PUBLIC_BACKEND_URL + `otp/`, formData);
};
