import axios from "axios";

export const addLead = (data) => {
  return axios.post(process.env.NEXT_PUBLIC_BACKEND_URL + "leads/", data);
};

export const createCVCounsellorUserAccount = (data) => {
  return axios.post(
    process.env.NEXT_PUBLIC_CV_COUNSELLOR_CLIENT_BACKEND_URL +
      "collegevidya/customer/create",
    data
  );
};
