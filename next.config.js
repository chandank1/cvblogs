/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  basePath: "/blog",
  assetPrefix: "/blog",
  swcMinify: true,
  trailingSlash: true,
  images: {
    domains: [
      "collegevidyanew.s3.amazonaws.com",
      "cv-counsellors.s3.ap-south-1.amazonaws.com",
      "collegevidya.com",
      "img.youtube.com",
      "d1aeya7jd2fyco.cloudfront.net",
    ],
  },
};

module.exports = nextConfig;
