import Image from "next/image";
import React, { useEffect, useRef } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Spinner from "react-bootstrap/Spinner";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { getCookie, hasCookie, setCookie } from "cookies-next";
import {
  addLead,
  createCVCounsellorUserAccount,
} from "../services/LeadService";
import CountryDropDown from "./Global/CountryDropDown";
import moment from "moment";
import { getCities, pushCustomActivity } from "../services/MiscService";
import GroupImage from "./Global/GroupImage";
import { Dropdown } from "primereact/dropdown";
import RemoveMessage from "./Global/RemoveMessage";
import ConnectWithTop from "./Global/ConnectWithTop";
import SecureMessage from "./Global/SecureMessage";
import VerifyOTPModal from "./Global/VerifyOTPModal";
import Logo from "./Global/Logo";
import { useState } from "@hookstate/core";
import store from "../utils/store";
import CashbackCheckbox from "./Global/CashbackCheckbox";

const LeadForm = ({
  menuData,
  states,
  externalLink,
  handleClose,
  fromInline,
  screenSize,
}) => {
  const gtmRef = useRef(null);

  const [cities, setCities] = React.useState([]);
  const [loading, setLoading] = React.useState(false);

  const [showOTPModal, setShowOTPModal] = React.useState(false);
  const [otpModalContent, setShowOTPModalContent] = React.useState(null);

  const { cashbackSwitch, tagLine, cashbackMaxAmount } = useState(store);

  const validationSchema = Yup.object().shape({
    gender: Yup.string().required("Please select a gender"),
    full_name: Yup.string().required("Name is required"),
    dob: Yup.date()
      .typeError("Invalid date")
      .required("Please enter your date of birth")
      .min(moment().subtract(100, "years"), "Max age 100 Yr")
      .max(moment().subtract(12, "years"), "Min age 12 Yr"),
    email: Yup.string()
      .email("Invalid email")
      .matches(/^[a-zA-Z0-9_.]+@[^\s@]+\.[a-zA-Z]{2,}$/, "Invalid email")
      .required("Please enter your email address"),
    selected_country: Yup.number().required(),
    phone_code: Yup.string(),
    country_name: Yup.string(),
    mobile_number: Yup.number()
      .typeError("Invalid number")
      .required("Number is required")
      .when("selected_country", {
        is: (selected_country) => {
          return selected_country === 101;
        },
        then: Yup.number()
          .typeError("Invalid Number")
          .required("Number is required")
          .test("len", "Invalid Number", (val) => val.toString().length === 10)
          .test("startsWith", "Invalid Number", (val) =>
            /^[6-9]/.test(val.toString())
          ),
      })
      .when("selected_country", {
        is: (selected_country) => {
          return selected_country !== 101;
        },
        then: Yup.number()
          .typeError("Invalid number")
          .required("Number is required")
          .test(
            "len",
            "Invalid Number",
            (val) => val.toString().length >= 3 && val.toString().length <= 15
          ),
      }),
    course: Yup.number()
      .typeError("Course is required")
      .required("Course is required"),
    state: Yup.number()
      .required("Please select a state")
      .typeError("State is required"),
    city: Yup.number()
      .required("Please select a city")
      .typeError("City is required"),
    couponApplied: Yup.string().required("Select an option"),
  });

  const {
    register,
    getValues,
    setValue,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {},
    mode: "onChange",
    resolver: yupResolver(validationSchema),
  });

  const saveLeadForm = (leadformdata) => {
    setCookie("leadform", JSON.stringify(leadformdata));
  };

  useEffect(() => {
    if (getValues("state")) {
      getCities(getValues("state"))
        .then((response) => {
          setCities(response.data.data);
          if (getValues("state") === 42) {
            setValue("city", 8487, { shouldValidate: true });
          } else {
            setValue("city", null, { shouldValidate: true });
          }
        })
        .catch(() => {
          setCities([]);
          setValue("city", null, { shouldValidate: true });
        });
    }
  }, [getValues("state")]);

  const onSubmit = (data) => {
    setLoading(true);

    const selectedCourse = menuData
      .map((domain) => domain.courses)
      .flat()
      .filter((course) => course.id === data.course)[0];

    let formData = new FormData();
    formData.append("user_id", -1);
    formData.append("gender", data.gender);
    formData.append("name", data.full_name);
    formData.append("email", data.email);
    if (getValues("selected_country") === 101) {
      formData.append("mobile", data.mobile_number);
    } else {
      formData.append(
        "mobile",
        "+" + getValues("phone_code") + "-" + data.mobile_number
      );
    }
    formData.append("country_name", getValues("country_name"));
    formData.append("dob", data.dob);
    if (selectedCourse.parent_courses[0]) {
      const redirectionalCourse = selectedCourse.parent_courses[0];
      formData.append("course", redirectionalCourse.id);
    } else {
      formData.append("course", data.course);
    }
    formData.append("specializations", "");
    formData.append("state", data.state);
    formData.append("city", data.city);
    formData.append("website_url", "blog-inline-lead-form");
    formData.append("actual_course", selectedCourse.name);

    if (
      getValues("couponApplied") === "true" ||
      getValues("couponApplied") === "true1"
    ) {
      formData.append("coupon_apply", data.couponApplied);
    }

    if (hasCookie("referrer_id")) {
      formData.append("referral", getCookie("referrer_id"));
    } else if (hasCookie("campaign_name")) {
      formData.append("campaign_name", getCookie("campaign_name"));
      formData.append("ad_group_name", getCookie("ad_group_name"));
      formData.append("ads_name", getCookie("ads_name"));
      formData.append("source_campaign", getCookie("source_campaign"));
      if (hasCookie("partner_type"))
        formData.append("partner_type", getCookie("partner_type"));
    }

    addLead(formData)
      .then((response) => {
        setLoading(false);

        saveLeadForm({
          gender: data.gender,
          name: data.full_name,
          email: data.email,
          selected_country: data.selected_country,
          mobile_number: data.mobile_number,
          dob: data.dob,
          state: data.state,
          city: data.city,
        });

        if (!hasCookie("lead-filled")) {
          gtmRef.current.click();

          <script
            dangerouslySetInnerHTML={{
              __html: `gtag("event", "conversion", {
                send_to: "AW-10855552401/t3NbCN7j04AYEJGrqrgo",
              })`,
            }}
          />;
        }

        setCookie("lead-filled", "true");
        if (
          getValues("couponApplied") === "true" ||
          getValues("couponApplied") === "true1"
        ) {
          pushCustomActivity({
            uid: response.data.user_id,
            mx_value_one:
              getValues("couponApplied") === "true"
                ? `Receive CV Subsidy Cash upto ${cashbackMaxAmount.get()} directly in your bank account`
                : `Get upskilling courses worth ${cashbackMaxAmount.get()}`,
            activity_name: "type_to_apply",
          });
        }

        let redirectionURL = null;

        if (fromInline) {
          if (selectedCourse.parent_courses[0]) {
            const redirectionalCourse = selectedCourse.parent_courses[0];
            redirectionURL = `https://collegevidya.com/colleges-universities/${redirectionalCourse.slug}/?uid=${response.data.user_id}`;
          } else {
            redirectionURL = `https://collegevidya.com/colleges-universities/${selectedCourse.slug}?uid=${response.data.user_id}`;
          }
        } else {
          redirectionURL = externalLink;
        }

        if (
          hasCookie("referrer_id") &&
          response.data.refer_lead_already_exist === "true"
        ) {
          iziToast.error({
            title: "Lead Already Exists",
            message:
              "Your referrer will not be paid on your admission but you will still get the subsidy.",
            timeout: 3000,
            position: "topRight",
          });
        }

        setShowOTPModalContent(
          <VerifyOTPModal
            uid={response.data.user_id}
            userName={data.full_name}
            userEmail={data.email}
            userMobile={
              getValues("selected_country") === 101
                ? data.mobile_number
                : "+" + getValues("phone_code") + "-" + data.mobile_number
            }
            redirectionURL={redirectionURL}
            closeModal={() => setShowOTPModal(false)}
          />
        );
        setShowOTPModal(true);
      })
      .catch(() => {
        setLoading(false);
      });
  };

  return (
    <Form
      className="px-3 py-4 rounded contact_form shadow sticky-top border-top border-5 border-primary position-relative mb-3 mx-0"
      onSubmit={handleSubmit(onSubmit)}
    >
      {fromInline ? null : (
        <>
          <Image
            className="position-absolute end-0 top-0 cursor-pointer me-4 mt-4"
            onClick={handleClose}
            style={{ zIndex: "1111" }}
            src={"/blog/Images/icons/close-dark.svg"}
            width="40"
            height="40"
            alt="close"
          />
          <h5 className="fw-bold mb-3">
            Hey, before you go!
            <br />
            Let us know you better..
          </h5>
        </>
      )}
      {/* <Form.Group className="d-flex gap-3 align-items-center mb-4 position-relative">
        <Form.Check
          type="radio"
          label="Male"
          value={"male"}
          checked={getValues("gender") === "male"}
          onChange={(e) =>
            setValue("gender", e.target.value, { shouldValidate: true })
          }
        />

        <Form.Check
          type="radio"
          label="Female"
          value={"female"}
          checked={getValues("gender") === "female"}
          onChange={(e) =>
            setValue("gender", e.target.value, { shouldValidate: true })
          }
        />
        <span className="p-error text-xs d-inline-block">
          {errors.gender?.message}
        </span>
      </Form.Group> */}

      <Form.Group className="mb-4 position-relative" controlId="formBasicEmail">
        <Form.Control
          {...register("full_name")}
          type="text"
          placeholder="Enter Name"
          size="lg"
        />
        <span className="p-error text-xs">{errors.full_name?.message}</span>
      </Form.Group>

      <Row>
        <Col className="col-12 col-md-6 mb-4">
          <Form.Group className="position-relative">
            <Dropdown
              appendTo={"self"}
              panelStyle={{ maxWidth: "100%" }}
              options={[
                {
                  id: "male",
                  name: "Male",
                },
                {
                  id: "female",
                  name: "Female",
                },
                {
                  id: "other",
                  name: "Other",
                },
              ]}
              value={getValues("gender")}
              onChange={(e) =>
                setValue("gender", e.value, { shouldValidate: true })
              }
              className={`w-100 ${errors.gender ? "p-invalid block" : ""}`}
              optionValue="id"
              optionLabel="name"
              placeholder="Select a Gender"
            />
            <span className="p-error text-xs">{errors.gender?.message}</span>
          </Form.Group>
        </Col>
        <Col className="col-12 col-md-6 mb-4">
          <Form.Group
            className="position-relative"
            controlId="formBasicPassword"
          >
            <Form.Control
              type="text"
              placeholder="Date of Birth"
              size="md"
              onClick={(e) => {
                e.currentTarget.type = "date";
              }}
              onFocus={(e) => {
                e.currentTarget.type = "date";
              }}
              onMouseEnter={(e) => {
                e.currentTarget.type = "date";
              }}
              max={new Date().toISOString().split("T")[0]}
              {...register("dob")}
            />
            <span className="p-error text-xs">{errors.dob?.message}</span>
          </Form.Group>
        </Col>
      </Row>

      <InputGroup className="flag_dropdown mb-4 position-relative">
        <CountryDropDown setValue={setValue} />
        <Form.Control
          type="text"
          placeholder="Mobile Number"
          size="lg"
          {...register("mobile_number")}
        />
        <span
          className="position-absolute end-0 me-2 px-2 rounded-bottom text-success"
          style={{
            bottom: "-16px",
            backgroundColor: "#d4fee8",
            fontSize: "11px",
          }}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="12"
            height="12"
            fill="currentColor"
            className="bi bi-shield-fill-check"
            viewBox="0 0 16 16"
          >
            <path
              fillRule="evenodd"
              d="M8 0c-.69 0-1.843.265-2.928.56-1.11.3-2.229.655-2.887.87a1.54 1.54 0 0 0-1.044 1.262c-.596 4.477.787 7.795 2.465 9.99a11.777 11.777 0 0 0 2.517 2.453c.386.273.744.482 1.048.625.28.132.581.24.829.24s.548-.108.829-.24a7.159 7.159 0 0 0 1.048-.625 11.775 11.775 0 0 0 2.517-2.453c1.678-2.195 3.061-5.513 2.465-9.99a1.541 1.541 0 0 0-1.044-1.263 62.467 62.467 0 0 0-2.887-.87C9.843.266 8.69 0 8 0zm2.146 5.146a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647z"
            />
          </svg>{" "}
          We don&apos;t spam{" "}
        </span>
        <span className="p-error text-xs">{errors.mobile_number?.message}</span>
      </InputGroup>

      <Form.Group
        className="mb-4 position-relative"
        controlId="formBasicPassword"
      >
        <Form.Control
          type="email"
          placeholder="Email"
          size="lg"
          {...register("email")}
        />
        <span className="p-error text-xs">{errors.email?.message}</span>
      </Form.Group>
      <Form.Group className="mb-4 position-relative">
        <Dropdown
          options={menuData.map((domain) => domain.courses).flat()}
          value={getValues("course")}
          onChange={(e) =>
            setValue("course", e.value, { shouldValidate: true })
          }
          className={`w-100 ${errors.course ? "p-invalid block" : ""}`}
          optionValue="id"
          optionLabel="display_name"
          filter
          placeholder="Select a Course"
          appendTo="self"
          enforceFocus={false}
        />
        <span className="p-error text-xs">{errors.course?.message}</span>
      </Form.Group>
      {getValues("selected_country") === 101 ? (
        <>
          <Form.Group className="mb-4 position-relative">
            <Dropdown
              
              options={states}
              disabled={getValues("selected_country") !== 101}
              value={getValues("state")}
              onChange={(e) =>
                setValue("state", e.value, { shouldValidate: true })
              }
              className={`w-100 ${errors.state ? "p-invalid block" : ""}`}
              optionValue="id"
              optionLabel="state"
              filter
              placeholder="Select a State"
              appendTo="self"
              enforceFocus={false}
            />
            <span className="p-error text-xs">{errors.state?.message}</span>
          </Form.Group>

          <Form.Group className="mb-4 position-relative">
            <Dropdown
              options={cities}
              disabled={getValues("selected_country") !== 101}
              value={getValues("city")}
              onChange={(e) =>
                setValue("city", e.value, { shouldValidate: true })
              }
              className={`w-100 ${errors.city ? "p-invalid block" : ""}`}
              optionValue="id"
              optionLabel="city"
              filter
              placeholder="Select a City"
              appendTo="self"
              enforceFocus={false}
            />
            <span className="p-error text-xs">{errors.city?.message}</span>
          </Form.Group>
        </>
      ) : null}

      {cashbackSwitch.get() && (
        <CashbackCheckbox
          screenSize={screenSize}
          getValues={getValues}
          setValue={setValue}
          errors={errors}
        />
      )}

      <Button
        disabled={loading}
        variant="primary"
        type="submit"
        className="bgprimary w-100 py-3"
      >
        {loading ? (
          <>
            <Spinner size="sm" animation="border" /> Submit
          </>
        ) : (
          "Submit"
        )}
      </Button>
      {fromInline ? null : (
        <>
          {" "}
          <SecureMessage />
        </>
      )}
      <ConnectWithTop />
      <RemoveMessage />
      <input ref={gtmRef} id="OUTBOUND" type={"hidden"} />

      <Modal
        style={{ zIndex: 99999, backgroundColor: "rgba(0,0,0,0.9)" }}
        show={showOTPModal}
        onHide={() => setShowOTPModal(false)}
        backdrop="static"
        centered
      >
        <Modal.Header className="border-0 justify-content-start position-relative ps-0">
          <Modal.Title
            id="contained-modal-title-vcenter"
            className="d-flex align-items-center pt-2"
          >
            <div className="d-flex align-items-center ps-3">
              <Logo />
              <p className="m-0" style={{ fontSize: "12px" }}>
                {tagLine.get()}
              </p>
            </div>
          </Modal.Title>
          <span
            className="position-absolute cursor-pointer"
            onClick={() => setShowOTPModal(false)}
            style={{ top: "8px", right: "16px" }}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="26"
              height="26"
              fill="currentColor"
              className="bi bi-x"
              viewBox="0 0 16 16"
            >
              <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708" />
            </svg>
          </span>
        </Modal.Header>
        <Modal.Body className="p-3">{otpModalContent}</Modal.Body>
      </Modal>
    </Form>
  );
};

export default LeadForm;
