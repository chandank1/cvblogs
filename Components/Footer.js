import React, { useState } from "react";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Link from "next/link";
import Image from "next/image";
import Modal from "react-bootstrap/Modal";
import LazyLoad from "react-lazy-load";
import VideoModal from "./Global/VideoModal";
import Influencer from "./Global/IframeWithCustomButton";
import IframeWithCustomButton from "./Global/IframeWithCustomButton";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation } from "swiper";
import GroupImage from "./Global/GroupImage";

const Footer = ({
  allCategories,
  search,
  setSearch,
  menuData,
  homepagereels,
}) => {
  const [show2, setShow2] = useState(false);
  const handleClose2 = () => setShow2(false);
  const handleShow2 = () => setShow2(true);

  const [show3, setShow3] = useState(false);
  const handleClose3 = () => setShow3(false);
  const handleShow3 = () => setShow3(true);

  return (
    <>
      <LazyLoad offset={50}>
        <footer
          className="pt-5 mt-5"
          style={{ backgroundColor: "#111", paddingBottom: "120px" }}
        >
          <Container>
            <Row>
              <Col className="col-12 col-sm-6 col-lg-3">
                <p
                  className="fw-bold d-inline-block border-bottom pb-1 border-2"
                  style={{ color: "#cfcfcf" }}
                >
                  UG Course
                </p>
                <ul className="list-unstyled fs-14">
                  {menuData
                    .filter((domain) => domain.name.includes("UG"))[0]
                    .courses.map((course) => (
                      <li key={course.id} className="mb-2">
                        <Link
                          href={`https://collegevidya.com/courses/${course.slug}`}
                          style={{ color: "#989eb6" }}
                        >
                          {course.name}
                        </Link>
                      </li>
                    ))}
                  <li className="mb-2">
                    <Link
                      href="https://collegevidya.com/online-distance/online-distance-pg-course/"
                      style={{ color: "#989eb6" }}
                    >
                      View All
                    </Link>
                  </li>
                </ul>
              </Col>
              <Col className="col-12 col-sm-6 col-lg-3">
                <p
                  className="fw-bold d-inline-block border-bottom pb-1 border-2"
                  style={{ color: "#cfcfcf" }}
                >
                  PG Course
                </p>
                <ul className="list-unstyled fs-14">
                  {menuData
                    .filter((domain) => domain.name.includes("PG"))[0]
                    .courses.map((course) => (
                      <li key={course.id} className="mb-2">
                        <Link
                          href={`https://collegevidya.com/courses/${course.slug}`}
                          style={{ color: "#989eb6" }}
                        >
                          {course.name}
                        </Link>
                      </li>
                    ))}
                  <li className="mb-2">
                    <Link
                      href="https://collegevidya.com/online-distance/online-distance-pg-course/"
                      style={{ color: "#989eb6" }}
                    >
                      View All
                    </Link>
                  </li>
                </ul>
              </Col>
              <Col className="col-12 col-sm-6 col-lg-3">
                <p
                  className="fw-bold d-inline-block border-bottom pb-1 border-2"
                  style={{ color: "#cfcfcf" }}
                >
                  Diploma
                </p>
                <ul className="list-unstyled fs-14">
                  {menuData
                    .filter((domain) => domain.name.includes("Diploma"))[0]
                    .courses.map((course) => (
                      <li key={course.id} className="mb-2">
                        <Link
                          href={`https://collegevidya.com/courses/${course.slug}`}
                          style={{ color: "#989eb6" }}
                        >
                          {course.name}
                        </Link>
                      </li>
                    ))}
                </ul>
                <p
                  className="fw-bold pt-2 d-inline-block border-bottom pb-1 border-2"
                  style={{ color: "#cfcfcf" }}
                >
                  Executive Education
                </p>
                <ul className="list-unstyled fs-14">
                  {menuData
                    .filter((domain) => domain.name.includes("Executive"))[0]
                    .courses.map((course) => (
                      <li key={course.id} className="mb-2">
                        <Link
                          href={`https://collegevidya.com/courses/${course.slug}`}
                          style={{ color: "#989eb6" }}
                        >
                          {course.name}
                        </Link>
                      </li>
                    ))}
                </ul>
              </Col>
              <Col className="col-12 col-sm-6 col-lg-3">
                <p
                  className="fw-bold d-inline-block border-bottom pb-1 border-2"
                  style={{ color: "#cfcfcf" }}
                >
                  Certificate
                </p>
                <ul className="list-unstyled fs-14">
                  {menuData
                    .filter((domain) => domain.name.includes("Certificate"))[0]
                    .courses.map((course) => (
                      <li key={course.id} className="mb-2">
                        <Link
                          href={`https://collegevidya.com/courses/${course.slug}`}
                          style={{ color: "#989eb6" }}
                        >
                          {course.name}
                        </Link>
                      </li>
                    ))}
                </ul>
                <p
                  className="mb-1"
                  style={{ color: "#989eb6", fontSize: "12px" }}
                >
                  Contact Us:{" "}
                </p>
                <Link
                  className="text-decoration-none d-block text-white"
                  href="mailto:info@collegevidya.com"
                >
                  info@collegevidya.com
                </Link>
                <div className="position-relative mt-5">
                  <span
                    style={{ fontSize: "10px", top: "-15px", left: "-10px" }}
                    className="bgprimary rounded px-1 text-white position-absolute start-0 ms-4 translate-middle"
                  >
                    Toll Free
                  </span>
                  <Link
                    style={{ border: "1px solid #fff", fontSize: "13px" }}
                    className="text-white text-decoration-none  bg-dark px-3 pt-3 pb-2 rounded"
                    href="tel:1800-420-5757"
                  >
                    1800-420-5757
                  </Link>
                </div>
                <p className="fs-14 text-white mt-4">Download App</p>
                <div className="d-flex gap-3">
                  <div className="d-flex flex-column gap-3 mb-2">
                    <Link
                      href="https://play.google.com/store/apps/details?id=com.app.collegevidya"
                      role="button"
                      target="_blank"
                    >
                      <img
                        src={"/blog/Images/google_play.png"}
                        width={120}
                        height={38}
                        alt="play store"
                      />
                    </Link>
                    <Link
                      href="https://apps.apple.com/in/app/college-vidya/id6450980823"
                      role="button"
                      target="_blank"
                    >
                      <img
                        src={"/blog/Images/appstore.png"}
                        width={120}
                        height={38}
                        alt="app store"
                      />
                    </Link>
                  </div>
                  <img
                    src="/blog/Images/app-qr.svg"
                    width={90}
                    height={90}
                    alt="QR Code"
                  />
                </div>
                <div className="d-flex gap-2 align-items-center mb-3 flex-wrap">
                  <div className="mt-3 d-inline-flex align-items-center justify-content-between gap-2 bg-white w-100 px-2 py-1 rounded shadow-sm position-relative">
                    <Link
                      href="/images/pdf/great-place-to-work.pdf"
                      target="_blank"
                    >
                      <div className="d-flex gap-2 align-items-center">
                        <img
                          className="rounded"
                          src={"/blog/images/great-place-to-work.png"}
                          width={35}
                          height={55}
                          alt="Greate Place To Work"
                        />

                        <p className="m-0 text-dark fs-14">
                          Great Place To Work
                        </p>
                      </div>
                    </Link>
                  </div>
                  <div className="mt-2 d-inline-flex align-items-center justify-content-between gap-2 bg-white w-100 px-2 py-3 rounded shadow-sm">
                    <div className="d-flex gap-2 align-items-center">
                      <img
                        className="rounded"
                        src={"/blog/images/trust_pilot.png"}
                        width={30}
                        height={30}
                        alt="trustpilot"
                      />

                      <p className="m-0 text-dark fs-14">Trustpilot</p>
                    </div>
                    <p className="m-0 bgprimary px-2 py-1 d-flex gap-2 align-items-center rounded text-white">
                      4.7
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        class="bi bi-star-fill text-warning"
                        viewBox="0 0 16 16"
                      >
                        <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                      </svg>
                    </p>
                  </div>
                  <div className="mt-2 d-inline-flex align-items-center justify-content-between gap-2 bg-white w-100 px-2 py-3 rounded shadow-sm">
                    <div className="d-flex gap-2 align-items-center">
                      <img
                        className="rounded"
                        src={"/blog/images/career.png"}
                        width={30}
                        height={30}
                        alt="trustpilot"
                      />
                      <p className="m-0 text-dark fs-14">Career Karma</p>
                    </div>
                    <p className="m-0 bgprimary px-2 py-1 d-flex gap-2 align-items-center rounded text-white">
                      4.6
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        class="bi bi-star-fill text-warning"
                        viewBox="0 0 16 16"
                      >
                        <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                      </svg>
                    </p>
                  </div>
                </div>
              </Col>
            </Row>
            <ul className="fs-14 d-flex list-unstyled justify-content-between flex-wrap gap-2 mb-1 mt-4">
              <li>
                <Link
                  style={{ color: "#989eb6" }}
                  href="https://collegevidya.com/about-us/"
                >
                  About Us
                </Link>
              </li>
              <li>
                <Link
                  style={{ color: "#989eb6" }}
                  href="https://collegevidya.com/blog/"
                >
                  Blog
                </Link>
              </li>
              <li>
                <Link
                  target="_blank"
                  style={{ color: "#989eb6" }}
                  href="https://collegevidya.com/career-finder/"
                >
                  Career Finder (Career Suitability Test){" "}
                  <sup
                    className="fw-bold"
                    style={{ fontSize: "10px", color: " #1abc9c" }}
                  >
                    New
                  </sup>
                </Link>
              </li>
              <li>
                <Link
                  style={{ color: "#989eb6" }}
                  href="https://collegevidyapartners.com/"
                >
                  Become Business Associate
                </Link>
              </li>
              <li>
                <Link
                  style={{ color: "#989eb6" }}
                  href="https://collegevidya.com/sitemap/"
                >
                  Sitemap
                </Link>
              </li>
              <li>
                <Link
                  style={{ color: "#989eb6" }}
                  href="https://collegevidya.com/contact-us/"
                >
                  Contact us
                </Link>
              </li>

              <li>
                <Link
                  style={{ color: "#989eb6" }}
                  href="https://collegevidya.com/collection/"
                >
                  Important Videos(CV TV)
                </Link>
              </li>
              <li>
                <Link
                  style={{ color: "#989eb6" }}
                  href="https://collegevidya.com/our-trust/"
                >
                  Our Trust
                </Link>
              </li>
              <li>
                <Link
                  style={{ color: "#989eb6" }}
                  href="https://collegevidya.com/tool/"
                >
                  Our Tools
                </Link>
              </li>
              <li>
                <Link
                  style={{ color: "#989eb6" }}
                  href="https://collegevidya.com/web-stories/"
                >
                  Web Stories
                </Link>
              </li>
              <li>
                <Link
                  style={{ color: "#989eb6" }}
                  href="https://collegevidya.com/question-answer/"
                >
                  Ask any Question - CV Forum
                </Link>
              </li>
            </ul>
            <p
              className="text-center border-top pt-4"
              style={{ fontSize: "10px", color: "#989eb6" }}
            >
              <span className="d-block mb-3">
                <Link
                  className="text-decoration-none"
                  href="https://collegevidya.com/term-and-conditions/"
                >
                  {" "}
                  Terms &amp; Conditions
                </Link>
                <Link
                  className="text-decoration-none"
                  href="https://collegevidya.com/refund-policy/"
                >
                  {" "}
                  / Refund Policy
                </Link>
                <Link
                  className="text-decoration-none"
                  href="https://collegevidya.com/privacy-policy/"
                >
                  {" "}
                  / Our Policy
                </Link>
              </span>
              The intend of College Vidya is to provide unbiased precise
              information &amp; comparative guidance on Universities and its
              Programs of Study to the Admission Aspirants. The contents of the
              College vidya Site, such as Texts, Graphics, Images, Blogs,
              Videos, University Logos, and other materials contained on College
              vidya Site (collectively, “Content”) are for information purpose
              only. The content is not intended to be a substitute for in any
              form on offerings of its Academia Partner. Infringing on
              intellectual property or associated rights is not intended or
              deliberately acted upon. The information provided by College Vidya
              on www.collegevidya.com or any of its mobile or any other
              applications is for general information purposes only. All
              information on the site and our mobile application is provided in
              good faith with accuracy and to the best of our knowledge,
              however, we make nor representation or warranty of any kind,
              express or implied, regarding the accuracy, adequacy, validity,
              reliability, completeness of any information on the Site or our
              mobile application. College vidya &amp; its fraternity will not be
              liable for any errors or omissions and damages or losses resultant
              if any from the usage of its information.
            </p>
            <p
              className="text-center"
              style={{ fontSize: "13px", color: "#989eb6" }}
            >
              © 2025 College Vidya, Inc. All Rights Reserved.
            </p>
            <p
              className="text-center"
              style={{ fontSize: "13px", color: "#989eb6" }}
            >
              Build with{" "}
              <Image
                src={"/blog/Images/icons/heart-fill.svg"}
                width={14}
                height={14}
                alt="heart"
                priority
              />{" "}
              Made in India.
            </p>
            <Row>
              <Col className="d-flex align-items-center justify-content-center">
                <span
                  className="me-2"
                  style={{ fontSize: "12px", color: "#989eb6" }}
                >
                  Social Links :{" "}
                </span>
                <ul className="list-unstyled d-flex gap-2 m-0">
                  <li>
                    <Link
                      href="https://www.facebook.com/collegevidya"
                      style={{ color: "#989eb6" }}
                      target="_blank"
                    >
                      <Image
                        src={"/blog/Images/icons/facebook.svg"}
                        width={16}
                        height={16}
                        priority
                        alt="facebook"
                      />
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="https://www.instagram.com/collegevidya/"
                      style={{ color: "#989eb6" }}
                      target="_blank"
                    >
                      <Image
                        src={"/blog/Images/icons/instagram.svg"}
                        width={16}
                        height={16}
                        priority
                        alt="instagram"
                      />
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw"
                      style={{ color: "#989eb6" }}
                      target="_blank"
                    >
                      <Image
                        src={"/blog/Images/icons/youtube.svg"}
                        width={16}
                        height={16}
                        priority
                        alt="youtube"
                      />
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="https://twitter.com/CollegeVidya"
                      style={{ color: "#989eb6" }}
                      target="_blank"
                    >
                      <Image
                        src={"/blog/Images/icons/twitter.svg"}
                        width={16}
                        height={16}
                        priority
                        alt="twitter"
                      />
                    </Link>
                  </li>
                  <li>
                    <Link
                      href="https://www.linkedin.com/company/college-vidya"
                      style={{ color: "#989eb6" }}
                      target="_blank"
                    >
                      <Image
                        src={"/blog/Images/icons/linkedin.svg"}
                        width={16}
                        height={16}
                        priority
                        alt="linkedin"
                      />
                    </Link>
                  </li>
                </ul>
              </Col>
            </Row>
          </Container>
        </footer>
      </LazyLoad>
      <div
        className="d-block d-lg-none position-fixed bottom-0 px-2 py-3 w-100 rounded-top text-center bg-white start-50 translate-middle-x"
        style={{ zIndex: "11" }}
      >
        <Link
          className="fw-bold w-100 d-flex align-items-center justify-content-center gap-1 text-white fw-bold"
          style={{
            backgroundColor: "#f75d34",
            zIndex: "11",
            boxShadow: "0 4px 12px rgba(255,86,48,.251)",
            borderRadius: "8px",
            padding: "10px 0",
            fontSize: "14px",
          }}
          href="https://collegevidya.com/suggest-me-an-university/?utm_source=blog_stickyfooter&utm_medium=organic&utm_campaign=Sticky_footer&utm_content=lead"
        >
          <GroupImage /> Talk to Career Experts{" "}
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            className="bi bi-arrow-right"
            viewBox="0 0 16 16"
          >
            <path
              fillRule="evenodd"
              d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8"
            />
          </svg>
        </Link>
      </div>

      <Modal
        show={show2}
        onHide={handleClose2}
        backdrop="static"
        fullscreen
        className="influencer_swiper"
        style={{ paddingBottom: "70px" }}
      >
        <Modal.Header closeButton>
          <Modal.Title className="fw-bold">
            Trusted <span className="text-primary">Voice</span>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="p-0 bg-dark d-flex align-items-center">
          <Swiper
            loop={true}
            slidesPerView="auto"
            spaceBetween={20}
            centeredSlides={false}
            navigation={true}
            keyboard={false}
            slidesPerGroup={1}
            breakpoints={{
              "@0.00": {
                slidesPerView: 1,
              },
              500: {
                slidesPerView: 1,
              },
              1024: {
                slidesPerView: 2,
              },
              1336: {
                slidesPerView: 3,
              },
            }}
            modules={[Navigation]}
          >
            {homepagereels &&
              homepagereels.map((reel) => (
                <SwiperSlide key={reel.id}>
                  <IframeWithCustomButton
                    src={reel.youtube_link}
                    thumbnail={reel.thumbnail}
                  />
                </SwiperSlide>
              ))}
          </Swiper>
        </Modal.Body>
      </Modal>

      <Modal show={show3} onHide={handleClose3} backdrop="static" fullscreen>
        <Modal.Header closeButton>
          <Modal.Title>Top Universities</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ul className="list-unstyled fs-14">
            {menuData
              .map((domain) => domain.courses)
              .flat()
              .map((course) => (
                <li
                  className="px-3 py-2 rounded mb-2 border"
                  style={{ backgroundColor: "aliceblue", fontSize: "14px" }}
                  key={course.id}
                  value={course.slug}
                >
                  <Link
                    href={`https://collegevidya.com/top-universities-colleges/${course.slug}`}
                  >
                    {course.name}
                  </Link>
                </li>
              ))}
          </ul>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default Footer;
