import React from "react";
import {
  FacebookIcon,
  LinkedinIcon,
  TwitterIcon,
  WhatsappIcon,
} from "next-share";
import Link from "next/link";

const AuthorCard = ({ authorData }) => {
  return (
    <div className="bg-light rounded p-3 mt-4">
      <div className="d-flex align-items-center justify-content-between flex-wrap gap-3">
        <div className="d-flex align-items-center gap-2">
          <img
            className="rounded-circle"
            src={authorData.profile_image}
            alt="profile"
            style={{ width: "50px", height: "50px" }}
          />

          <div>
            {" "}
            <p className="m-0">
              <span className="text-secondary">
                <small>By</small>
              </span>{" "}
              <Link href={`/author/${authorData.slug}`} target="_blank">
                <span className="fs-16 fw-bold text-dark hover-underline">
                  {authorData.name}
                </span>
              </Link>
            </p>
            <p className="text-secondary m-0">
              <small>{authorData.title}</small>
            </p>
          </div>
        </div>
        <div className="d-flex align-items-center gap-2">
          <small className="text-secondary">
            {" "}
            {authorData.author_follow_me.length > 0 ? "Follow :" : null}
          </small>{" "}
          <div className="d-flex gap-2 ps-1">
            {authorData.author_follow_me?.map((sm, index) => {
              if (sm.logo === "facebook") {
                return (
                  <Link key={index} href={sm.link} target="_blank">
                    <FacebookIcon size={25} round />
                  </Link>
                );
              }
              if (sm.logo === "twitter") {
                return (
                  <Link key={index} href={sm.link} target="_blank">
                    <TwitterIcon size={25} round />
                  </Link>
                );
              }
              if (sm.logo === "whatsapp") {
                return (
                  <Link key={index} href={sm.link} target="_blank">
                    <WhatsappIcon size={25} round />
                  </Link>
                );
              }
              if (sm.logo === "linkedin") {
                return (
                  <Link key={index} href={sm.link} target="_blank">
                    <LinkedinIcon size={25} round />
                  </Link>
                );
              }
            })}
          </div>
        </div>
      </div>
      <p className="fs-14 mt-3">{authorData.about}</p>
    </div>
  );
};

export default AuthorCard;
