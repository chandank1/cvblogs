import Image from "next/image";
import Link from "next/link";
import React from "react";

const CareerFinderCard = () => {
  return (
    <div className="bg-white border rounded text-center pt-3 pb-4 px-3 shadow-sm">
      <Image
        src="/blog/Images/icons/career_anim.gif"
        width={100}
        height={100}
      />
      <p className="fw-bold textprimary mb-2 h5 mt-2">
        Career Finder 
      </p>
      <p className="fw-bold textprimary mb-2 h6 m-0">
      (Career Suitability Test)
      </p>

      <p className="fs-14 text-body-secondary">
        Explore and Find out your Most Suitable Career Path. Get Started with
        our Career Finder Tool Now!
      </p>
      <p className="mb-2">
        {" "}
        <Link
          href="https://collegevidya.com/career-finder/"
          className="bgprimary text-white fs-14 px-3 py-2 rounded shadow-sm"
          target="_blank"
        >
          Get Started{" "}
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            className="bi bi-arrow-right"
            viewBox="0 0 16 16"
          >
            <path
              fill-rule="evenodd"
              d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8"
            />
          </svg>
        </Link>
      </p>
    </div>
  );
};

export default CareerFinderCard;
