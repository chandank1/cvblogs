import Image from "next/image";
import React, { useState } from "react";

const IframeWithCustomButton = ({ src, thumbnail }) => {
  const [showIframe, setShowIframe] = useState(false);

  const handleClick = () => {
    setShowIframe(true);
  };

  return (
    <div className="position-relative d-flex align-items-center justify-content-center ">
      {!showIframe && (
        <div>
          <img src={thumbnail} alt="Thumbnail" className="rounded w-100" />
          <button
            onClick={handleClick}
            className="position-absolute top-50 start-50 translate-middle border-0 rounded py-2"
            style={{ backgroundColor: "rgba(0,0,0,0.05)" }}
          >
            {" "}
            <span className="p-1 rounded-circle border-0">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="56"
                height="56"
                fill="#fff"
                className="cursor-pointer bi bi-play-circle"
                viewBox="0 0 16 16"
              >
                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                <path d="M6.271 5.055a.5.5 0 0 1 .52.038l3.5 2.5a.5.5 0 0 1 0 .814l-3.5 2.5A.5.5 0 0 1 6 10.5v-5a.5.5 0 0 1 .271-.445z" />
              </svg>
            </span>
          </button>
        </div>
      )}

      {showIframe && (
        <iframe
          src={src}
          frameBorder="0"
          allowFullScreen
          title="Embedded Content"
          style={{ width: "100%", height: "350px" }}
        />
      )}
    </div>
  );
};

export default IframeWithCustomButton;
