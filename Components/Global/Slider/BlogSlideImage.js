import Image from "next/image";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay } from "swiper";
import "swiper/css";
import "swiper/css/effect-cards";
import { EffectCards } from "swiper";
import RatingStar from "../../../Components/Global/RatingStar";
// import styles from "./SwipeImage.module.scss";

const BlogSlideImage = () => {
  const swipeImages = [
    {
      img: "/blog/Images/video/mentor-1.png",
      rating: "4",
      counselling: "2412",
      qualification: "MBA, 5 yr Exp",
    },
    {
      img: "/blog/Images/video/mentor-2.png",
      rating: "5",
      counselling: "2134",
      qualification: "M.Com, 4 yr Exp",
    },
    {
      img: "/blog/Images/video/mentor-3.png",
      rating: "4",
      counselling: "1789",
      qualification: "MBA, 10 yr Exp",
    },
    {
      img: "/blog/Images/video/mentor-4.png",
      rating: "5",
      counselling: "2209",
      qualification: "MCA, 8 yr Exp",
    },
    {
      img: "/blog/Images/video/mentor-5.png",
      rating: "4",
      counselling: "1957",
      qualification: "M.Com., 7 yr Exp",
    },
    {
      img: "/blog/Images/video/mentor-6.png",
      rating: "4",
      counselling: "1586",
      qualification: "Ph.D, 15 yr Exp",
    },
    {
      img: "/blog/Images/video/mentor-7.png",
      rating: "5",
      counselling: "2308",
      qualification: "MCA, 8 yr Exp",
    },
  ];

  return (
    <>
      <Swiper
        loop={true}
        grabCursor={true}
        modules={[Autoplay, EffectCards]}
        autoplay={{
          delay: 1000,
          disableOnInteraction: false,
        }}
        className={`text-center`}
      >
        {swipeImages.map((list, index) => (
          <SwiperSlide
            className="d-flex flex-column px-3 py-4 text-center justify-content-center align-items-center"
            key={index}
          >
            <img src={list.img} alt="mentor1" width={160} height={160} />
            <div
              className="my-3 px-3 rounded py-2 text-success fw-bold"
              style={{ fontSize: "14px", backgroundColor: "#d4fee8" }}
            >
              {list.qualification}
            </div>
            <div className="d-flex justify-content-center text-center">
              <RatingStar value={list.rating} />
            </div>
            <p
              className="mt-3 text-success mb-2 text-center px-3 py-1 rounded d-flex align-items-center gap-1 justify-content-center"
              style={{ backgroundColor: "#f0fff0", fontSize: "16px" }}
            >
              <span className="greenblink"></span> {list.counselling}+
            </p>
            <p
              className="text-success text-center"
              style={{ fontSize: "12px" }}
            >
              Students Counseled
            </p>
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
};

export default BlogSlideImage;
