import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/effect-fade";
import "swiper/css/pagination";
import "swiper/css/navigation";
import "swiper/css/autoplay";
import "swiper/css/grid";
import { EffectFade, Navigation, Autoplay, Grid, Pagination } from "swiper";
import Card from "react-bootstrap/Card";
import Marquee from "react-fast-marquee";

const UniversityFormSliderMobile = () => {
  const suggest_university_list = [
    { img: "/blog/Images/university_list/1_amity-online-university-logo.jpg" },
    {
      img: "/blog/Images/university_list/2_chandigarh-distance-university-logo.jpg",
    },
    { img: "/blog/Images/university_list/3_chandigarh-online-university-logo.jpg" },
    {
      img: "/blog/Images/university_list/4_datta-meghe-institute-of-higher-education-online-logo.webp",
    },
    {
      img: "/blog/Images/university_list/5_dy-patil-vidyapeeth-university-online.jpg",
    },
    { img: "/blog/Images/university_list/6_GLA-University-Online-logo.jpg" },
    { img: "/blog/Images/university_list/7_Hindustan-University-logo.jpg" },
    { img: "/blog/Images/university_list/8_Jain-University-logo.jpg" },
    {
      img: "/blog/Images/university_list/9_Lingayas-Vidyapeeth-University-logo.jpg",
    },
    {
      img: "/blog/Images/university_list/10_Lovely-Professional-University-Online-logo.jpg",
    },
    {
      img: "/blog/Images/university_list/24_golden_gate.png",
    },
    {
      img: "/blog/Images/university_list/23_birchwood-logo.webp",
    },
  ];

  return (
    <>
      {/* <Swiper
        slidesPerView={4}
        grid={{
          rows: 1,
          fill: "row",
        }}
        pagination={false}
        spaceBetween={20}
        speed={5000}
        autoplay={{
          delay: 0,
          disableOnInteraction: false,
        }}
        loop={false}
        navigation={false}
        modules={[Grid, Navigation, Autoplay, Pagination]}
      >
        {suggest_university_list.map((list, index) => (
          <SwiperSlide key={index}>
            <Card className="shadow-1 position-relative px-2 py-2 d-flex align-items-center">
              <img
                src={list.img}
                className="rounded w-100"
                width={250}
                alt=""
                loading="lazy"
                objectFit="contain"
              />
            </Card>
          </SwiperSlide>
        ))}
      </Swiper> */}

      <Marquee speed={50} loop={0} autoFill={true}>
        {suggest_university_list.map((list, index) => (
          <div key={index} className="mx-2">
            <Card className="shadow-1 position-relative px-2 py-2 h-100">
              <img
                src={list.img}
                className="rounded w-100"
                alt=""
                loading="lazy"
                objectFit="contain"
                style={{ maxWidth: "125px" }}
              />
            </Card>
          </div>
        ))}
      </Marquee>
    </>
  );
};

export default UniversityFormSliderMobile;
