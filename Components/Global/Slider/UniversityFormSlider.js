import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/effect-fade";
import "swiper/css/pagination";
import "swiper/css/navigation";
import "swiper/css/autoplay";
import "swiper/css/grid";
import { EffectFade, Navigation, Autoplay, Grid, Pagination } from "swiper";
import Card from "react-bootstrap/Card";

const UniversityFormSlider = () => {
  const suggest_university_list = [
    { img: "/images/university_list/1_amity-online-university-logo.jpg" },
    {
      img: "/images/university_list/2_chandigarh-distance-university-logo.jpg",
    },
    { img: "/images/university_list/3_chandigarh-online-university-logo.jpg" },
    {
      img: "/images/university_list/4_datta-meghe-institute-of-higher-education-online-logo.webp",
    },
    {
      img: "/images/university_list/5_dy-patil-vidyapeeth-university-online.jpg",
    },
    { img: "/images/university_list/6_GLA-University-Online-logo.jpg" },
    { img: "/images/university_list/7_Hindustan-University-logo.jpg" },
    { img: "/images/university_list/8_Jain-University-logo.jpg" },
    {
      img: "/images/university_list/9_Lingayas-Vidyapeeth-University-logo.jpg",
    },
    {
      img: "/images/university_list/10_Lovely-Professional-University-Online-logo.jpg",
    },
  ];

  return (
    <>
       <Swiper
          slidesPerView={6}
          // centeredSlides={true}
          // effect={"fade"}
          grid={{
            rows: 2,
            fill: "row",
          }}
          pagination={false}
          spaceBetween={20}
          // pagination={{
          //   clickable: true,
          // }}
          speed={5000}
          autoplay={{
            delay: 0,
            disableOnInteraction: false,
          }}
          loop={false}
              navigation={false}
              breakpoints={{
                "@0.00": {
                  slidesPerView: 3,
                },
                500: {
                  slidesPerView: 3,
                },
                1024: {
                  slidesPerView: 3,
                },
                1336: {
                  slidesPerView: 3,
                },
                1920: {
                  slidesPerView: 3,
                },
              }}
          modules={[Grid, Navigation, Autoplay, Pagination]}
          
        >
        {suggest_university_list.map((list, index) => (
          <SwiperSlide key={index}>
            <Card className="shadow-1 position-relative px-2 py-2 d-flex align-items-center">
              <img
                src={list.img}
                className="rounded w-100"
                width={250}
                // height={75}
                alt=""
                        loading="lazy"
                        objectFit="contain"
              />
            </Card>
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
};

export default UniversityFormSlider;
