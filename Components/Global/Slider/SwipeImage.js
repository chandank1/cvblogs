import Image from "next/image";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay } from "swiper";
import "swiper/css";
import "swiper/css/effect-cards";
import { EffectCards } from "swiper";
import RatingStar from "../../../Components/Global/RatingStar";
import styles from "./SwipeImage.module.scss";

const SwipeImage = () => {
  const swipeImages = [
    {
      img: "/blog/Images/video/mentor-1.png",
      rating: "3",
      counselling: "2412",
      qualification: "MBA, 5 yr Exp",
    },
    {
      img: "/blog/Images/video/mentor-2.png",
      rating: "4",
      counselling: "2134",
      qualification: "M.Com, 4 yr Exp",
    },
    {
      img: "/blog/Images/video/mentor-3.png",
      rating: "4",
      counselling: "1789",
      qualification: "MBA, 5 yr Exp",
    },
    {
      img: "/blog/Images/video/mentor-4.png",
      rating: "3",
      counselling: "2209",
      qualification: "MCA, 8 yr Exp",
    },
    {
      img: "/blog/Images/video/mentor-5.png",
      rating: "4",
      counselling: "1957",
      qualification: "M.Com., 7 yr Exp",
    },
    {
      img: "/blog/Images/video/mentor-6.png",
      rating: "4",
      counselling: "1586",
      qualification: "Ph.D, 15 yr Exp",
    },
    {
      img: "/blog/Images/video/mentor-7.png",
      rating: "3",
      counselling: "2308",
      qualification: "MCA, 8 yr Exp",
    },
  ];

  return (
    <>
      <Swiper
        loop={true}
        effect={"cards"}
        grabCursor={true}
        modules={[Autoplay, EffectCards]}
        autoplay={{
          delay: 1000,
          disableOnInteraction: false,
        }}
        className={`${styles.mentor_slider}`}
      >
        {swipeImages.map((list, index) => (
          <SwiperSlide className="d-flex flex-column px-3 py-4" key={index}>
            <img src={list.img} alt="mentor1" width={160} height={160} />
            <div className="text-dark my-2" style={{ fontSize: "14px" }}>
              {list.qualification}
            </div>
            <div>
              <RatingStar value={list.rating} />
            </div>
            <p
              className="mt-3 text-success mb-2 text-center px-3 py-1 rounded d-flex align-items-center gap-1"
              style={{ backgroundColor: "#f0fff0", fontSize: "16px" }}
            >
              <span className="greenblink"></span> {list.counselling}+
            </p>
            <p
              className="text-success text-center"
              style={{ fontSize: "12px" }}
            >
              Students Counseled
            </p>
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
};

export default SwipeImage;
