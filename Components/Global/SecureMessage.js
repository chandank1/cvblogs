import Image from "next/image";
import React from "react";

const SecureMessage = () => {
  return (
    <div className="text-center form_desc">
      <div className="mt-3 text-center secure-bgcolor bg-gradient d-inline-block rounded">
        <span className="fw-normal px-2 text-dark" style={{fontSize:"12px"}}>
          <Image
            src={"/blog/Images/icons/lock-fill.svg"}
            width={14}
            height={14}
            alt="key"
          />{" "}
          Your personal information is secure with us
        </span>
      </div>
    </div>
  );
};

export default SecureMessage;
