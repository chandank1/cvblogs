import React from "react";
import { FacebookShareButton, FacebookIcon } from "next-share";
import { LinkedinShareButton, LinkedinIcon } from "next-share";
import { WhatsappShareButton, WhatsappIcon } from "next-share";
import { TwitterShareButton, TwitterIcon } from "next-share";

const Share = ({ url, title }) => {
  return (
    <>
      <div className="px-3 d-flex gap-1 mb-3">
        <FacebookShareButton url={url} title={title}>
          <FacebookIcon size={25} round />
        </FacebookShareButton>
        <TwitterShareButton url={url} title={title}>
          <TwitterIcon size={25} round />
        </TwitterShareButton>
        <WhatsappShareButton url={url} title={title} separator=":: ">
          <WhatsappIcon size={25} round />
        </WhatsappShareButton>
        <LinkedinShareButton url={url}>
          <LinkedinIcon size={25} round />
        </LinkedinShareButton>
      </div>
    </>
  );
};

export default Share;
