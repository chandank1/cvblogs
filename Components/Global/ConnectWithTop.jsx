import React from "react";
import GroupImage from "./GroupImage";

const ConnectWithTop = () => {
  return (
    <div>
      {" "}
      <div className="d-flex fs-11 align-items-center justify-content-center gap-1 mt-3 flex-wrap py-2 rounded">
        <GroupImage />
        <span style={{ fontSize: "12px" }}>Connect with Top CV Experts</span>
        ⭐⭐⭐⭐⭐{" "}
      </div>
    </div>
  );
};

export default ConnectWithTop;
