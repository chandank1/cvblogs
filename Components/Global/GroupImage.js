import React from "react";
import { Avatar } from "primereact/avatar";
import { AvatarGroup } from "primereact/avatargroup"; //Optional for grouping

const GroupImage = () => {
  return (
    <>
      {" "}
      <AvatarGroup>
        <Avatar image="/blog/Images/video/mentor-1.png" shape="circle" />
        <Avatar image="/blog/Images/video/mentor-2.png" shape="circle" />
        <Avatar image="/blog/Images/video/mentor-3.png" shape="circle" />
      </AvatarGroup>
    </>
  );
};

export default GroupImage;
