import { useState } from "@hookstate/core";
import moment from "moment/moment";
import React from "react";
import store from "../../utils/store";

const CashbackViewDetail = () => {
  const { cashbackText, cashbackMaxAmount } = useState(store);
  const content_list = [
    {
      data: (
        <>
          The maximum {cashbackText.get()} in Selected Universities is Upto{" "}
          <span>{cashbackMaxAmount.get()}</span>{" "}
        </>
      ),
    },
    {
      data: (
        <>
          The CV {cashbackText.get()} is only applicable on admissions processed
          through College Vidya*
        </>
      ),
    },
    {
      data: <>CV {cashbackText.get()} Link will be provided instantly</>,
    },
    {
      data: <>Amount will be credited instantly in your bank account</>,
    },
    {
      data: (
        <>
          CV {cashbackText.get()} Money will be transferred to the applicant’s
          account only
        </>
      ),
    },
    {
      data: (
        <>
          {cashbackText.get()} will be transferred to the applicant&apos;s
          account only
        </>
      ),
    },

    {
      data: `This offer is valid till ${moment()
        .add(7, "days")
        .format("MMMM Do")}, 11 PM`,
    },
    {
      data: (
        <>
          You can earn money by sharing the CV {cashbackText.get()} link with
          others
        </>
      ),
    },
  ];

  return (
    <div className={`mycustom_list text-start`}>
      <ul className="mb-3 fs-14">
        {content_list.map((list, index) => (
          <li key={index}>{list.data}</li>
        ))}
      </ul>
      <p className="fs-12 m-0">
        * College Vidya does not accept any fees from students. CV assist
        students in admission process & redirects student to the official
        website of university/edtech to deposit their fees.{" "}
      </p>
    </div>
  );
};

export default CashbackViewDetail;
