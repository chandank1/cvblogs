import Image from "next/image";
import Link from "next/link";
import React from "react";
import icon1 from "../../public/Images/icons/person.png";

const CategoryList = () => {
  const CategoryList = [
    {
      heading: "AI and Machine Learning",
      link: "link",
      icon: icon1,
    },
    {
      heading: "AI and Machine Learning",
      link: "link",
      icon: icon1,
    },
    {
      heading: "AI and Machine Learning",
      link: "link",
      icon: icon1,
    },
    {
      heading: "AI and Machine Learning",
      link: "link",
      icon: icon1,
    },
    {
      heading: "AI and Machine Learning",
      link: "link",
      icon: icon1,
    },
    {
      heading: "AI and Machine Learning",
      link: "link",
      icon: icon1,
    },
    {
      heading: "AI and Machine Learning",
      link: "link",
      icon: icon1,
    },
    {
      heading: "AI and Machine Learning",
      link: "link",
      icon: icon1,
    },
    {
      heading: "AI and Machine Learning",
      link: "link",
      icon: icon1,
    },
    {
      heading: "AI and Machine Learning",
      link: "link",
      icon: icon1,
    },
    {
      heading: "AI and Machine Learning",
      link: "link",
      icon: icon1,
    },
  ];

  return (
    <>
      <ul
        className="list-unstyled px-4 py-4 rounded border"
        style={{ backgroundColor: "aliceblue" }}
      >
        {CategoryList.map((list, index) => (
          <li key={list.index}>
            <Link
              className="text-decoration-none text-dark d-block py-2 my-1 px-2"
              href={list.link}
            >
              {" "}
              <Image src={list.icon} width={24} height={24} alt="" />{" "}
              <span className="ms-2">{list.heading}</span>
            </Link>
          </li>
        ))}
      </ul>
    </>
  );
};

export default CategoryList;
