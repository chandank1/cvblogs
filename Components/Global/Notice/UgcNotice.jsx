import { useState } from "react";
import Modal from "react-bootstrap/Modal";

const UgcNotice = ({ screenSize }) => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <>
      <div className="mb-3">
        <span
          role="button"
          onClick={handleShow}
          className="position-relative"
          style={{ filter: "drop-shadow(7px 5px 4px #f5f5f5)" }}
        >
          <img
            className="w-100"
            style={{ maxWidth: "400px" }}
            src="/blog/Images/notice/ugc_announced.webp"
            alt="UGC Notice"
            objectFit="contain"
          />
        </span>
      </div>
      <Modal
        size={`${screenSize && screenSize.width < 768 ? "md" : "lg"}`}
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title className="fs-14 fw-bold">
            UGC Announces Admissions in Two Academic Sessions (July & January)
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="p-0">
          <iframe
            className="embed-responsive-item"
            width={"100%"}
            height={screenSize && screenSize.width < 768 ? "250" : "475"}
            src={`https://www.youtube.com/embed/OcHfoR_c210`}
            title="YouTube video player"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          />
        </Modal.Body>
      </Modal>
    </>
  );
};

export default UgcNotice;
