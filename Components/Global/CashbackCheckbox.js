import React, { useEffect, useRef } from "react";
import Modal from "react-bootstrap/Modal";
import CashbackViewDetail from "./CashbackViewDetail";
import store from "../../utils/store";
import { useState } from "@hookstate/core";
import CashbackCount from "./CashbackCount";
import { Dropdown } from "primereact/dropdown";
import styles from "./CashbackCheckbox.module.scss";

const CashbackCheckbox = ({ screenSize, getValues, setValue, errors }) => {
  const audioRef = useRef(null);
  const [popupShown, setPopupShown] = React.useState(false);
  const [show, setShow] = React.useState(false);

  const [donatePopupShown, setDonatePopupShown] = React.useState(false);
  const [donatePopupShow, setDonatePopupShow] = React.useState(false);

  const { cashbackSwitch, cashbackText, cashbackMaxAmount, cashbackTextOnly } =
    useState(store);
  const [audioPlaying, setAudioPlaying] = React.useState(false);

  useEffect(() => {
    if (show) {
      if (audioRef.current) {
        audioRef.current.currentTime = 3;
        audioRef.current.play();
      }
      const timer = setTimeout(() => {
        setShow(false);
      }, 8000);

      return () => clearTimeout(timer);
    }
  }, [show]);

  useEffect(() => {
    if (donatePopupShow) {
      const timer = setTimeout(() => {
        setDonatePopupShow(false);
      }, 5000);

      return () => clearTimeout(timer);
    }
  }, [donatePopupShow]);

  useEffect(() => {
    if (audioRef.current) {
      if (!audioRef.current.paused) {
        setAudioPlaying(true);
      }
    }
  }, [audioRef.current]);

  useEffect(() => {
    if (getValues("couponApplied") === "true") {
      if (!popupShown) {
        setShow(true);
        setPopupShown(true);
      }
    } else if (getValues("couponApplied") === "false") {
      if (!donatePopupShown) {
        setDonatePopupShow(true);
        setDonatePopupShown(true);
      }
    }
  }, [getValues("couponApplied")]);

  return (
    <>
      {cashbackSwitch.get() && (
        <div className={`${styles.cashback_wrap} row position-relative mb-4`}>
          <div className="field position-relative">
            <span className="lead_form_wrap">
              <Dropdown
                checkmark={true}
                highlightOnSelect={true}
                className="w-100"
                appendTo={"self"}
                panelStyle={{ maxWidth: "100%" }}
                options={[
                  {
                    id: "true1",
                    name: (
                      <span className="d-flex gap-1 align-items-center">
                        {getValues("couponApplied") === "true1" ? (
                          <svg
                            style={{ minWidth: "12px" }}
                            xmlns="http://www.w3.org/2000/svg"
                            width="12"
                            height="12"
                            fill="currentColor"
                            className="bi bi-circle-fill"
                            viewBox="0 0 16 16"
                          >
                            <circle cx="8" cy="8" r="8" />
                          </svg>
                        ) : (
                          <svg
                            style={{ minWidth: "12px" }}
                            xmlns="http://www.w3.org/2000/svg"
                            width="12"
                            height="12"
                            fill="currentColor"
                            className="bi bi-circle"
                            viewBox="0 0 16 16"
                          >
                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16" />
                          </svg>
                        )}{" "}
                        Get upskilling courses worth {cashbackMaxAmount.get()}{" "}
                      </span>
                    ),
                  },
                  {
                    id: "true",
                    name: (
                      <span className="d-flex gap-1 align-items-center">
                        {getValues("couponApplied") === "true" ? (
                          <svg
                            style={{ minWidth: "12px" }}
                            xmlns="http://www.w3.org/2000/svg"
                            width="12"
                            height="12"
                            fill="currentColor"
                            className="bi bi-circle-fill"
                            viewBox="0 0 16 16"
                          >
                            <circle cx="8" cy="8" r="8" />
                          </svg>
                        ) : (
                          <svg
                            style={{ minWidth: "12px" }}
                            xmlns="http://www.w3.org/2000/svg"
                            width="12"
                            height="12"
                            fill="currentColor"
                            className="bi bi-circle"
                            viewBox="0 0 16 16"
                          >
                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16" />
                          </svg>
                        )}{" "}
                        Receive CV Subsidy Cash upto {cashbackMaxAmount.get()}{" "}
                        directly in your bank account
                      </span>
                    ),
                  },
                  {
                    id: "false",
                    name: (
                      <span className="d-flex gap-1 align-items-center ">
                        {getValues("couponApplied") === "false" ? (
                          <svg
                            style={{ minWidth: "12px" }}
                            xmlns="http://www.w3.org/2000/svg"
                            width="12"
                            height="12"
                            fill="currentColor"
                            className="bi bi-circle-fill"
                            viewBox="0 0 16 16"
                          >
                            <circle cx="8" cy="8" r="8" />
                          </svg>
                        ) : (
                          <svg
                            style={{ minWidth: "12px" }}
                            xmlns="http://www.w3.org/2000/svg"
                            width="12"
                            height="12"
                            fill="currentColor"
                            className="bi bi-circle"
                            viewBox="0 0 16 16"
                          >
                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16" />
                          </svg>
                        )}{" "}
                        Choose to donate your CV Subsidy benefit to another
                        learner in need
                      </span>
                    ),
                  },

                  {
                    id: false,
                    name: (
                      <div
                        className="d-flex gap-1 align-items-center p-2 rounded text-success fw-bold fs-12"
                        style={{ backgroundColor: "#d4fee8" }}
                      >
                        <span style={{ minWidth: "40px" }}>
                          <img
                            src={"/blog/images/icons/subsidy-logo.png"}
                            width={40}
                            height={30}
                            style={{ objectFit: "contain" }}
                            alt="subsidy logo"
                          />
                        </span>
                        College Vidya Provides this benefit to online learners
                        to enhance the gross enrollment ratio in India.
                      </div>
                    ),
                    disabled: true,
                  },
                ]}
                optionValue="id"
                optionLabel="name"
                value={getValues("couponApplied")} // Bind the value to the state
                onChange={(e) =>
                  setValue("couponApplied", e.value, { shouldValidate: true })
                }
                panelClassName="cashback-dropdown-panel"
                placeholder={
                  <>
                    <img
                      src={"/blog/images/icons/new_icon.gif"}
                      width={20}
                      height={20}
                      alt="animated icon"
                      quality={75}
                      priority
                    />
                    CV Subsidy Benefits{" "}
                    <span className="fs-12 text-success fw-bold">
                      Upto {cashbackMaxAmount.get()}
                    </span>
                  </>
                }
              />

              <label
                className="d-flex align-items-center gap-1 d-none"
                htmlFor="gender"
                style={{
                  position: "absolute",
                  left: "5px",
                  top: "-10px",
                  backgroundColor: "#fff",
                  padding: "0 7px",
                  zIndex: "1",
                  fontSize: "14px",
                }}
              >
                <img
                  src={"/blog/images/icons/new_icon.gif"}
                  width={20}
                  height={20}
                  alt="animated icon"
                  quality={75}
                  priority
                />{" "}
                CV Subsidy Cashback<span className="text-danger">*</span>{" "}
                <span className="fs-12 text-success fw-bold">
                  Upto {cashbackMaxAmount.get()}
                </span>
              </label>
            </span>
          </div>
          <span className="p-error text-xs" style={{ paddingLeft: "20px" }}>
            {errors["couponApplied"]?.message}
          </span>
        </div>
      )}

      <p className="mb-3 text-end">
        <span className="fs-12 text-secondary d-inline-block" role="button">
          <details>
            <summary>View Details</summary>
            <div className="px-2 py-3 rounded fs-12 bg-light rounded mt-2 text-start">
              <div className="d-flex align-items-center gap-2 mb-3">
                {" "}
                {screenSize && screenSize.width > 250 ? (
                  <img
                    className={`${styles.cashback_stamp}`}
                    src={"/blog/images/icons/subsidy-logo.png"}
                    width={60}
                    height={35}
                    objectFit="contain"
                    alt="cashback"
                  />
                ) : null}
              </div>
              <CashbackViewDetail />
            </div>
          </details>
        </span>
      </p>
      <Modal
        show={show}
        onHide={() => setShow(false)}
        centered
        size="sm"
        style={{ zIndex: 99999, backgroundColor: "rgba(0,0,0,0.8)" }}
      >
        <Modal.Header closeButton className="border-0"></Modal.Header>
        <Modal.Body className="text-center pt-0">
          <audio
            src="/blog/images/audios/count1.mp4"
            controls
            ref={audioRef}
            hidden
            muted
          />

          <img
            src={"/blog/images/icons/subsidy-logo.png"}
            width={80}
            height={45}
            alt="cashback"
          />
          <p className="mt-2 mb-0 fs-14 fw-bold">
            <span className="text-success">Congratulations!</span> Save upto{" "}
            <span className="text-nowrap">{cashbackMaxAmount.get()}</span>{" "}
            Instantly on Admission!
          </p>
          {audioPlaying && <CashbackCount />}
        </Modal.Body>
        <Modal.Footer
          className="fs-14 justify-content-center textprimary fw-bold"
          style={{ borderTop: "1px solid #ccc" }}
        >
          Awesome!
        </Modal.Footer>
      </Modal>

      <Modal
        show={donatePopupShow}
        onHide={() => setDonatePopupShow(false)}
        centered
        size="sm"
        style={{ zIndex: 99999, backgroundColor: "rgba(0,0,0,0.8)" }}
      >
        {/* <Modal.Header closeButton className="border-0"></Modal.Header> */}
        <Modal.Body
          className="text-center pt-5 rounded"
          style={{
            background: "url('/blog/images/donate_bg.png')",
            backgroundPosition: "top left",
            backgroundRepeat: "no-repeat",
          }}
        >
          <span
            onClick={() => setDonatePopupShow(false)}
            className="position-absolute top-0 end-0 me-2 mt-2"
            role="button"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="20"
              height="20"
              fill="currentColor"
              className="bi bi-x-lg"
              viewBox="0 0 16 16"
            >
              <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8z" />
            </svg>
          </span>
          <p className="fw-bold textprimary mb-0">
            Give yourself a pat on the back
          </p>
          <p className="fs-12 px-2">
            Only a few kind-hearted people donate their CV{" "}
            {cashbackTextOnly.get().replace("*", "")} to help Students in need.
          </p>
          <img
            className="rounded w-100"
            src={"/blog/images/clapping_new.gif"}
            alt="clapping"
            layout="responsive"
          />
        </Modal.Body>
      </Modal>
    </>
  );
};

export default CashbackCheckbox;
