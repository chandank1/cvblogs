import moment from "moment";
import { useEffect, useState } from "react";

const Timer = ({ timerText }) => {
  const [duration, setDuration] = useState(null);

  const tomorrow = moment().clone().add(1, "day").format("l");
  const tomorrowDate = moment(`${tomorrow} 00:00 AM`);

  useEffect(() => {
    const timer = window.setInterval(() => {
      setDuration(moment.duration(tomorrowDate.diff(moment())));
    }, 1000);
    return () => {
      window.clearInterval(timer);
    };
  }, []);

  return (
    <>
      {duration ? (
        <div className="d-flex gap-2 position-relative pt-4 pb-3 rounded justify-content-center">
          {/* <p
            className="bg-primary position-absolute top-0 start-50 translate-middle w-100 text-white fs-12 text-center rounded"
            style={{ fontSize: "13px" }}
          >
            Take admission to any Online Degree Program
          </p> */}
          <div className="bg-light rounded text-dark py-1 px-3 position-relative d-flex align-items-center justify-content-center">
            <span className="h2 m-0">{duration && duration.days()}</span>
            <span
              className="position-absolute top-100 start-50 mt-2 translate-middle rounded text-dark w-100 text-center"
              style={{
                fontSize: "10px",
                // backgroundColor: "#ae3717",
                maxWidth: "90%",
              }}
            >
              Days
            </span>
          </div>
          <span className="d-flex align-items-center text-dark">:</span>
          <div className="bg-light rounded text-dark py-1 px-3 position-relative d-flex align-items-center justify-content-center">
            <span className="h2 m-0">{duration && duration.hours()}</span>
            <span
              className="position-absolute top-100 start-50 mt-2 translate-middle rounded text-dark w-100 text-center"
              style={{
                fontSize: "10px",
                // backgroundColor: "#ae3717",
                maxWidth: "90%",
              }}
            >
              Hours
            </span>
          </div>
          <span className="d-flex align-items-center text-dark">:</span>
          <div className="bg-light rounded text-dark py-1 px-3 position-relative d-flex align-items-center justify-content-center">
            <span className="h2 m-0">{duration && duration.minutes()}</span>
            <span
              className="position-absolute top-100 start-50 mt-2 translate-middle rounded text-dark w-100 text-center"
              style={{
                fontSize: "10px",
                // backgroundColor: "#ae3717",
                maxWidth: "90%",
              }}
            >
              Minutes
            </span>
          </div>
          <span className="d-flex align-items-center text-dark">:</span>
          <div className="bg-light rounded text-dark py-1 px-3 position-relative d-flex align-items-center justify-content-center">
            <span className="h2 m-0">{duration && duration.seconds()}</span>
            <span
              className="position-absolute top-100 start-50 mt-2 translate-middle rounded text-dark w-100 text-center"
              style={{
                fontSize: "10px",
                // backgroundColor: "#ae3717",
                maxWidth: "90%",
              }}
            >
              Seconds
            </span>
          </div>
        </div>
      ) : null}
    </>
  );
};

export default Timer;
