import React, { useEffect, useState } from "react";
import { Dropdown, DropdownButton } from "react-bootstrap";
import { getAllCountries, getUserCountry } from "../../services/MiscService";

const CountryDropDown = ({ setValue }) => {
  const [selectedCountry, setSelectedCountry] = useState(null);
  const [countries, setCountries] = useState([]);

  useEffect(() => {
    getAllCountries().then((response) => setCountries(response.data));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onCountryChange = (e) => {
    setSelectedCountry(e.id);
    setValue("selected_country", e.id, { shouldValidate: true });
    setValue(
      "phone_code",
      countries.filter((country) => country.id === e.id)[0].phonecode,
      { shouldValidate: true }
    );
    setValue(
      "country_name",
      countries.filter((country) => country.id === e.id)[0].name,
      { shouldValidate: true }
    );
    if (e.id !== 101) {
      setValue("state", 42, { shouldValidate: true });
    }
  };

  useEffect(() => {
    if (countries.length > 0) {
      getUserCountry()
        .then((response) => {
          const autoCountry = countries.some(
            (country) => country.iso2 === response.country
          );
          const userCountry = autoCountry
            ? countries.filter(
                (country) => country.iso2 === response.country
              )[0].id
            : countries[0].id;
          const phoneCode = autoCountry
            ? countries.filter(
                (country) => country.iso2 === response.country
              )[0].phonecode
            : countries[0].phonecode;
          const countryName = autoCountry
            ? countries.filter(
                (country) => country.iso2 === response.country
              )[0].name
            : countries[0].name;
          setSelectedCountry(userCountry);
          setValue("selected_country", userCountry, { shouldValidate: true });
          setValue("phone_code", phoneCode, { shouldValidate: true });
          setValue("country_name", countryName, { shouldValidate: true });
          if (userCountry !== 101) {
            setValue("state", 42, { shouldValidate: true });
          }
        })
        .catch(() => {
          const userCountry = countries.filter(
            (country) => country.id === 101
          )[0];
          setSelectedCountry(userCountry.id);
          setValue("selected_country", userCountry.id, {
            shouldValidate: true,
          });
          setValue("phone_code", userCountry.phonecode, {
            shouldValidate: true,
          });
          setValue("country_name", userCountry.name, { shouldValidate: true });
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [countries]);

  return (
    <DropdownButton
      title={
        selectedCountry ? (
          <div className="d-flex align-items-center gap-1">
            <img
              src={`https://flagcdn.com/w20/${countries
                .filter((country) => country.id === selectedCountry)[0]
                .iso2.toLowerCase()}.png`}
            />
            <span>
              {" "}
              <div className="fs-14" style={{ marginTop: "1px" }}>
                {countries
                  .filter((country) => country.id === selectedCountry)[0]
                  .phonecode.includes("+")
                  ? ""
                  : "+"}
                {
                  countries.filter(
                    (country) => country.id === selectedCountry
                  )[0].phonecode
                }
              </div>
            </span>
          </div>
        ) : (
          ""
        )
      }
    >
      <div style={{ maxHeight: "300px", overflow: "auto" }}>
        {countries.map((country, index) => (
          <Dropdown.Item key={index} onClick={() => onCountryChange(country)}>
            <div
              className="d-flex align-items-center justify-content-between my-1"
              style={{ fontSize: "14px" }}
            >
              <div className="d-flex align-items-center gap-1">
                <span>
                  <img
                    src={`https://flagcdn.com/w20/${country.iso2.toLowerCase()}.png`}
                  />
                </span>
                <span>{country.name}</span>
              </div>
              <div>
                {country.phonecode.includes("+") ? "" : "+"}
                {country.phonecode}
              </div>
            </div>{" "}
          </Dropdown.Item>
        ))}
      </div>
    </DropdownButton>
  );
};

export default CountryDropDown;
