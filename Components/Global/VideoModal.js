import React, { useState } from "react";
import Image from "next/image";
import Link from "next/link";
import Modal from "react-bootstrap/Modal";
import SwipeImage from "./Slider/SwipeImage";

const VideoModal = ({ fromFooter }) => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <>
      {fromFooter ? (
        <span className="m-0" style={{ fontSize: "9px" }} onClick={handleShow}>
          Counseling
        </span>
      ) : (
        <span className="m-0 d-flex align-items-center text-nowrap" onClick={handleShow}>
          <Image
            className="me-1"
            src={"/blog/Images/icons/video-camera.png"}
            alt="video"
            width={20}
            height={20}
            priority
          />{" "}
          Video Counseling
        </span>
      )}

      <Modal show={show} onHide={handleClose} centered style={{ zIndex: "9999" }}>
        <Modal.Body className="text-center position-relative">
          <Image
            src={"/blog/Images/icons/close-dark.svg"}
            className="cursor-pointer position-absolute end-0 top-0 me-2 mt-2"
            onClick={handleClose}
            width={30}
            height={30}
            alt="Close"
          />
          {/* <Image
            src={"/blog/Images/business.webp"}
            width={300}
            height={200}
            alt="video Counseling"
          /> */}

          <div className="pt-2">
            <SwipeImage />
          </div>
          <h2 className="h2 bold-font mt-3">
            Get <span style={{ color: "#0056d2" }}>Real Experts</span> on your
            side
          </h2>
          <p className="mb-2 px-3" style={{ fontSize: "14px" }}>
            Before venturing to seek career counseling, answer a few basic
            questions so we can connect you with our best expert counselor for
            personalised guidance and mentorship.
          </p>
          <Link
            className="text-white px-3 py-2 d-inline-block my-3 text-decoration-none"
            href="https://collegevidya.com/suggest-me-an-university/"
            style={{ backgroundColor: "#0056d2", borderRadius: "25px" }}
          >
            Let&apos;s start
          </Link>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default VideoModal;
