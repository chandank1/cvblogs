import Image from "next/image";
import Link from "next/link";
import React from "react";
import { useState } from "@hookstate/core";
import store from "../../utils/store";

const Logo = () => {
  const { universalLogo } = useState(store);
  return (
    <>
      <div className="d-none d-lg-block">
        {" "}
        <Link href="https://collegevidya.com/">
          <Image
            src={universalLogo.get()}
            width={170}
            height={50}
            alt="logo"
            quality={100}
            priority
            style={{ objectFit: "contain" }}
          />
        </Link>
      </div>
      <div className="d-block d-lg-none">
        {" "}
        <Link href="https://collegevidya.com/">
          <Image
            src={universalLogo.get()}
            width={110}
            height={40}
            alt="logo"
            quality={100}
            priority
            style={{ objectFit: "contain" }}
          />
        </Link>
      </div>
    </>
  );
};

export default Logo;
