import Link from "next/link";
import React, { useEffect, useState } from "react";
import { Form } from "react-bootstrap";
import {
  getOTP,
  pushCustomActivity,
  verifyOTP,
} from "../../services/MiscService";
import { Button } from "primereact/button";

const VerifyOTPModal = ({
  uid,
  userName,
  userEmail,
  userMobile,
  redirectionURL,
  closeModal,
}) => {
  const [verifyingOTP, setVerifyingOTP] = useState(false);
  const [OTPfromUser, setOTPfromUser] = useState(null);
  const [resendTimer, setResendTimer] = useState(30);
  const [showContinueButton, setShowContinueButton] = useState(false);

  const checkOTP = () => {
    setVerifyingOTP(true);
    verifyOTP(userMobile, OTPfromUser.toString())
      .then(() => {
        // setVerifyingOTP(false);
        pushCustomActivity({
          uid: uid,
          mx_value_one: "yes",
          activity_name: "otp_verified",
        });

        window.location.href = redirectionURL;
      })
      .catch(() => {
        setVerifyingOTP(false);
        iziToast.error({
          title: "Error",
          message: "Invalid OTP!",
          timeout: 2000,
          position: "topRight",
        });
      });
  };

  useEffect(() => {
    const timer = window.setInterval(() => {
      if (resendTimer > 0) {
        setResendTimer((prevTime) => prevTime - 1);
      }
    }, 1000);
    return () => {
      window.clearInterval(timer);
    };
  }, [resendTimer]);

  useEffect(() => {
    getOTP(userName, userMobile, userEmail);
  }, []);

  return (
    <div className="form-demo">
      <div className="flex justify-content-center">
        <div>
          <div className="otp-panel">
            <div className="pb-4">
              <p
                style={{ fontSize: "16px" }}
                className="m-0 fw-bold signup_text"
              >
                Please enter,
                <br /> 6 digit OTP sent on {userMobile} <br /> & {userEmail}
              </p>
              <p
                className="d-inline-block fs-14 mt-3 text-primary cursor-pointer"
                onClick={() => closeModal()}
              >
                Change Mobile Number
              </p>
            </div>
            <label className="mb-2" htmlFor="Mobile Number">
              Enter OTP
            </label>
            <Form.Control
              type="number"
              className="w-100"
              onChange={(e) => setOTPfromUser(e.target.value)}
              useGrouping={false}
              pattern="[0-9]*"
            />
            <Button
              className={`bluebtn w-100 mt-3 mb-4 d-flex justify-content-center fs-14`}
              disabled={!OTPfromUser || OTPfromUser.toString().length !== 6}
              label={"Confirm OTP"}
              onClick={() => checkOTP()}
              loading={verifyingOTP}
            />
            <div className="text-center">
              {" "}
              {resendTimer <= 0 ? (
                <span className="fs-12">Didn&apos;t receive the OTP yet?</span>
              ) : null}
              {resendTimer > 0 ? (
                <div style={{ fontSize: "12px" }}>
                  You can resend OTP after{" "}
                  <span style={{ color: "orange" }}>
                    {Math.floor(resendTimer / 60)}:{resendTimer % 60}
                  </span>
                </div>
              ) : (
                <span
                  style={{ fontSize: "12px" }}
                  className="text-primary cursor-pointer"
                  onClick={() => {
                    pushCustomActivity({
                      uid: uid,
                      mx_value_one: "yes",
                      activity_name: "resent_otp_code",
                    });

                    getOTP(userName, userMobile, userEmail);
                    setResendTimer(59);
                    setShowContinueButton(true);
                  }}
                >
                  &nbsp;Resend
                </span>
              )}
              {showContinueButton ? (
                <div>
                  <Link
                    href={redirectionURL}
                    onClick={() =>
                      pushCustomActivity({
                        uid: uid,
                        mx_value_one: "yes",
                        activity_name: "continued_without_otp",
                      })
                    }
                    className="d-inline-block px-3 py-2 rounded mt-3 text-success fw-bold"
                    style={{ backgroundColor: "#ddfce3", fontSize: "14px" }}
                  >
                    Continue Without OTP{" "}
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      className="bi bi-arrow-right"
                      viewBox="0 0 16 16"
                    >
                      <path
                        fillRule="evenodd"
                        d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8"
                      />
                    </svg>
                  </Link>
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VerifyOTPModal;
