import React from "react";
import Rupee from "../Global/Rupee";
import { useState } from "@hookstate/core";
import SlotCounter from "react-slot-counter";
import store from "../../utils/store";

const getRandomNumberInRange = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

const CashbackCount = () => {
  const { cashbackTextOnly } = useState(store);

  const min = 4065567;
  const max = 4500000;
  const text = getRandomNumberInRange(min, max);

  return (
    <>
      {" "}
      <figure className="text-center mt-3">
        <div className="blockquote mb-3 d-inline-flex gap-2">
          <div className="d-flex">
            {" "}
            <span
              className="fw-bold text-center mx-1 rounded"
              style={{
                padding: "0px 6px",
                backgroundColor: "rgb(243, 201, 2,0.3)",
                color: "#f75d34",
              }}
            >
              <Rupee />
            </span>
            <SlotCounter
              value={text}
              animateOnVisible={{
                triggerOnce: false,
              }}
              charClassName="fw-bold bg-success bg-opacity-10 text-success text-center mx-1 px-1 rounded"
            />
          </div>
        </div>
        <figcaption className="text-success" style={{ fontSize: "12px" }}>
          {cashbackTextOnly.get().replace("*", "")} cash transferred to
          student&apos;s accounts to support GER in India.
        </figcaption>
      </figure>
    </>
  );
};

export default CashbackCount;
