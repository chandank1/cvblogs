import React from "react";
import { Rating } from "primereact/rating";
import styles from "./RatingStar.module.scss";

const RatingStar = (props) => {
  return (
    <>
      <Rating
        className={`${styles.star_color}`}
        value={props.value}
        readOnly
        stars={5}
        cancel={false}
      />
    </>
  );
};

export default RatingStar;
