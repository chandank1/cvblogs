import Link from "next/link";
import React from "react";

const RemoveMessage = () => {
  return (
    <>
      <div className="text-center d-none" style={{ fontSize: "11px" }}>
        By clicking, you agree to our{" "}
        <Link
          className="text-primary"
          href="https://collegevidya.com/privacy-policy/"
          target="_blank"
        >
          Privacy policy
        </Link>
        ,
        <Link
          className="text-primary"
          href="https://collegevidya.com/term-and-conditions/"
          target="_blank"
        >
          Terms of Use<sup>+</sup>
        </Link>
        Disclaimers{" "}
      </div>

      <p className="fs-11 text-center">
        I authorise College Vidya & its representatives to contact me with
        updates and notifications via Email/SMS/What&apos;sApp/Call. This will
        override on DND/NDNC{" "}
        <Link
          className="text-primary"
          href="https://collegevidya.com/privacy-policy/"
          target="_blank"
        >
          Privacy policy
        </Link>
        ,
        <Link
          className="text-primary"
          href="https://collegevidya.com/term-and-conditions/"
          target="_blank"
        >
          Terms of Use<sup>+</sup>
        </Link>
      </p>
    </>
  );
};

export default RemoveMessage;
