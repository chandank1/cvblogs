import React from "react";
import styles from "./Rupee.module.scss";

const Rupee = () => {
  return (
    <>
      <span className={`${styles.rupee}`}>&#8377;</span>
    </>
  );
};

export default Rupee;
