import React from "react";

const Podcast = ({ podcastURL }) => {
  return (
    <iframe
      className="embed-responsive-item"
      srcDoc={podcastURL}
      loading="lazy"
      width="100%"
      height="200"
      frameBorder="0"
      scrolling="no"
    />
  );
};

export default Podcast;
