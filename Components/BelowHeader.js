import React, { useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Link from "next/link";
import Image from "next/image";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import VideoModal from "./Global/VideoModal";

const BelowHeader = ({ menuData }) => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <div
        className="py-3 d-none d-lg-block"
        style={{ backgroundColor: "#f6f7f8" }}
      >
        <Container>
          <Row>
            <Col className="col-3">
              <Form.Select
                onChange={(e) =>
                  (window.location.href = `https://collegevidya.com/top-universities-colleges/${e.target.value}/`)
                }
              >
                <option>Top Universities</option>
                {menuData
                  .map((domain) => domain.courses)
                  .flat()
                  .map((course) => (
                    <option key={course.id} value={course.slug}>
                      {course.name}
                    </option>
                  ))}
              </Form.Select>
            </Col>
            <Col className="d-flex align-items-center">
              <ul className="list-unstyled d-flex m-0 gap-3 align-items-center fs-14">
                <li>
                  <Link
                    className="d-flex align-items-center gap-1 text-nowrap"
                    href="/expert-interviews"
                    style={{ width: "max-content" }}
                  >
                    <Image
                      src={"/blog/Images/icons/expert-interview.svg"}
                      alt="video"
                      width={20}
                      height={20}
                      priority
                    />{" "}
                    <span>Expert Interviews</span>
                  </Link>
                </li>
                <li>
                  <Link
                    href="https://collegevidya.com/blog/category/reviews/"
                    className="d-flex text-nowrap"
                    style={{ width: "max-content" }}
                  >
                    University Reviews
                  </Link>
                </li>
                <li>
                  <Link
                    href="https://collegevidya.com/blog/category/career-opportunities/"
                    className="d-flex text-nowrap"
                    style={{ width: "max-content" }}
                  >
                    Career Guide
                  </Link>
                </li>
                <li className="cursor-pointer">
                  <VideoModal />
                </li>
                <li onClick={handleShow} className="cursor-pointer">
                  <p
                    className="m-0 rounded px-2 py-1 d-flex text-nowrap"
                    style={{ border: "1px solid #000", width: "max-content" }}
                  >
                    Important Facts
                  </p>
                </li>
                <li>
                  <Link
                    href="https://collegevidya.com/question-answer/"
                    className="m-0 rounded px-2 py-1 d-flex bg-dark text-white text-nowrap"
                    style={{ border: "1px solid #000", width: "max-content" }}
                  >
                    Ask any Question - CV Forum
                  </Link>
                </li>
              </ul>
            </Col>
          </Row>
        </Container>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Important Facts</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p
            className="text-center rounded p-2"
            style={{ backgroundColor: "aliceblue" }}
          >
            We have prepared a checklist especially for you
          </p>
          <Row className="row-cols-2 cheklist">
            <Col className="mb-3">
              <Link
                href="https://collegevidya.com/checklist/?t=online-degree-course"
                className="border rounded px-2 py-3 text-center d-inline-block w-100 h-100 d-flex justify-content-center flex-column align-items-center"
              >
                <Image
                  src={"/blog/Images/online1.webp"}
                  width={60}
                  height={60}
                  alt="MBA,MCA,BCA,B.COM"
                />
                <p className="px-3 mt-3">MBA,MCA,BCA,B.COM & Others</p>
              </Link>
            </Col>
            <Col className="mb-3">
              <Link
                href="https://collegevidya.com/checklist/?t=btech-mtech"
                className="border rounded px-2 py-3 text-center d-inline-block w-100 h-100 d-flex justify-content-center flex-column align-items-center"
              >
                <Image
                  src={"/blog/Images/online1.webp"}
                  width={60}
                  height={60}
                  alt="MBA,MCA,BCA,B.COM"
                />
                <p className="px-3 mt-3">
                  B.Tech & M.Tech For Working Professionals
                </p>
              </Link>
            </Col>
            <Col className="mb-3">
              <Link
                href="https://collegevidya.com/checklist/?t=cms-ed"
                className="border rounded px-2 py-3 text-center d-inline-block w-100 h-100 d-flex justify-content-center flex-column align-items-center"
              >
                <Image
                  src={"/blog/Images/online4.webp"}
                  width={60}
                  height={60}
                  alt="MBA,MCA,BCA,B.COM"
                />
                <p className="px-3 mt-3">CMSED</p>
              </Link>
            </Col>
            <Col className="mb-3">
              <Link
                href="https://collegevidya.com/checklist/?t=certificate-diploma"
                className="border rounded px-2 py-3 text-center d-inline-block w-100 h-100 d-flex justify-content-center flex-column align-items-center"
              >
                <Image
                  src={"/blog/Images/online-education.webp"}
                  width={60}
                  height={60}
                  alt="MBA,MCA,BCA,B.COM"
                />
                <p className="px-3 mt-3">Skill Certificates and Diploma</p>
              </Link>
            </Col>
          </Row>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default BelowHeader;
