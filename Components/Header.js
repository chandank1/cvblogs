import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import Logo from "../Components/Global/Logo";
import { DropdownSubmenu, NavDropdownMenu } from "react-bootstrap-submenu";
import "react-bootstrap-submenu/dist/index.css";
import React, { useEffect } from "react";
import Offcanvas from "react-bootstrap/Offcanvas";
import Accordion from "react-bootstrap/Accordion";
import Link from "next/link";
import Image from "next/image";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import { Button, Form, InputGroup } from "react-bootstrap";
import { useRouter } from "next/router";
import { useState } from "@hookstate/core";
import store from "../utils/store";

const Header = ({ menuData, search }) => {
  const router = useRouter();
  const [show1, setShow1] = React.useState(false);
  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);

  const [show3, setShow3] = React.useState(false);
  const handleClose3 = () => setShow3(false);
  const handleShow3 = () => setShow3(true);

  const [defaultSearchValue, setDefaultSearchValue] = React.useState(null);
  const { tagLine } = useState(store);

  const { register, handleSubmit } = useForm({
    defaultValues: {
      query: router.query.q,
    },
    mode: "onChange",
  });

  const onSubmit = (data) => {
    const query = data.query;
    window.location.href = `/blog/search?q=${query}`;
  };

  useEffect(() => {
    if (router.isReady) {
      setDefaultSearchValue(router.query.q);
    }
  }, [router]);

  return (
    <>
      <Container>
        <div className="d-flex d-lg-none align-items-center justify-content-between mt-3">
          <div className="d-flex align-items-center">
            <Logo />{" "}
            <span>
              <Link href="https://collegevidya.com/blog/">
                <Image
                  src={"/blog/Images/blog.png"}
                  width={40}
                  height={20}
                  alt="blog"
                  objectFit="contain"
                />
              </Link>
            </span>
          </div>

          <div className="d-flex align-items-center gap-2">
            {/* <Link href={"https://collegevidya.com/web-stories/"}>
              <Image
                src={"/blog/Images/icons/web-story.svg"}
                width={32}
                height={32}
                alt="badge"
                priority
              />
            </Link> */}

            {/* <div
              onClick={handleShow1}
              className="bgprimary text-white rounded px-2 py-2 cursor-pointer d-flex gap-1 flex-nowrap"
              style={{ fontSize: "10px", minWidth: "max-content" }}
            >
              <span className="d-none d-sm-block">Explore</span>
              <span>Courses</span>
            </div> */}

            <Link
              className="bgprimary text-white rounded px-2 py-2 cursor-pointer"
              style={{ fontSize: "10px", minWidth: "max-content" }}
              href={
                "https://collegevidya.com/suggest-me-an-university/?utm_source=blog_stickyfooter&utm_medium=organic&utm_campaign=Sticky_footer&utm_content=lead"
              }
            >
              Find University{" "}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="12"
                height="12"
                fill="currentColor"
                className="bi bi-arrow-right"
                viewBox="0 0 16 16"
              >
                <path
                  fillRule="evenodd"
                  d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8"
                />
              </svg>
            </Link>
          </div>
        </div>

        <Navbar expand="lg" variant="dark" className="d-none d-lg-flex">
          <div className="text-nowrap text-center d-flex align-items-center justify-content-center">
            <Logo />{" "}
            <span>
              <Link href="https://collegevidya.com/blog/">
                <Image
                  src={"/blog/Images/blog.png"}
                  width={60}
                  height={28}
                  alt="blog"
                  objectFit="contain"
                />
              </Link>
            </span>
          </div>

          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ms-auto align-items-center">
              <NavDropdownMenu
                title="Explore Programs"
                id="collasible-nav-dropdown"
                className="bgprimary rounded px-1 header_title"
                alignLeft
              >
                {menuData.map((domain) => (
                  <DropdownSubmenu key={domain.id} href="#" title={domain.name}>
                    {domain.courses.map((course) => (
                      <NavDropdown.Item
                        key={course.id}
                        href={`https://collegevidya.com/courses/${course.slug}`}
                      >
                        {course.name}
                      </NavDropdown.Item>
                    ))}
                  </DropdownSubmenu>
                ))}
              </NavDropdownMenu>
              <Nav.Link
                href="https://collegevidya.com/suggest-me-an-university/"
                className="rounded mx-2 px-2 text-primary d-none d-lg-block position-relative header_title"
                style={{
                  backgroundColor: "aliceblue",
                }}
              >
                Suggest in 2 Mins
                {/* <img
                  className="w-100"
                  style={{ borderRadius: "25px" }}
                  src={"/blog/images/icons/ai-suggest.gif"}
                  alt="find best university"
                /> */}
                <span
                  className="rounded px-1 text-white position-absolute top-0 start-50 translate-middle d-flex"
                  style={{ backgroundColor: "#f75d34", fontSize: "10px" }}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="12"
                    height="12"
                    fill="currentColor"
                    className="bi bi-lightning-charge-fill"
                    viewBox="0 0 16 16"
                  >
                    <path d="M11.251.068a.5.5 0 0 1 .227.58L9.677 6.5H13a.5.5 0 0 1 .364.843l-8 8.5a.5.5 0 0 1-.842-.49L6.323 9.5H3a.5.5 0 0 1-.364-.843l8-8.5a.5.5 0 0 1 .615-.09z" />
                  </svg>{" "}
                  in 2 Mins
                </span>
              </Nav.Link>
              <Nav.Link
                href="https://collegevidya.com/web-stories/"
                className="rounded mx-2 px-2 text-primary d-none d-lg-block header_title"
                style={{
                  backgroundColor: "ghostwhite",
                }}
              >
                <Image
                  src={"/blog/Images/icons/web-story.svg"}
                  width={20}
                  height={20}
                  alt="badge"
                  priority
                />{" "}
                Web Stories
              </Nav.Link>
              <Nav.Link
                href="https://collegevidya.com/career-finder/"
                className="rounded mx-2 px-2 text-primary d-none d-lg-block header_title position-relative"
                style={{
                  backgroundColor: "ghostwhite",
                }}
              >
                Career Finder
               
              </Nav.Link>
              <div>
                <Form
                  onSubmit={handleSubmit(onSubmit)}
                  className="d-flex h-100"
                >
                  <InputGroup className="me-2 fs-12">
                    <Form.Control
                      {...register("query")}
                      size="lg"
                      style={{ backgroundColor: "#f8f9fa" }}
                      type="search"
                      placeholder="Search..."
                      aria-label="Search"
                      defaultValue={defaultSearchValue}
                      required
                      className="fs-12"
                    />
                    <Button
                      type="submit"
                      variant="outline-secondary"
                      style={{ border: "1px solid #ccc" }}
                      id="button-addon1"
                    >
                      <Image
                        src={"/blog/Images/icons/search.png"}
                        width={16}
                        height={16}
                        alt="search"
                      />
                    </Button>
                  </InputGroup>
                </Form>
              </div>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </Container>

      {search ? (
        <div>
          <Form
            onSubmit={handleSubmit(onSubmit)}
            className="d-flex h-100 mx-3 mt-1 bg-light py-2 px-2 rounded"
          >
            <InputGroup className="me-2">
              <Form.Control
                {...register("query")}
                size="lg"
                type="search"
                placeholder="Search Here..."
                aria-label="Search"
                defaultValue={defaultSearchValue}
                required
              />
              <Button
                type="submit"
                variant="outline-secondary"
                id="button-addon1"
              >
                <Image
                  src={"/blog/Images/icons/search.png"}
                  width={18}
                  height={18}
                  alt="search"
                />
              </Button>
            </InputGroup>
          </Form>
        </div>
      ) : null}

      <Offcanvas show={show1} onHide={handleClose1} style={{ zIndex: "9999" }}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title style={{ fontSize: "14px" }}>
            {" "}
            {tagLine.get()}
          </Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <p className="mb-1" style={{ fontSize: "12px", color: "#00d8a2" }}>
            Compare Universities
          </p>
          <Accordion className="nav-accordion">
            {menuData.map((domain) => (
              <Accordion.Item key={domain.id} eventKey={domain.id}>
                <Accordion.Header className="border-0">
                  {domain.name}
                </Accordion.Header>
                <Accordion.Body>
                  {domain.courses.map((course) => (
                    <Link
                      className="d-block py-2"
                      key={course.id}
                      href={`https://collegevidya.com/courses/${course.slug}`}
                    >
                      <Image
                        src={course.icon}
                        width={16}
                        height={16}
                        priority
                        alt={course.name}
                      />{" "}
                      <span className="ms-2">{course.name}</span>
                    </Link>
                  ))}
                </Accordion.Body>
              </Accordion.Item>
            ))}
          </Accordion>
          <p
            className="mb-1 mt-3"
            style={{ fontSize: "12px", color: "#00d8a2" }}
          >
            Why Trust Us
          </p>
          <ul className="list-unstyled menu_list">
            <li>
              <Link
                className="d-block"
                href="https://collegevidya.com/our-trust/"
              >
                College Vidya Commitment
              </Link>
            </li>
            <li>
              <Link
                className="d-block"
                href="https://chats.collegevidya.com/collegevidya/verify-counsellor"
              >
                Verify Your Advisor
              </Link>
            </li>
            <li>
              <Link className="d-block" href="https://verify.collegevidya.com/">
                {" "}
                Verify Your University{" "}
              </Link>
            </li>
            <li>
              <Link
                className="d-block"
                href="https://collegevidya.com/why-college-vidya/"
              >
                {" "}
                Why College Vidya?{" "}
              </Link>
            </li>
            <li>
              <Link
                className="d-block"
                href="https://collegevidya.com/collection/"
              >
                Important Videos(CV TV)
              </Link>
            </li>
          </ul>
          <p className="mb-1" style={{ fontSize: "12px", color: "#00d8a2" }}>
            Resources
          </p>
          <ul className="list-unstyled menu_list">
            <li>
              <Link
                className="d-block"
                href="https://collegevidya.com/question-answer/"
                onClick={() =>
                  (window.location.href =
                    "https://collegevidya.com/question-answer/")
                }
              >
                Ask any Question - CV Forum
              </Link>
            </li>
            <li>
              <Link
                className="d-block"
                href="https://collegevidya.com/blog/"
                onClick={() =>
                  (window.location.href = "https://collegevidya.com/blog/")
                }
              >
                Blogs
              </Link>
            </li>
            <li>
              <Link
                className="d-block"
                href="https://collegevidya.com/web-stories/"
                onClick={() =>
                  (window.location.href =
                    "https://collegevidya.com/web-stories/")
                }
              >
                Web Stories
              </Link>
            </li>
            <li>
              <Link
                className="d-block"
                href="https://collegevidya.com/blog/expert-interviews/"
                onClick={() =>
                  (window.location.href =
                    "https://collegevidya.com/blog/expert-interviews/")
                }
              >
                Expert Interviews
              </Link>
            </li>

            <li
              onClick={() => {
                handleShow3();
                handleClose1();
              }}
            >
              <Link className="d-block" href="">
                Top Universities
              </Link>
            </li>
          </ul>
          <p className="mb-1" style={{ fontSize: "12px", color: "#00d8a2" }}>
            Contact Us
          </p>
          <ul className="list-unstyled menu_list">
            <li>
              <Link className="d-block" href="tel:18004205757">
                <svg
                  stroke="currentColor"
                  fill="currentColor"
                  stroke-width="0"
                  version="1"
                  viewBox="0 0 48 48"
                  enable-background="new 0 0 48 48"
                  font-size="18"
                  height="1em"
                  width="1em"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fill="#2196F3"
                    d="M26.4,33.9c0,0,4-2.6,4.8-3c0.8-0.4,1.7-0.6,2.2-0.2c0.8,0.5,7.5,4.9,8.1,5.3c0.6,0.4,0.8,1.5,0.1,2.6 c-0.8,1.1-4.3,5.5-5.8,5.4c-1.5,0-8.4,0.4-20.3-11.4C3.6,20.7,4,13.8,4,12.3c0-1.5,4.3-5.1,5.4-5.8c1.1-0.8,2.2-0.5,2.6,0.1 c0.4,0.6,4.8,7.3,5.3,8.1c0.3,0.5,0.2,1.4-0.2,2.2c-0.4,0.8-3,4.8-3,4.8s0.7,2.8,5,7.2C23.5,33.2,26.4,33.9,26.4,33.9z"
                  ></path>
                  <g fill="#3F51B5">
                    <path d="M35,9H25v4h10c1.1,0,2,0.9,2,2v10h4V15C41,11.7,38.3,9,35,9z"></path>
                    <polygon points="28,16 21.3,11 28,6"></polygon>
                  </g>
                </svg>{" "}
                New User : 18004205757
              </Link>
            </li>
            <li>
              <Link className="d-block" href="tel:18003097947">
                <svg
                  stroke="currentColor"
                  fill="currentColor"
                  stroke-width="0"
                  version="1"
                  viewBox="0 0 48 48"
                  enable-background="new 0 0 48 48"
                  font-size="20"
                  height="1em"
                  width="1em"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fill="#2196F3"
                    d="M40,22h-8l-4,4V12c0-2.2,1.8-4,4-4h8c2.2,0,4,1.8,4,4v6C44,20.2,42.2,22,40,22z"
                  ></path>
                  <circle fill="#FFA726" cx="17" cy="19" r="8"></circle>
                  <path
                    fill="#607D8B"
                    d="M30,36.7c0,0-3.6-6.7-13-6.7S4,36.7,4,36.7V40h26V36.7z"
                  ></path>
                </svg>{" "}
                Existing User : 18003097947
              </Link>
            </li>
          </ul>
          <p className="mb-1" style={{ fontSize: "12px", color: "#00d8a2" }}>
            {" "}
            Download App
          </p>
          <ul className="list-unstyled menu_list pt-2 d-flex align-items-center gap-2">
            <li>
              <Link
                href="https://play.google.com/store/apps/details?id=com.app.collegevidya"
                target="_blank"
              >
                <img
                  src={"/blog/Images/google_play.png"}
                  width={120}
                  height={38}
                  alt="play store"
                />
              </Link>
            </li>
            <li>
              <Link href="https://apps.apple.com/in/app/college-vidya/id6450980823">
                <img
                  src={"/blog/Images/appstore.png"}
                  width={120}
                  height={38}
                  alt="app store"
                />
              </Link>
            </li>
          </ul>
        </Offcanvas.Body>
      </Offcanvas>

      <Modal show={show3} onHide={handleClose3} backdrop="static" fullscreen>
        <Modal.Header closeButton>
          <Modal.Title>Top Universities</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ul className="list-unstyled">
            {menuData
              .map((domain) => domain.courses)
              .flat()
              .map((course) => (
                <li
                  className="px-3 py-2 rounded mb-2 border"
                  style={{ backgroundColor: "aliceblue", fontSize: "14px" }}
                  key={course.id}
                  value={course.slug}
                >
                  <Link
                    href={`https://collegevidya.com/top-universities-colleges/${course.slug}`}
                  >
                    {course.name}
                  </Link>
                </li>
              ))}
          </ul>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default Header;
